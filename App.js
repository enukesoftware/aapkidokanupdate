/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { View, Image, Platform, DeviceEventEmitter } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Geocoder from 'react-native-geocoding';
import Constants from './app/constants/constants';
import Order from './app/constants/order';
import NavigationService from './app/global/NavigationService';
import { configureStore } from "./app/redux/Store/Store"
import { Provider } from 'react-redux';
import firebase from 'react-native-firebase';

//Screens
//Login Stack
import OtpScreen from './app/components/Otp';
import SignUp from './app/components/SignUp';
import LoginScreen from './app/components/Login';
import SelectLocation from './app/components/SelectLocation';
import SetStoreScreen from './app/components/SetStore';

//Main Stack
import HomeScreen from './app/components/Home';
import SubCategoryScreen from './app/components/SubCategory';
import MoreScreen from './app/components/More';
import CategoriesScreen from './app/components/Categories';
import AuthLoadingScreen from './app/components/Auth';
import StoresScreen from './app/components/Stores';
import MyOrdersScreen from './app/components/MyOrders';
import SearchScreen from './app/components/Search';
import ProfileScreen from './app/components/Profile';
import MyAddressScreen from './app/components/MyAddress';
import AddressFormScreen from './app/components/AddressForm';
import PaymentScreen from './app/components/Payment';
import CartScreen from './app/components/Cart';
import SelectAddressScreen from './app/components/SelectAddress';
import OrderStatusScreen from './app/components/OrderStatus';
import OrderDetails from './app/components/OrderDetails';
import OrderDetailsScreen from './app/components/OrderDetails';
import AllProductsScreen from './app/components/AllProducts';
import ChangePasswordScreen from './app/components/ChangePassword';
import ForgotPasswordScreen from './app/components/ForgotPassword';
import ResetPasswordScreen from './app/components/ResetPassword';
import AddressAutocompleteScreen from './app/components/AddressAutocomplete';
import SearchAddressScreen from './app/components/SearchAddress';
import TrackOrderScreen from './app/components/TrackDriver/Index';

import SelectLocality from './app/components/SelectLocality';

//Icons
import HomeOnIcon from './app/assets/images/home-green.png';
import HomeOffIcon from './app/assets/images/home.png';
import TilesOnIcon from './app/assets/images/tiles-on.png';
import TilesOffIcon from './app/assets/images/tiles-off.png';
import ShopOnIcon from './app/assets/images/shop-on.png';
import ShopOffIcon from './app/assets/images/shop-off.png';
import MenuOnIcon from './app/assets/images/menu-on.png';
import MenuOffIcon from './app/assets/images/menu-off.png';
// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.
const OrderStatusStack = createStackNavigator({
	Status: OrderStatusScreen
});

const LoginStack = createStackNavigator({
	// ForgotPassword: ForgotPasswordScreen,
	// first: LoginScreen,
	SelectLocation: SelectLocation,
	Login: LoginScreen,
	SelectLocationAfterLogin: SelectLocation,
	SignUp: SignUp,
	Otp: OtpScreen,
	SetStore: SetStoreScreen,
	ForgotPassword: ForgotPasswordScreen,
	SendOtp: OtpScreen,
	ResetPassword: ResetPasswordScreen
});

const OrdersStack = createStackNavigator({
	MainOrders: MyOrdersScreen,
	MainOrderDetails: OrderDetailsScreen
});

const OtherLoginNavigator = createStackNavigator({
	OtherLogin: LoginScreen,
	OtherSignUp: SignUp,
	OtherOtp: OtpScreen,
	OtherForgotPassword: ForgotPasswordScreen,
	OtherSendOtp: OtpScreen,
	OtherResetPassword: ResetPasswordScreen
})

const CartStack = createStackNavigator({
	Cart: CartScreen,
	SelectAddressInHome: SelectAddressScreen,
	AddAddressFormInHome: AddressFormScreen,
	SearchAddress: SearchAddressScreen,
	AddressAutocomplete: AddressAutocompleteScreen,
	PaymentInHome: PaymentScreen,
});

const HomeTab = createStackNavigator({
	// FirstPage: SubCategoryScreen,
	Home: HomeScreen,
	SelectLocationInHome: SelectLocation,
	SetStoreInHome: SetStoreScreen,
	HomeSubCategory: SubCategoryScreen,
	AllProducts: AllProductsScreen,
	AllCategories: CategoriesScreen
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const CategoriesTab = createStackNavigator({
	Categories: CategoriesScreen,
	SubCategory: SubCategoryScreen,
	CategoryCart: CartScreen,
	SelectAddress: SelectAddressScreen,
	PaymentInCategory: PaymentScreen
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const SearchTab = createStackNavigator({
	Search: SearchScreen,
	SearchCart: CartScreen,
	SelectAddressInSearch: SelectAddressScreen,
	PaymentInSearch: PaymentScreen
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const StoreTab = createStackNavigator({
	Store: StoresScreen,
	StoreCart: CartScreen,
	SelectAddressInStore: SelectAddressScreen,
	PaymentInStore: PaymentScreen
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const MoreTab = createStackNavigator({
	// first: MyOrdersScreen,
	More: MoreScreen,
	MyOrdersScreen: MyOrdersScreen,
	OrderDetails: OrderDetails,
	TrackOrder: TrackOrderScreen,
	Profile: ProfileScreen,
	MyAddress: MyAddressScreen,
	AddressForm: AddressFormScreen,
	SearchAddress: SearchAddressScreen,
	AddressAutocomplete: AddressAutocompleteScreen,
	MoreLogin: LoginScreen,
	ChangePassword: ChangePasswordScreen
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const TabNavigator = createBottomTabNavigator({
	Home: {
		screen: HomeTab,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={HomeOffIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={HomeOnIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}

				</View>

			),
		})
	},
	Categories: {
		screen: CategoriesTab,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={TilesOffIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={TilesOnIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}

				</View>

			),
		})
	},
	Search: {
		screen: SearchTab,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{
					position: "relative",
					height: '100%',
					width: '100%',
					justifyContent: "center",
					alignItems: "center"
				}}>
					<View style={{
						position: "relative",
						zIndex: 99,
						top: -15,
						shadowColor: "#00b762",
						shadowOpacity: .5,
						shadowRadius: 5,
						shadowOffset: { height: 7, width: 1 },
						elevation: 5,
						zIndex: 0,
						borderRadius: 27,
					}}>
						<Image source={require("./app/assets/images/search.png")} style={{
							width: 50,
							height: 50,
							resizeMode: "contain",
						}}></Image>
					</View>
				</View>
			)
		})
	},
	Store: {
		screen: StoreTab,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={ShopOffIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={ShopOnIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}

				</View>
			)
		})
	},
	More: {
		screen: MoreTab,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor, focused }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={MenuOffIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={MenuOnIcon} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}

				</View>
			)
		})
	},
}, {
	// initialRouteName: "More",
	tabBarOptions: {
		activeTintColor: '#00b762',
		inactiveTintColor: 'black',
		showLabel: false,
	},
});

const LocalityStack = createStackNavigator({
	Locality: {
		screen: SelectLocality
	},
	SearchAddress: {
		screen: SearchAddressScreen
	},
	AddressAutocomplete: {
		screen: AddressAutocompleteScreen
	}
}, {
	initialRouteName: 'Locality',
})

const Navigations = createAppContainer(
	createSwitchNavigator({
		AuthLoading: AuthLoadingScreen,
		LoginStack: LoginStack,
		MainStack: TabNavigator,
		OrderStatusStack: OrderStatusStack,
		OtherLoginStack: OtherLoginNavigator,
		MainOrderStack: OrdersStack,
		Cart: CartStack,
		LocalityStack: LocalityStack
	}, {
		initialRouteName: "AuthLoading"
	}
	)
);

const store = configureStore();

export default class App extends Component {

	componentWillMount() {
		const iosConfig = {
			clientId: '348600376683-ruvv1fdlf8c43229sbqdoo7q85m78q36.apps.googleusercontent.com',
			appId: '1:348600376683:ios:b0f7da9f29a8b076',
			apiKey: 'AIzaSyDSnWV4dyN6lNhIXKAjL82H83dFa9tBL-k',
			databaseURL: 'https://aapkidokan-4c311.firebaseio.com/',
			storageBucket: 'aapkidokan-4c311.appspot.com',
			messagingSenderId: '348600376683',
			projectId: Constants.projectId,
			// enable persistence by adding the below flag
			persistence: true,
		}

		const androidConfig = {
			clientId: '348600376683-oiaropopfnhhotimv914cdfak6fco4f3.apps.googleusercontent.com',
			appId: '1:348600376683:android:b0f7da9f29a8b076',
			apiKey: 'AIzaSyDr0bnFXpvvpRajSxQYdIx5TkscFtVSbrM',
			databaseURL: 'https://aapkidokan-4c311.firebaseio.com/',
			storageBucket: 'aapkidokan-4c311.appspot.com',
			messagingSenderId: '348600376683',
			projectId: Constants.projectId,
			// enable persistence by adding the below flag
			persistence: true,
		}

		firebase.initializeApp(
			Platform.OS === 'ios' ? iosConfig : androidConfig,
			'taxiyefood'
		);
	}

	showNotification(data) {
		console.log("ShowingNotification:" + JSON.stringify(data))
		console.log("NOTI_ID:" + Math.round((new Date()).getTime() / 1000).toString())
		const channel = new firebase.notifications.Android.Channel('Aapkidokan', 'Aapkidokan', firebase.notifications.Android.Importance.Max)
		firebase.notifications().android.createChannel(channel);
		const notification = new firebase.notifications.Notification()
			.setNotificationId(Math.round((new Date()).getTime() / 1000).toString())
			.setTitle(data.notificationTitle)
			.setBody(data.notificationBody)
			.android.setBigText(data.notificationBody)
			.setSound("default")
			.android.setAutoCancel(true)
			.setData(data)
			.android.setChannelId('Aapkidokan')

		firebase.notifications().displayNotification(notification).then(() => console.log("Show")).catch(err => console.error("error " + err));
	}

	componentDidMount() {
		Geocoder.setApiKey(Constants.GOOGLE_API_KEY);

		/*
    * Triggered when a particular notification has been received in foreground
    * */
		this.notificationListener = firebase.notifications().onNotification((res) => {
			console.log("NOTI_onNotification:", res)

			AsyncStorage.getItem("user").then(result => {
				if (res && res.data && result) {
					let user = JSON.parse(result);
					let data = res.data;
					if (data.type && user && user._id) {
						// if (data.type == 'customer' || data.type == 'driver') {
						// console.log("received initial notification " + JSON.stringify(data));
						DeviceEventEmitter.emit("ORDER_NOTIFICATION")
						DeviceEventEmitter.emit("ORDER_DETAIL_NOTIFICATION", { orderId: data.order_id })
						// }
						this.showNotification(data)
					}
				}
			})
		});

		firebase
			.notifications()
			.getInitialNotification()
			.then(res => {
				if (res) {
					let notification = res.notification;
					console.log("received initial notification " + JSON.stringify(notification.data));
					if (notification.data && notification.data.type) {
						notification.data.type = parseInt(notification.data.type);
						this.handleNavigation(notification.data);
					}
				}
			});

		this.notificationOpenedListener = firebase
			.notifications()
			.onNotificationOpened((res) => {
				if (res) {
					let notification = res.notification;
					console.log("received opened notification " + JSON.stringify(notification.data));
					if (notification.data && notification.data.type) {
						notification.data.type = parseInt(notification.data.type);
						this.handleNavigation(notification.data);
					}
				}
			});
		// this.handleRouting();
	}

	handleNavigation = (data) => {
		let routeName = "Home";
		AsyncStorage.getItem("user").then(res => {
			// console.log(res);
			if (res) {
				let user = JSON.parse(res);
				if (user && user._id && data.type == 2 && data.order_id) {
					routeName = "MainOrders"
					NavigationService.navigate("MainOrderDetails", { order_id: data.order_id })
				} else {
					NavigationService.navigate(routeName);
				}
			}
		})
	}

	render() {
		return (
			<Provider store={store}>
				<Navigations
					ref={navigatorRef => {
						NavigationService.setTopLevelNavigator(navigatorRef);
					}} />
			</Provider>
		)
	}
}