import React, { Component } from 'react'
import { Platform, View, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Images from '../../global/images';
import constants from '../../constants/constants';
import NavigationService from '../../global/NavigationService';

export default class AddressAutocomplete extends Component {

	static navigationOptions = ({ navigation }) => ({
		header: null,

	});

	constructor() {
		super()
	}

	goBack = (address, location) => {
		const { navigation } = this.props;
		navigation.goBack();
		navigation.state.params.onSelect({
			address: address,
			addressForAutoSearch: address,
			region: {
				latitude: location.lat,
				longitude: location.lng
			}
		});
	}

	saveLocation = (address, location) => {
		let data = {
			address: address,
			latitude: location.latitude,
			longitude: location.longitude
		};
		console.log(data);
		// this.props.locationSelected()
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: constants.green }} forceInset={{ top: 'always' }}
			>
				<View style={{
					flex: 1, shadowColor: '#000',
					shadowOffset: { width: 0, height: 2 },
					shadowOpacity: 0.2,
					shadowRadius: 2,
					elevation: 8,
					backgroundColor: constants.backgroundPageColor
				}}>
					<GooglePlacesAutocomplete
						minLength={2}
						autoFocus={false}
						returnKeyType={'search'}
						fetchDetails={true}
						placeholder={"Search Your Location . . ."}
						placeholderTextColor="white"
						nearbyPlacesAPI='GooglePlacesSearch'
						query={{
							// available options: https://developers.google.com/places/web-service/autocomplete
							key: constants.GOOGLE_API_KEY,
							language: 'en', // language of the results
							// types: 'geocode'
						}}
						onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
							console.log('DATA ' + JSON.stringify(data.description));
							console.log('DETAILS ' + JSON.stringify(details.geometry.location));
							this.goBack(data.description, details.geometry.location)
							// this.saveLocation(data.description, details.geometry.location)
						}}
						styles={{
							textInputContainer: {
								borderTopWidth: 0,
								borderBottomWidth: 0,
								paddingLeft: 44,
								backgroundColor: constants.green,
							},
							textInput: {
								height: 44,
								color: 'white',
								fontSize: 16,
								backgroundColor: constants.green,
								borderRadius: 0,
								paddingTop: 0,
								paddingBottom: 0,
								paddingLeft: 10,
								paddingRight: 10,
								marginTop: 0,
								marginLeft: 0,
								marginRight: 0,
								fontFamily: constants.fonts.regular
							},
							predefinedPlacesDescription: {
								color: 'white',
								fontFamily: constants.fonts.regular
							},
						}}
						currentLocation={false}
					/>
					<TouchableOpacity
						onPress={() => {
							console.log("Going back");
							NavigationService.goBack();
						}}
						style={{
							backgroundColor: constants.green,
							width: 44,
							height: 44,
							alignItems: 'flex-start',
							justifyContent: 'center',
							paddingLeft: 4,
							position: 'absolute',
							top: 0,
							left: 0,
						}}>
						<Image source={Platform.OS == 'ios' ? Images.ic_back_ios : Images.ic_back_android} style={{ width: 24, height: 24 }}></Image>

					</TouchableOpacity>
				</View>
			</SafeAreaView>
		);
	};
}