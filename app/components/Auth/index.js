
import React, { Component } from 'react';
import { ImageBackground, StatusBar } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import splash from './../../assets/images/splash.png'
import constants from '../../constants/constants';
// import firebase from 'react-native-firebase';
import NavigationService from '../../global/NavigationService';
import SplashScreen from 'react-native-splash-screen';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class AuthScreen extends Component {
	constructor() {
		super();
		console.log("checking auth page");
		this.state = {
			eventNumber: null,
			postList: null,
			isHidden: true,
			loadPost: false,
			title: null,
			userId: null
		}
	}

	componentWillMount = () => {
		// AsyncStorage.clear();
		// AsyncStorage.removeItem("store");
		// AsyncStorage.removeItem("city");
		// AsyncStorage.removeItem("area");
		// AsyncStorage.removeItem("user");
		// AsyncStorage.removeItem("selectAddress");
		// AsyncStorage.removeItem("selectedSlot");
		AsyncStorage.multiGet(constants.routesData).then((data) => {
			constants.store = data[0][1] ? JSON.parse(data[0][1]) : {};
			constants.area = data[1][1] ? JSON.parse(data[1][1]) : {};
			constants.city = data[2][1] ? JSON.parse(data[2][1]) : {};
			constants.selectAddress = data[3][1] ? JSON.parse(data[3][1]) : {};;
			constants.user = data[4][1] ? JSON.parse(data[4][1]) : {};
			constants.coupon = data[5][1] ? JSON.parse(data[5][1]) : {};
			constants.taxes = data[6][1] ? JSON.parse(data[6][1]) : {};
			constants.delivery_charges = data[7][1] ? JSON.parse(data[7][1]) : [];
			constants.FCM_TOKEN = data[8][1] ? data[8][1] : "";
			constants.cartStore = data[9][1] ? JSON.parse(data[9][1]) : {};
			constants.cartArea = data[10][1] ? JSON.parse(data[10][1]) : {};
			if (!constants.cartArea && constants.area) {
				constants.cartArea = constants.area
			}
			constants.cartCity = data[11][1] ? JSON.parse(data[11][1]) : {};
			if (!constants.cartCity && constants.city) {
				constants.cartCity = constants.city
			}
			this.props.setCart(data[12][1] ? JSON.parse(data[12][1]) : [])
			// if (!data[8][1]) this.checkPermission();
			SplashScreen.hide();
			// NavigationService.navigate("OtherLoginStack");
			if (!constants.city._id || !constants.area._id) {
				NavigationService.navigate("SelectLocation");
				// NavigationService.navigate("LocalityStack");
			} else if (!constants.store._id) {
				NavigationService.navigate("SetStore");
			} else {
				NavigationService.navigate("MainStack");
			}
		});
	}

	render() {
		console.log("checking auth");
		console.disableYellowBox = true;
		return (
			<ImageBackground source={splash} style={{ width: '100%', height: '100%' }} resizeMode="stretch">
				<StatusBar backgroundColor={constants.green} barStyle="light-content" />
			</ImageBackground>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(null, mapDispatchToProps)(AuthScreen)