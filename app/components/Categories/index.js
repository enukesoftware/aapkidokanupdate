import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Alert, Animated, StatusBar, Image, TouchableHighlight, Platform } from 'react-native';
import { Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import constants from '../../constants/constants';
import Images from '../../global/images';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import CardView from 'react-native-cardview'
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';

export default class Categories extends Component {
	static navigationOptions = {
		header: null,
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			categories: [1],
			storeData: {},
			loading: false,
			totalCartItem: 0,
			headerHeight: 0,
			storeChanged: false
		};
	}

	componentWillMount = () => {
		console.log("Home page");
		this.calculateCartItem();
		this.getStoresAndCategories();
	}

	changedStoreCheck = () => {
		if (this.state.storeChanged) {
			Alert.alert(
				'Alert',
				'Your basket has products from another store. Clear basket to shop from this store?',
				[
					{
						text: 'CLEAR',
						onPress: () => {
							AsyncStorage.removeItem("cart").then(() => {
								// this.addProduct(index, type)
								this.calculateCartItem();
							})
						},
						style: 'cancel',
					},
					{
						text: 'CANCEL',
						onPress: () => console.log('Cancel Pressed')
					}
				],
				{ cancelable: false },
			);
		} else this.goToCart();
	}

	getStoresAndCategories = () => {
		this.setState({ loading: true });
		AsyncStorage.getItem("storeData").then((data) => {
			console.log(JSON.parse(data))
			let result = JSON.parse(data)
			this.setState({ storeData: result }, () => {
				ApiManager.getCategories().then((res) => {
					console.log(res.data)
					setTimeout(() => {
						this.setState({ loading: false }, () => console.log("loading paused"))
					}, 100);
					setTimeout(() => {
						if (res && res.success && res.code == 200) {
							this.setState({ categories: res.data.categories }, () => {
								this.calculateCartItem();
							})
						} else {
							Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
						}
					}, 500);
				});
			})
		})
	}

	renderCategoryList = () => {
		return (
			<View style={{
				flexDirection: "row",
				flexWrap: 'wrap',
				marginBottom: 50
			}}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white" }}
				/>
				{
					this.state.categories.map((category, index) =>
						(
							<TouchableOpacity key={index} style={{
								backgroundColor: "white",
								height: 170,
								width: "45%",
								borderRadius: 10,
								shadowColor: 'dimgrey',
								shadowOffset: { width: 0, height: 1 },
								shadowOpacity: .5,
								shadowRadius: 2,
								elevation: 2,
								margin: "2.5%",
								alignItems: "center",
							}} onPress={() => {
								constants.previousStateForSubCategory = this.props.navigation.state.routeName;
								constants.category = category;
								NavigationService.navigate("SubCategory");
							}
							}>
								<Image source={{ uri: constants.imageBaseURL + category.picture }}
									style={{ width: "100%", height: 120, resizeMode: "contain" }}></Image>
								<View style={{
									height: 40, width: "100%", paddingLeft: 3, paddingRight: 3,
									justifyContent: "center", alignItems: "center"
								}}>
									<Text style={{
										fontSize: 14, color: "black", textAlign: "center",
										fontFamily: constants.fonts.medium
									}} numberOfLines={2}>{category.name} </Text>
								</View>

							</TouchableOpacity>
						))
				}
			</View>
		)
	}

	calculateCartItem = () => {
		AsyncStorage.getItem("cart").then((data) => {
			let cart = data ? JSON.parse(data) : [];
			let counter = 0;
			for (let i = 0; i < cart.length; i++) {
				counter = counter + cart[i].count;
			}
			this.setState({ totalCartItem: counter }, () => {
				console.log("total items" + this.state.totalCartItem);
			});
		})
	}

	goToCart = () => {
		constants.previousStateForCart = this.props.navigation.state.routeName;
		if (this.props.navigation.state.routeName == "AllCategories") {
			NavigationService.navigate("Cart");
		} else {
			NavigationService.navigate("CategoryCart");
		}
	}

	render() {
		const Screen =
		{
			heignt: Dimensions.get('window').height,
			width: Dimensions.get('window').width,
			scale: Dimensions.get('window').scale,
			fontScale: Dimensions.get('window').fontScale,
		};
		let animatBackgroundColor = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: ['transparent', '#1faf4b'],
			extrapolate: 'clamp'
		});
		let translateY = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: [0, -((Screen.heignt / 3) - (getStatusBarHeight(false) + 44))],
			extrapolate: 'clamp',
		});

		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<NavigationEvents onWillFocus={() => {
					this.getStoresAndCategories();
					const store = constants.store;
					AsyncStorage.getItem("cart").then((data) => {
						let cart = data ? JSON.parse(data) : [];
						let changedStore = false;
						for (let i = 0; i < cart.length; i++) {
							if (store._id != cart[i].store_id) {
								changedStore = true;
								break;
							}
						}
						this.setState({ storeChanged: changedStore }, () => {
							console.log("store changed:--" + this.state.storeChanged);
						});
					});
					//Call whatever logic or dispatch redux actions and update the screen!
				}}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					contentContainerStyle={{ paddingTop: ((Screen.heignt / 3) - this.state.headerHeight) + 45 }}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderCategoryList()}
				</Animated.ScrollView>

				<Animated.Image style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					zIndex: -1,
					height: Screen.heignt / 3.5,
					width: '100%',
					alignItems: 'center',
					justifyContent: 'flex-end',
					transform: [{ translateY }],
					backgroundColor: '#f5f5f5'
				}}
					resizeMode='stretch'
					source={Images.backgroundHeader}
				/>
				<Animated.View style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: Screen.heignt / 3.4,
					maxHeight: 230,
					backgroundColor: 'transparent',
					alignItems: 'center',
					justifyContent: 'center',
					transform: [{ translateY }],
					paddingTop: this.state.headerHeight + 45,
					paddingBottom: 10
				}}>
					<View style={{ alignItems: 'center', justifyContent: 'center' }}>
						<CardView cardElevation={2}
							cardMaxElevation={2}
							cornerRadius={10}
							style={{
								width: 180,
								height: 85,
								justifyContent: "center",
								alignItems: "center"
							}}>
							<Image style={{
								width: 175, height: 80,
								resizeMode: "stretch",
								borderRadius: 7,
							}} source={{ uri: constants.imageBaseURL + this.state.storeData.picture }} />
						</CardView>
						<TouchableOpacity style={{
							marginTop: 7,
							padding: 10,
							flexDirection: "row"
						}} >
							<Text style={{
								color: 'black',
								fontSize: 18,
								color: "black", fontFamily: constants.fonts.medium,
							}}>{this.state.storeData.name}</Text>
						</TouchableOpacity>
					</View>
				</Animated.View>

				<Animated.View style={{
					top: 0, left: 0, right: 0,
					position: 'absolute', flexDirection: 'row',
					backgroundColor: animatBackgroundColor,
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: { height: (getStatusBarHeight(true) + 30), }
					})
				}}>
					<Animated.View style={{
						height: '100%', width: "15%",
						paddingLeft: 15, justifyContent: 'center',
						alignItems: 'center',
						// backgroundColor: "red",
						position: "absolute",
						left: 0,
						bottom: 0,
						zIndex: 1
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						{this.props.navigation.state.routeName == "AllCategories" ?
							<TouchableHighlight style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 40 }, android: { marginTop: 10 } }) }} underlayColor='transparent' onPress={() => {
								NavigationService.goBack();
							}}>
								<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.back} />
							</TouchableHighlight> : null
						}
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						justifyContent: 'center',
						width: "100%",
						marginLeft: this.props.navigation.state.routeName == "AllCategories" ? "15%" : 20
						//  alignItems: "center",
						// backgroundColor: "blue"
					}}>
						<Text style={{
							fontSize: 18, color: 'white',
							fontFamily: constants.fonts.bold, textTransform: "capitalize",
							...Platform.select({ ios: { marginTop: 30 }, android: {} })
						}}>Categories</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						width: '25%',
						maxWidth: 90,
						// backgroundColor: "green",
						justifyContent: 'space-between',
						alignItems: 'center',
						flexDirection: 'row',
						position: "absolute",
						right: 0,
						bottom: 0,
						zIndex: 1
					}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							...Platform.select({
								ios: { marginTop: 40 },
								android: { marginTop: 10 }
							}),
							marginLeft: 10
						}} underlayColor='transparent' onPress={() => {
							NavigationService.navigate("Search")
						}}>
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.search} />
						</TouchableHighlight>
						<TouchableOpacity style={{
							height: 35,
							width: 35,
							...Platform.select({
								ios: { marginTop: 40 },
								android: { marginTop: 10 }
							})
						}} underlayColor='transparent' onPress={() => {
							{
								this.state.totalCartItem ?
									this.changedStoreCheck() :
									Alert.alert("Please add few items in cart");
							}
						}}>
							{
								this.state.totalCartItem ?
									<View style={{
										backgroundColor: "#e9001f",
										width: "auto",
										height: "auto",
										paddingTop: this.state.storeChanged ? 5 : 2,
										paddingBottom: this.state.storeChanged ? 4 : 3,
										paddingLeft: 7,
										paddingRight: 8,
										position: "absolute",
										zIndex: 999,
										top: -12,
										left: -8,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: 15
									}}>
										{
											this.state.storeChanged ?
												<Image source={Images.closeImg} style={{ height: 10, width: 10 }} /> :
												<Text style={{
													color: "white",
													fontFamily: constants.fonts.bold, fontSize: 10
												}}>{this.state.totalCartItem}</Text>
										}
									</View> : null
							}
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
						</TouchableOpacity>
					</Animated.View>
				</Animated.View>
				<StatusBar backgroundColor="#1faf4b" barStyle="light-content" />
			</View >
		)
	}
}