
import React, { Component } from 'react';
import { Image, View, Text, Dimensions, ScrollView, findNodeHandle, TextInput, TouchableOpacity, ImageBackground, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import styles from '../../../app/global/styles';
import constants from '../../../app/constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
import Images from '../../global/images';

class Otp extends Component {
	static navigationOptions = () => ({
		header: null
	});

	state = {
		otp: '',
		verification_token: '',
		contact_number: '',
		loading: false
	}

	componentDidMount = () => {
		let params = this.props.navigation.state.params;
		console.log("Otp sent" + JSON.stringify(params.data));
		this.setState({
			loading: false,
			verification_token: params.data.verification_token,
			contact_number: params.data.contact_number
		}, () => {
			console.log("state is :---" + JSON.stringify(this.state));
		});
	}

	verifyOtp = () => {
		this.setState({ loading: true });
		ApiManager.verifyOtp(this.state).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				if (res && res.success && res.code == 200) {
					console.log("Otp succes" + JSON.stringify(res));
					if (this.props.navigation.state.routeName == "OtherOtp") {
						NavigationService.navigate("MainStack", { logined: true });
					} else if (this.props.navigation.state.routeName == "SendOtp") {
						AsyncStorage.removeItem("user");
						NavigationService.navigate("ResetPassword");
					} else if (this.props.navigation.state.routeName == "OtherSendOtp") {
						AsyncStorage.removeItem("user");
						NavigationService.navigate("OtherResetPassword");
					} else {
						NavigationService.navigate(constants.previousStateForLogin, { logined: true });
					}
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				Alert.alert(err.singleStringMessage);
			}, 500);
			console.log("Otp error" + JSON.stringify(err));
		})
	}

	resendOtp = () => {
		this.setState({ loading: true });
		ApiManager.resendOtp({ contact_number: this.state.contact_number }).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				if (res.success && res.code == 200) {
					Toast.showWithGravity(res.data.message, Toast.LONG, Toast.BOTTOM);
					this.setState({ verification_token: res.data.verification_token }, () => {
						console.log(JSON.stringify(this.state));
					})
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.LONG, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false });
			console.log(JSON.stringify(err));
		})
	}

	inputFocused = (ref) => {
		this._scroll(ref, 100);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	render() {
		console.log(this.props.navigation.state.routeName);
		return (
			<KeyboardAwareView animated={true}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<ImageBackground source={Images.background} style={{ flex: 1 }}>
					<ScrollView showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}
						ref="myScrollView" keyboardDismissMode='interactive'>
						<View style={{ height: screenHeight, width: screenWidth, justifyContent: "center", alignItems: "center" }}>
							<View style={[styles.innerSection, { minHeight: 250, paddingBottom: 30 }]}>
								<View style={styles.logoSection}>
									<Image source={Images.logo} style={styles.logo}></Image>
								</View>
								<Text style={[styles.innerlabel, { fontSize: 15, marginBottom: 15 }]}>Enter OTP to
						{
										(this.props.navigation.state.routeName == "SendOtp" || this.props.navigation.state.routeName == "OtherSendOtp") ?
											" Reset Your Password" : " Verify Your Account"
									}
								</Text>
								<View style={styles.inputSection}>
									<Image source={Images.setting} style={styles.inputLogo} />
									<TextInput
										style={styles.input}
										value={this.state.otp}
										placeholder='Enter OTP'
										placeholderTextColor={constants.placeHolderColor}
										onChangeText={(text) => { this.setState({ otp: text }) }}
										returnKeyType="done"
										autoCapitalize="none"
										ref="input"
										onFocus={this.inputFocused.bind(this, 'input')}
										onSubmitEditing={() => {
											if (!this.state.otp || !this.state.otp.length) {
												Alert.alert("Please enter your OTP");
											} else this.verifyOtp();
										}}
									/>
								</View>
								<TouchableOpacity style={styles.loginButton}
									onPress={() => {
										if (!this.state.otp || !this.state.otp.length) {
											Alert.alert("Please enter your OTP");
										} else this.verifyOtp();
									}}>
									<Text style={[styles.loginText, { color: "white" }]}>VERIFY OTP</Text>
								</TouchableOpacity>
								<TouchableOpacity style={[styles.forgotLine, { marginTop: 5 }]} onPress={() => this.resendOtp()}>
									<Text style={{ color: "black", fontFamily: constants.fonts.bold, color: constants.black }}>Resend OTP</Text>
								</TouchableOpacity>
							</View>
						</View>
					</ScrollView>
				</ImageBackground>
			</KeyboardAwareView>
		)
	}
}

export default Otp;