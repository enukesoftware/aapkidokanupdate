
import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, ImageBackground, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import styles from '../../global/styles';
import constants from '../../constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import { Dropdown } from 'react-native-material-dropdown';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';

class SelectLocation extends Component {
	static navigationOptions = () => ({
		header: null
	});
	state = {
		cities: [],
		areas: [],
		logined: false,
		loading: false,
		city: {
			"_id": "",
			"name": "",
			"created_at": "",
			"updated_at": "",
			"__v": 0
		},
		area: {
			"_id": "",
			"name": "",
			"created_at": "",
			"updated_at": "",
			"__v": 0
		}
	}

	refreshPage = () => {
		this.setState({
			city: {
				"_id": "",
				"name": "",
				"created_at": "",
				"updated_at": "",
				"__v": 0
			},
			area: {
				"_id": "",
				"name": "",
				"created_at": "",
				"updated_at": "",
				"__v": 0
			},
			// cities: [],
			areas: [],
		}, () => {
			console.log("Store refreshed");
		})
	}

	componentWillMount = () => {
		let params = this.props.navigation.state.params;
		if ((params && params.logined) || (constants.user._id)) {
			this.setState({ logined: true });
		}
		console.log(params);
		this.getCities();
	}

	getCities = () => {
		this.setState({ loading: true }, () => {
			ApiManager.getCities().then((res) => {
				setTimeout(() => {
					this.setState({ loading: false });
				}, 100)
				setTimeout(() => {
					if (res.singleStringMessage) {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					} else {
						this.setState({ cities: res.data.cities }, () => console.log("Cities set"))
					}
				}, 500);
			}).catch((err) => {
				this.setState({ loading: false });
				Toast.showWithGravity(err.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				console.log("Otp error" + JSON.stringify(err));
			})
		});
	}

	getAreas = () => {
		this.setState({ loading: true }, () => {
			ApiManager.getAreas(this.state.city).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false });
				}, 100)
				setTimeout(() => {
					if (res.singleStringMessage) {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					} else {
						this.setState({ areas: res.data.city.areas }, () => console.log("areas set"))
					}
				}, 500);
			}).catch((err) => {
				this.setState({ loading: false });
				Toast.showWithGravity(err.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				console.log("Otp error" + JSON.stringify(err));
			})
		});
	}

	setLocations = async () => {
		constants.area = this.state.area;
		constants.city = this.state.city;
		// await AsyncStorage.setItem("area", JSON.stringify(this.state.area));
		// await AsyncStorage.setItem("city", JSON.stringify(this.state.city));
		// if (this.props.navigation.state.routeName == "SelectLocationInHome") {
		// 	NavigationService.navigate("SetStoreInHome");
		// } else {
		// 	NavigationService.navigate("SetStore");
		// }
		AsyncStorage.multiSet([['area', JSON.stringify(this.state.area)], ['city', JSON.stringify(this.state.city)]], () => {
			if (this.props.navigation.state.routeName == "SelectLocationInHome") {
				NavigationService.navigate("SetStoreInHome");
			} else {
				NavigationService.navigate("SetStore");
			}
		});
	}

	render() {
		console.log("Settings");
		return (
			<ImageBackground source={Images.background} style={{ flex: 1 }}>
				<NavigationEvents
					onWillFocus={() => {
						this.refreshPage();
						//Call whatever logic or dispatch redux actions and update the screen!
					}}
				/>
				<View style={styles.outerSection}>
					<Spinner
						size="large"
						visible={this.state.loading}
						color={constants.green}
						textContent={'Please wait . . .'}
						textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
					/>
					<View style={[styles.innerSection, { minHeight: 200, paddingBottom: 30 }]}>
						<View style={styles.logoSection}>
							<Image source={Images.logo} style={styles.logo}></Image>
						</View>
						<Text style={[styles.innerlabel, { fontSize: 15, marginTop: 15 }]}>Smart Way of Shopping</Text>
						<View style={[styles.inputSection, { marginTop: 20 }]}>
							<Image source={Images.world} style={styles.inputLogo} />
							<Dropdown
								placeholder="Select Your City"
								placeholderTextColor={constants.placeHolderColor}
								data={this.state.cities}
								value={this.state.city.name}
								containerStyle={{
									marginLeft: 10, width: "80%",
									marginTop: -20,
								}}
								inputContainerStyle={{ borderBottomColor: 'transparent' }}
								pickerStyle={{
									width: "80%",
									marginLeft: -40, marginTop: 40
								}}
								onChangeText={(value, index) => {
									this.setState({ city: this.state.cities[index] }, () => {
										this.getAreas();
									});
								}}
							/>
						</View>
						<View style={[styles.inputSection]}>
							<Image source={Images.location} style={styles.inputLogo} />
							<Dropdown
								placeholder="Select Your Delivery Area"
								placeholderTextColor={constants.placeHolderColor}
								data={this.state.areas}
								value={this.state.area.name}
								containerStyle={{
									width: "80%",
									marginLeft: 10,
								}}
								inputContainerStyle={{
									borderBottomColor: 'transparent',
								}}
								pickerStyle={{
									width: "80%",
									marginLeft: -40, marginTop: 40,
								}}
								dropdownOffset={{ top: 10, left: 0 }}
								onChangeText={(value, index) => {
									this.setState({ area: this.state.areas[index] });
								}}
							/>
						</View>
						<TouchableOpacity style={styles.loginButton}
							onPress={() => {
								if (!this.state.city._id || !this.state.city._id.length) {
									Alert.alert("Please enter your city");
								} else if (!this.state.area._id || !this.state.area._id.length) {
									Alert.alert("Please enter your area");
								} else this.setLocations()
							}}>
							<Text style={[styles.loginText, { color: "white" }]}>START SHOPPING</Text>
						</TouchableOpacity>
						{
							!this.state.logined ?
								<TouchableOpacity style={[styles.forgotLine, { marginTop: 5 }]}
									onPress={() => NavigationService.navigate("Login")}>
									<Text style={{
										color: "black",
										color: "black", fontFamily: constants.fonts.regular
									}}>Already have an account?</Text>
									<Text style={{ color: "black", fontFamily: constants.fonts.semiBold, marginLeft: 10 }}>Log In</Text>
								</TouchableOpacity> : null
						}
					</View>
				</View>
			</ImageBackground>
		)
	}
}

export default SelectLocation;