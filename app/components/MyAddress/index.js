import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity, Animated, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Images from '../../global/images';
import CardView from 'react-native-cardview';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ApiManager from '../../apimanager/ApiManager';
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';

export default class MyAddressScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};
	state = {
		selectedIndex: 0,
		scrollY: new Animated.Value(0.01),
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		loading: false,
		addressList: [],
		totalCartItem: 0
	};

	refreshPage = () => {
		this.setState({ loading: true });
		ApiManager.getAddressList().then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					if (constants.selectAddress && res.data.address) {
						for (let i = 0; i < res.data.address.length; i++) {
							if (res.data.address[i]._id == constants.selectAddress._id) {
								this.setState({ selectedIndex: i });
								break;
							}
						}
					}
					this.setState({ addressList: res.data.address ? res.data.address : [] }, () => {
						console.log(this.state.addressList);
					});
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			});
		});
	}

	calculateCartItem = () => {
		AsyncStorage.getItem("cart").then((data) => {
			let cart = data ? JSON.parse(data) : [];
			let counter = 0;
			for (let i = 0; i < cart.length; i++) {
				counter = counter + cart[i].count;
			}
			this.setState({ totalCartItem: counter }, () => {
				console.log("total items" + this.state.totalCartItem);
			});
		})
	}

	goToAddressForm = (index) => {
		if (typeof (index) != 'undefined') {
			NavigationService.navigate("AddressForm", { address: this.state.addressList[index] })
		} else {
			NavigationService.navigate("AddressForm")
		}
	}

	deleteAddress = (index) => {
		this.setState({ loading: true })
		ApiManager.deleteAddress(this.state.addressList[index]._id).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					console.log(res);
					Toast.showWithGravity('Address deleted successfully . . .', Toast.SHORT, Toast.CENTER);
					this.refreshPage();
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
				}
			}, 500)
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			})
		});
	}

	renderAddressPage = () => {
		return (
			<View style={{
				...Platform.select({
					ios: { marginTop: (getStatusBarHeight(true) + 40), },
					android: { marginTop: 48 }
				}),
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-start',
				backgroundColor: constants.backgroundPageColor,
				marginLeft: 15,
				marginRight: 15
			}}>
				<ScrollView style={{ width: "100%" }}>
					{
						this.state.addressList.map((address, index) => (
							< CardView key={index} cardElevation={2}
								cardMaxElevation={2}
								cornerRadius={5}
								style={{
									height: "auto",
									width: "100%",
									backgroundColor: "white",
									borderColor: constants.backgroundPageColor,
									marginBottom: 25,
									padding: 15,
									paddingRight: 0,
									minHeight: 140,
									justifyContent: "space-between"
								}}>
								<View style={{
									flexDirection: "row",
									justifyContent: "space-between"
								}}>
									<View style={{
										width: this.state.screenWidth - 220,
										flexDirection: "row",
									}}>
										<Text style={{
											color: "black", fontFamily: constants.fonts.bold,
											fontSize: 18, color: "black"
										}} numberOfLines={1}>{address.full_name}</Text>
									</View>
									<View style={{
										width: 155,
										flexDirection: "row",
										justifyContent: "space-between"
									}}>
										<View style={{
											width: 55,
											backgroundColor: constants.lightgreen,
											justifyContent: "center",
											height: 24,
											alignItems: "center",
											borderRadius: 15,
										}}>
											<Text style={{ fontSize: 10, color: "black", fontFamily: constants.fonts.medium, textTransform: 'uppercase' }}>{address.alias}</Text>
										</View>
										<TouchableOpacity style={{
											width: 40,
											height: 40,
											marginTop: -10,
											justifyContent: "center",
											alignItems: "center"
										}} onPress={() => {
											AsyncStorage.setItem("selectAddress", JSON.stringify(this.state.addressList[index])).then(() => {
												this.setState({ selectedIndex: index }, () => {
													constants.selectAddress = this.state.addressList[index];
												});
											})
										}}>
											<View style={{
												width: 20,
												height: 20,
												borderColor: this.state.selectedIndex == index ? constants.green : "lightgrey",
												borderWidth: this.state.selectedIndex == index ? 5 : 2,
												borderRadius: 15
											}}></View>
										</TouchableOpacity>
										<TouchableOpacity style={{
											width: 30,
											height: 30,
											// backgroundColor: "red",
											// justifyContent: "center",
											alignItems: "center"
										}} onPress={() => this.goToAddressForm(index)}>
											<Image source={Images.edit} style={{ width: 18, height: 18 }}></Image>
										</TouchableOpacity>
										<TouchableOpacity style={{
											width: 30,
											height: 30,
											// backgroundColor: "blue",
											// justifyContent: "center",
											alignItems: "center"
										}} onPress={() => this.deleteAddress(index)}>
											<Image source={Images.delete} style={{ width: 18, height: 18 }}></Image>
										</TouchableOpacity>
									</View>
								</View>
								<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 15, color: "black" }}>
									Phone : {address.contact_number}
								</Text>
								<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 15, color: "black" }}>
									Email : {address.email}
								</Text>
								<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 15, color: "black" }}>
									What3Words : {address.what_3_words ? address.what_3_words : "N/A"}
								</Text>
								<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 15, color: "black" }}>
									{address.house_no + ', ' + address.city.name + ', ' + address.locality + (address.landmark ? (', ' + address.landmark) : '')}
								</Text>
							</CardView>
						))
					}
					<View style={{
						width: "100%",
						height: 70,
						justifyContent: "center",
						alignItems: "center",
						marginBottom: 50
					}}>
						<TouchableOpacity style={{
							backgroundColor: constants.green,
							height: 40,
							width: 150,
							borderRadius: 30,
							justifyContent: "center",
							alignItems: "center"
							// padding: 15
						}} onPress={() => this.goToAddressForm()}>
							<Text style={{
								color: "white",
								fontSize: 16,
								fontFamily: constants.fonts.bold
							}}>ADD ADDRESS</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</View>
		)
	}

	render() {
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<NavigationEvents onWillFocus={() => {
					this.refreshPage();
					//Call whatever logic or dispatch redux actions and update the screen!
				}} />
				<Spinner
					size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderAddressPage()}
				</Animated.ScrollView>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: { height: (getStatusBarHeight(true) + 40), }
					}),
					backgroundColor: '#1faf4b',
					paddingBottom: 10
				}}>
					<Animated.View style={{
						height: '100%', width: '10%',
						minWidth: 60,
						minHeight: 60,
						justifyContent: 'center',
						alignItems: 'center',
						position: "absolute",
						left: 0,
						top: 0,
						zIndex: 2,
						paddingLeft: 10
					}}>
						<TouchableOpacity style={{
							height: 45, width: 45,
							...Platform.select({
								ios: { marginTop: 50 },
								android: { marginTop: 19 }
							}),
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								NavigationService.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableOpacity>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 60,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: this.state.screenWidth - 60,
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<Text style={{
							fontSize: 18, color: 'white',
							fontFamily: constants.fonts.bold,
							// backgroundColor: "red"
						}}>My Address</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}