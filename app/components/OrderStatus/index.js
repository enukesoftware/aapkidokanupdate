
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, Dimensions, StyleSheet, StatusBar, Image } from 'react-native'
import constants from '../../../app/constants/constants';
import moment from 'moment';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';

class OrderStatus extends Component {
	static navigationOptions = ({ navigation }) => ({
		header: null
	});

	state = {
		order: {
			// is_express_delivery: true,
			deliver_end_time: new Date(),
			// deliver_start_time: new Date()
		}
	}

	componentDidMount = () => {
		this.setState({
			order: constants.order._id ? constants.order : {}
		}, () => {
			console.log(this.state.order);
			constants.order = {};
		})
	}

	render() {
		console.log("Order status");
		return (
			<SafeAreaView forceInset={{ bottom: 'never' }} style={{
				flex: 1,
				backgroundColor: constants.green,
				justifyContent: "flex-start",
				alignItems: "center"
			}} >
				<View style={{
					flex: 1, justifyContent: "center",
					alignItems: "center",
					backgroundColor: "white",
					width: Dimensions.get("window").width,
					height: Dimensions.get("window").height,
					// backgroundColor: constants.backgroundPageColor
				}}>
					<Image source={Images.smiley}></Image>
					<Text style={{
						color: "black",
						fontSize: 16, textAlign: "center",
						fontFamily: constants.fonts.bold,
						margin: 5,
						textTransform: "uppercase",
						marginTop: 30
					}}>
						Thank You For Shopping With Us</Text>
					<Text style={{
						color: "black",
						fontSize: 16, textAlign: "center",
						fontFamily: constants.fonts.medium,
						marginTop: 15
					}}> Order Id :
						<Text style={{
							fontFamily: constants.fonts.semiBold,
							textTransform: "uppercase", marginTop: -5
						}}> {this.state.order.order_id}</Text>
					</Text>
					{/* {
						this.state.order.is_express_delivery ? */}
					<Text style={{
						color: "black",
						fontSize: 16, textAlign: "center",
						margin: 10,
						marginLeft: "15%",
						marginRight: "15%"
					}}>Delivery Time :- </Text>
					<Text style={{
						color: "black",
						fontFamily: constants.fonts.semiBold,
						fontSize: 16, textAlign: "center",
						margin: 10,
						marginLeft: "15%",
						marginRight: "15%"
					}}>
						{moment(Date.now()).format('dddd') === moment(this.state.order.deliver_start_time).format('dddd') ? "Today" :
							moment(this.state.order.deliver_start_time).format('dddd')}, {moment(this.state.order.deliver_start_time).format("h:mm a")} to {moment(this.state.order.deliver_end_time).format("h:mm a")}
					</Text>
					<Text style={{
						color: "black",
						fontSize: 16, textAlign: "center",
						margin: 10,
						marginLeft: "15%",
						marginRight: "15%"
					}}>
						You can check status of your order on my order Page
				</Text>
					<View style={{
						width: "100%",
						flexDirection: "row",
						justifyContent: "center",
						marginTop: 15
					}}>
						<TouchableOpacity style={styles.buttons}
							onPress={() => {
								NavigationService.navigate("MainStack");
							}}>
							<Text style={{ color: constants.green, fontSize: 17, color: "white", fontWeight: "600" }}>HOME</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.buttons2}
							onPress={() => {
								NavigationService.navigate("MainOrderStack");
							}}>
							<Text style={{ fontSize: 17, color: "black", fontWeight: "500" }}>MY ORDERS</Text>
						</TouchableOpacity>
					</View>
					<StatusBar backgroundColor={constants.green} barStyle="light-content" />
				</View >
			</SafeAreaView>
		)
	}
}

export default OrderStatus;

const styles = StyleSheet.create({
	buttons: {
		backgroundColor: constants.green,
		margin: 5,
		padding: 10,
		paddingLeft: 17,
		paddingRight: 17,
		borderRadius: 20
	},
	buttons2: {
		borderWidth: 2,
		margin: 5,
		padding: 10,
		paddingLeft: 17,
		paddingRight: 17,
		borderRadius: 20,
		borderColor: "lightgrey"
	}
})