import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, TouchableOpacity, Animated, TouchableHighlight, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import CardView from 'react-native-cardview';
import constants from '../../constants/constants';
import { Dropdown } from 'react-native-material-dropdown';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class PaymentScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	state = {
		loading: false,
		selectedIndex: 0,
		scrollY: new Animated.Value(0.01),
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		mode: "cod",
		cardNo: "",
		cardHolderName: "",
		cvv: "",
		exp_month: "",
		exp_year: "",
		cardDetailsFilled: false,
		modeSelected: false,
		headerHeight: 0,
		store_address_id: '',
		store_id: ""
	};

	componentDidMount = () => {
		AsyncStorage.getItem("cartStore").then((data) => {
			let store = JSON.parse(data);
			this.setState({ store_id: store._id })
			let city = constants.city;
			let area = constants.area;
			for (let i = 0; i < store.address.length; i++) {
				if (city._id == store.address[i].city_id && area._id == store.address[i].area_id) {
					this.setState({ store_address_id: store.address[i]._id }, () => console.log("store address id set"))
					break;
				}
			}
		});
	}

	placeOrder = async () => {
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity("Internet is not available", Toast.SHORT, Toast.BOTTOM);
			return
		}
		let cart = this.props.cart ? JSON.parse(JSON.stringify(this.props.cart)) : [];
		let data = { products: [] }, slot_id = this.props.navigation.state.params.slot_id;
		for (let i = 0; i < cart.length; i++) {
			data.products.push({ _id: cart[i]._id, count: cart[i].count });
		}
		data.store_id = this.state.store_id;
		data.store_address_id = this.state.store_address_id;
		data.slot_id = slot_id == "Within 2 Hours" ? "" : slot_id;
		data.address_id = constants.selectAddress._id;
		data.coupon_id = constants.coupon._id ? constants.coupon._id : ""
		data.is_express_delivery = slot_id == "Within 2 Hours" ? true : false;
		console.log(JSON.stringify(data));
		this.setState({ loading: true });
		ApiManager.placeOrder(data).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false });
			}, 100);
			setTimeout(async () => {
				if (res.success) {
					constants.cartStore = {};
					constants.cartCity = {};
					constants.cartArea = {};
					await AsyncStorage.removeItem("cartStore");
					await AsyncStorage.removeItem("cartCity");
					await AsyncStorage.removeItem("cartArea");
					await AsyncStorage.removeItem("cart");
					constants.order = res.data.order;
					NavigationService.navigate("OrderStatusStack");
					this.props.setCart([]);
					// Toast.showWithGravity('Order placed successfully..', Toast.LONG, Toast.CENTER);
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			})
		})
	}

	renderPaymentPage = () => {
		let months = [{
			value: '01',
		}, {
			value: '02',
		}, {
			value: '03',
		}];
		let years = [{
			value: '2019',
		}, {
			value: '2020',
		}, {
			value: '2021',
		}];
		return (
			<View
				style={{
					marginTop: this.state.headerHeight,
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-start',
					backgroundColor: constants.backgroundPageColor,
					marginLeft: 15,
					marginRight: 15
				}}>
				< CardView cardElevation={5}
					cardMaxElevation={5}
					cornerRadius={5}
					style={{
						height: "auto",
						width: "100%",
						backgroundColor: "white",
						borderBottomWidth: 3,
						borderColor: constants.backgroundPageColor,
						marginBottom: 25,
						minHeight: 130,
						justifyContent: "flex-start",
						alignItems: "center"
					}}>
					<View style={{
						width: "100%",
						height: 40,
						backgroundColor: constants.lightgreen,
						justifyContent: "center"
					}}>
						<Text style={{
							color: "black",
							fontSize: 16, color: "black", fontFamily: constants.fonts.semiBold,
							marginLeft: 20,
						}}>Billing Information</Text>
					</View>
					<View style={{
						alignItems: "center",
						justifyContent: "space-evenly"
					}}>
						<Text style={{
							color: "black",
							fontSize: 18, color: "black", fontFamily: constants.fonts.semiBold,
							marginTop: 15,
						}}>Please Select Payment Mode</Text>
						<View style={{
							width: "auto",
							minHeight: 50,
							marginTop: 20,
							flexDirection: "row",
							justifyContent: "center"
						}}>
							{/* <View style={{
								width: "auto",
								flexDirection: "row",
								justifyContent: "space-between",
							}}>
								<TouchableOpacity style={{
									width: 40,
									height: 30,
									marginTop: -10,
									justifyContent: "flex-end",
									alignItems: "center",
								}} onPress={() => {
									this.setState({ mode: "credit", modeSelected: false });
								}}>
									<View style={{
										width: 20,
										height: 20,
										borderColor: this.state.mode == "credit" ? constants.green : "lightgrey",
										borderWidth: this.state.mode == "credit" ? 5 : 2,
										borderRadius: 15
									}}></View>
								</TouchableOpacity>
								<Text style={{ color: "black", fontFamily: constants.fonts.medium }}>Credit Card</Text>
							</View> */}

							<View style={{
								width: "auto",
								flexDirection: "row",
								justifyContent: "space-between",
							}}>
								<TouchableOpacity style={{
									width: 40,
									height: 30,
									marginTop: -10,
									justifyContent: "flex-end",
									alignItems: "center",
									// backgroundColor: "yellow"
								}} onPress={() => {
									this.setState({ mode: "cod", modeSelected: false });
								}}>
									<View style={{
										width: 20,
										height: 20,
										borderColor: this.state.mode == "cod" ? constants.green : "lightgrey",
										borderWidth: this.state.mode == "cod" ? 5 : 2,
										borderRadius: 15
									}}></View>
								</TouchableOpacity>
								<Text style={{ color: "black", fontFamily: constants.fonts.medium }}>Cash On Delivery</Text>
							</View>

						</View>
					</View>

					{
						(this.state.modeSelected && this.state.mode == "cod") ?
							<View style={{
								width: "95%",
								height: 40,
								alignItems: "center",
								justifyContent: "center",
								marginBottom: 15
							}}>
								<Text style={{ color: "black", fontSize: 14, textAlign: "center", color: "black", fontFamily: constants.fonts.medium }}>Be ready with the cash for making payment at the time of order delivery.</Text>
							</View>
							: null
					}
				</CardView>
				{
					(this.state.modeSelected && this.state.mode == "credit") ?
						< CardView cardElevation={5}
							cardMaxElevation={5}
							cornerRadius={5}
							style={{
								height: "auto",
								width: "100%",
								backgroundColor: "white",
								borderBottomWidth: 3,
								borderColor: constants.backgroundPageColor,
								marginBottom: 25,
								marginTop: 10,
								padding: 15,
								minHeight: 200,
								justifyContent: "space-between"
							}}>
							<View style={{
								width: "100%",
								height: 40, borderWidth: 1,
								borderColor: "lightgray",
								justifyContent: "center",
								borderRadius: 20,
								marginBottom: 15
							}}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.name}
									placeholder='Card Number'
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ cardNo: text }) }}>
								</TextInput>
							</View>
							<View style={{
								width: "100%",
								height: 40, borderWidth: 1,
								borderColor: "lightgray",
								justifyContent: "center",
								borderRadius: 20,
								marginBottom: 15
							}}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.email}
									placeholder='Name On Card'
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ cardHolderName: text }) }}>
								</TextInput>
							</View>
							<View style={{
								width: "100%",
								height: 40, borderWidth: 1,
								borderColor: "lightgray",
								justifyContent: "center",
								borderRadius: 20,
								marginBottom: 15
							}}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.phone}
									placeholder='CVV Number'
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ cvv: text }) }}>
								</TextInput>
							</View>

							<View style={{
								flexDirection: "row",
								justifyContent: "space-between"
							}}>
								<View style={{
									width: "47%",
									height: 40, borderWidth: 1,
									borderColor: "lightgray",
									justifyContent: "center",
									borderRadius: 20,
								}}>
									<Dropdown
										label={this.state.exp_month == '' ? "Expiry Month" : ""}
										data={months}
										value={this.state.exp_month}
										inputContainerStyle={{ borderBottomColor: 'transparent' }}
										containerStyle={{
											marginLeft: 15, width: "90%",
											marginBottom: 15
										}}
										onChangeText={(value) => {
											this.setState({ exp_month: value });
										}}
									/>
								</View>
								<View style={{
									width: "47%",
									height: 40, borderWidth: 1,
									borderColor: "lightgray",
									justifyContent: "center",
									borderRadius: 20,
								}}>
									<Dropdown
										label={this.state.exp_year == '' ? "Expiry Year" : ""}
										data={years}
										value={this.state.exp_year}
										inputContainerStyle={{ borderBottomColor: 'transparent' }}
										containerStyle={{
											marginLeft: 15, width: "90%",
											marginBottom: 15
										}}
										onChangeText={(value) => {
											this.setState({ exp_year: value });
										}}
									/>
								</View>
							</View>
						</CardView>
						: null
				}
				<View style={{
					width: "100%",
					height: 70,
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 50
				}}>
					{
						!this.state.modeSelected ?
							<TouchableOpacity style={{
								backgroundColor: constants.green,
								height: 50,
								width: 130,
								borderRadius: 30,
								justifyContent: "center",
								alignItems: "center"
							}} onPress={() => {
								this.setState({ modeSelected: true })
							}}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: constants.fonts.bold
								}}>CONTINUE</Text>
							</TouchableOpacity>
							: null
					}
					{
						(this.state.modeSelected && this.state.mode == "credit") ?
							<TouchableOpacity style={{
								backgroundColor: constants.green,
								height: 50,
								width: 130,
								borderRadius: 30,
								justifyContent: "center",
								alignItems: "center"
							}} onPress={() => this.placeOrder()}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: constants.fonts.bold
								}}>PAY NOW</Text>
							</TouchableOpacity> : null
					}
					{
						(this.state.modeSelected && this.state.mode == "cod") ?
							<TouchableOpacity style={{
								backgroundColor: constants.green,
								height: 50,
								width: "auto",
								borderRadius: 30,
								justifyContent: "center",
								alignItems: "center"
							}} onPress={() => this.placeOrder()}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: constants.fonts.bold,
									marginLeft: 15,
									marginRight: 15
								}}>CONFIRM AND PLACE ORDER</Text>
							</TouchableOpacity> : null
					}
				</View>
			</View>
		)
	}

	render() {
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderPaymentPage()}
				</Animated.ScrollView>
				<Animated.View style={{
					top: 0, left: 0, right: 0,
					position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 50), },
						android: { height: (getStatusBarHeight(true) + 40), }
					}),
					backgroundColor: constants.green
					// backgroundColor: 'red'
				}} onLayout={(event) => {
					this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
						console.log("Header height is:--" + this.state.headerHeight);
					})
					console.log(event.nativeEvent.layout);
				}}>
					<Animated.View style={{
						height: '100%',
						marginLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: '100%',
						// alignItems: "center",
						// backgroundColor: "red"
					}}>
						<TouchableHighlight style={{
							height: 45, width: 45,
							...Platform.select({
								ios: { marginTop: 45 },
								android: { marginTop: 14 }
							}),
							justifyContent: "center",
							position: "absolute",
							left: 5,
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								// alert("hello");
								NavigationService.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
						<Text style={{
							fontSize: 18,
							color: 'white', fontFamily: constants.fonts.bold,
							marginLeft: 50
						}}>Payment</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen)