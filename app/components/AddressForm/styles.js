import { StyleSheet, Dimensions } from 'react-native';
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
import constants from '../../constants/constants';

export default StyleSheet.create({
	inputBox: {
		width: screenWidth - 60,
		height: 40, borderWidth: 1,
		borderColor: "lightgray",
		justifyContent: "center",
		borderRadius: 20,
		marginBottom: 15
	},
	whiteBox: {
		height: "auto",
		width: screenWidth - 30,
		backgroundColor: "white",
		borderBottomWidth: 3,
		borderColor: constants.backgroundPageColor,
		marginBottom: 25,
		margin: 15,
		minHeight: 200,
		justifyContent: "space-between",
		alignItems: "center",
		paddingTop: 15,
	}
});