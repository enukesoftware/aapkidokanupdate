import React, { Component } from 'react';
import { findNodeHandle, View, Text, Image, Dimensions, Alert, TextInput, TouchableOpacity, Animated, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import checked from './../../assets/images/checked.png';
import Images from '../../global/images';
import CardView from 'react-native-cardview';
import constants from '../../constants/constants';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ApiManager from '../../apimanager/ApiManager';
import Toast from 'react-native-simple-toast';
import { Dropdown } from 'react-native-material-dropdown';
import { ScrollView } from 'react-native-gesture-handler';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import styles from './styles';

export default class AddressFormScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	state = {
		_id: "",
		selectedIndex: 0,
		scrollY: new Animated.Value(0.01),
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		full_name: "",
		alias: "home",
		selected: false,
		contact_number: "",
		email: "",
		house_no: "",
		locality: "",
		landmark: "",
		city: {
			"_id": "",
			"name": "",
			"created_at": "",
			"updated_at": "",
			"__v": 0
		},
		what_3_words: "",
		loading: false,
		cities: [],
		coordinates: {},
		gps_address: "",
		addressVerified: false
	};

	componentDidMount = () => {
		this.getCities();
		let params = this.props.navigation.state.params;
		if (params && params.address) {
			console.log("Address fetched" + JSON.stringify(params.address.full_name));
			this.setState({
				full_name: params.address.full_name,
				alias: params.address.alias,
				selected: false,
				contact_number: params.address.contact_number,
				email: params.address.email,
				house_no: params.address.house_no,
				locality: params.address.locality,
				landmark: params.address.landmark,
				city: params.address.city,
				what_3_words: params.address.what_3_words,
				_id: params.address._id,
				coordinates: params.address.coordinates,
				gps_address: params.address.gps_address,
				addressVerified: (params.address.coordinates && (params.address.coordinates.latitude && params.address.coordinates.longitude)) ? true : false
			}, () => {
				console.log("state is :---" + JSON.stringify(this.state));
			});
		}
	}

	getCities = () => {
		this.setState({ loading: true }, () => {
			ApiManager.getCities().then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(() => {
					if (res && res.success && res.code == 200) {
						this.setState({ cities: res.data.cities });
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					}
				}, 500)
			}).catch((err) => {
				this.setState({ loading: false });
				Alert.alert(err.singleStringMessage);
				console.log("Otp error" + JSON.stringify(err));
			})
		});
	}

	saveAddress = () => {
		let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (!this.state.full_name) {
			Toast.showWithGravity(
				'Please enter the name',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.email) {
			Toast.showWithGravity(
				'Please enter email',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!emailcontext.test(this.state.email)) {
			Toast.showWithGravity(
				'Email address is not valid',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.contact_number) {
			Toast.showWithGravity(
				'Please enter contact number',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (isNaN(this.state.contact_number) || (this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 11)) || (!this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 10))) {
			Toast.showWithGravity(
				'Contact number is not valid',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.house_no) {
			Toast.showWithGravity(
				'Please enter house number, etc...',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.locality) {
			Toast.showWithGravity(
				'Please enter area, sector, etc...',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.city._id) {
			Toast.showWithGravity(
				'Please select any city',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.alias) {
			Toast.showWithGravity(
				'Please select address type',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.addressVerified) {
			Toast.showWithGravity(
				'Please verify your Address',
				Toast.LONG, Toast.CENTER //TOP,BOTTOM
			);
		} else {
			this.setState({ loading: true });
			let address = {
				full_name: this.state.full_name,
				email: this.state.email,
				contact_number: this.state.contact_number,
				house_no: this.state.house_no,
				locality: this.state.locality,
				city_id: this.state.city._id,
				alias: this.state.alias,
				coordinates: {
					latitude: this.state.latitude,
					longitude: this.state.longitude,
				},
				gps_address: this.state.gps_address,
				landmark: this.state.landmark,
				what_3_words: this.state.what_3_words,
			}
			if (this.state._id) {
				address._id = this.state._id;
				ApiManager.updateAddress(address).then((res) => {
					setTimeout(() => {
						this.setState({ loading: false }, () => console.log("loading paused"))
					}, 100);
					setTimeout(() => {
						if (res && res.success) {
							console.log(res);
							Toast.showWithGravity(
								'Address updated . . .',
								Toast.SHORT, Toast.CENTER //TOP,BOTTOM
							);
							this.goBack();
						} else {
							Toast.showWithGravity(
								res.singleStringMessage,
								Toast.SHORT, Toast.CENTER //TOP,BOTTOM
							);
						}
					}, 500)
				}).catch((err) => {
					this.setState({ loading: false }, () => {
						console.log(err);
					})
				});
			} else {
				ApiManager.saveAddress(address).then((res) => {
					setTimeout(() => {
						this.setState({ loading: false }, () => console.log("loading paused"))
					}, 100);
					setTimeout(() => {
						if (res && res.success) {
							console.log(res);
							Toast.showWithGravity(
								'Address saved . . .',
								Toast.SHORT, Toast.CENTER //TOP,BOTTOM
							);
							this.goBack();
						} else {
							Toast.showWithGravity(
								res.singleStringMessage,
								Toast.SHORT, Toast.CENTER //TOP,BOTTOM
							);
						}
					}, 500);
				}).catch((err) => {
					this.setState({ loading: false }, () => {
						console.log(err);
					})
				});
			}
		}
	}

	addressFetched = data => {
		console.log("Address verified");
		console.log("Fetched data is :---" + JSON.stringify(data));
		this.setState(prevState => ({
			latitude: data.latitude,
			longitude: data.longitude,
			gps_address: data.address,
			addressVerified: true
		}), () => {
			console.log("New state is :-" + JSON.stringify(this.state))
		});
	};

	verifyAddress = () => {
		NavigationService.navigate("SearchAddress", this.state.coordinates ? {
			addressFetched: this.addressFetched,
			latitude: this.state.coordinates.latitude,
			longitude: this.state.coordinates.longitude,
			gps_address: this.state.gps_address
		} : { addressFetched: this.addressFetched });
	}

	inputFocused = (ref) => {
		let offValue = 200;
		switch (ref) {
			case "input5":
				offValue = 300
				break;
			case "input6":
				offValue = 230
				break;
			default:
				offValue = 150;
				break;
		}
		this._scroll(ref, offValue);
	}

	inputBlurred = (ref) => {
		this._scroll(ref, 0);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	renderAddressForm = () => {
		return (
			<KeyboardAwareView animated={true} >
				<View style={{
					marginTop: this.state.headerHeight,
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-start',
					backgroundColor: constants.backgroundPageColor,
				}}>
					<ScrollView showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}
						ref="myScrollView" keyboardDismissMode='interactive'
					>
						< CardView cardElevation={5}
							cardMaxElevation={5}
							cornerRadius={5}
							style={styles.whiteBox}>
							<View style={styles.inputBox}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.full_name}
									placeholder='Full Name'
									keyboardType="default"
									placeholderTextColor={constants.placeHolderColor}
									returnKeyType="next"
									onChangeText={(text) => { this.setState({ full_name: text }) }}
									returnKeyType="next"
									autoCapitalize="none"
									ref="input"
									onFocus={this.inputFocused.bind(this, 'input')}>
								</TextInput>
							</View>
							<View style={styles.inputBox} >
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.email}
									placeholder='Email Id'
									keyboardType="email-address"
									returnKeyType="next"
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ email: text }) }} returnKeyType="next"
									autoCapitalize="none"
									ref="input1"
									onFocus={this.inputFocused.bind(this, 'input1')}>
								</TextInput>
							</View>
							<View style={styles.inputBox}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.contact_number}
									maxLength={11}
									keyboardType="phone-pad"
									placeholder='Mobile Number'
									returnKeyType="next"
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ contact_number: text }) }}
									returnKeyType="next"
									autoCapitalize="none"
									ref="input2"
									onFocus={this.inputFocused.bind(this, 'input2')}>
								</TextInput>
							</View>
							<View style={styles.inputBox}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.house_no}
									returnKeyType="next"
									placeholder='House No, Flat No, Apartment'
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ house_no: text }) }}
									returnKeyType="next"
									autoCapitalize="none"
									ref="input3"
									onFocus={this.inputFocused.bind(this, 'input3')}>
								</TextInput>
							</View>
							<View style={styles.inputBox}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.locality}
									placeholder='Area, Sector, Colony'
									returnKeyType="next"
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ locality: text }) }}
									returnKeyType="next"
									autoCapitalize="none"
									ref="input4"
									onFocus={this.inputFocused.bind(this, 'input4')}>
								</TextInput>
							</View>
							<View style={styles.inputBox}>
								<TextInput
									style={{ marginLeft: 15, width: "100%" }}
									value={this.state.landmark}
									placeholder='Landmark'
									returnKeyType="next"
									placeholderTextColor={constants.placeHolderColor}
									onChangeText={(text) => { this.setState({ landmark: text }) }}
									returnKeyType="next"
									autoCapitalize="none"
									ref="input5"
									onFocus={this.inputFocused.bind(this, 'input5')}>
								</TextInput>
							</View>
							<View style={styles.inputBox}>
								<Dropdown placeholder="City"
									placeholderTextColor={constants.placeHolderColor}
									data={this.state.cities}
									value={this.state.city.name}
									containerStyle={{
										marginLeft: 15, width: "90%",
										marginTop: -20,
									}}
									inputContainerStyle={{ borderBottomColor: 'transparent' }}
									pickerStyle={{
										width: "90%",
										marginLeft: -12, marginTop: 40
									}}
									onChangeText={(value, index) => {
										this.setState({ city: this.state.cities[index] }, () => {
											// this.getAreas();
										});
									}}
								/>
							</View>
							<View style={{
								width: this.state.screenWidth - 60,
								height: 50,
								justifyContent: "flex-start",
								alignItems: "center",
								marginBottom: 15,
								flexDirection: "row",
								marginLeft: 5
							}}>
								<View style={{
									width: "42 %",
								}}>
									<Text style={{
										fontSize: 13,
										color: "black", fontFamily: constants.fonts.semiBold, color: "black"
									}}>Address alias</Text>
								</View>
								<View style={{
									width: "29%",
									flexDirection: "row",
									justifyContent: "space-between",
								}}>
									<TouchableOpacity style={{
										width: 40,
										height: 30,
										marginTop: -10,
										justifyContent: "flex-end",
										alignItems: "center",
									}} onPress={() => {
										this.setState({ alias: "home" });
									}}>
										<View style={{
											width: 20,
											height: 20,
											borderColor: this.state.alias == "home" ? constants.green : "lightgrey",
											borderWidth: this.state.alias == "home" ? 5 : 2,
											borderRadius: 15
										}}></View>
									</TouchableOpacity>
									<Text style={{ color: "black", fontFamily: constants.fonts.medium }}>Home</Text>
								</View>
								<View style={{
									width: "29%",
									flexDirection: "row",
									justifyContent: "space-between",
								}}>
									<TouchableOpacity style={{
										width: 40,
										height: 30,
										marginTop: -10,
										justifyContent: "flex-end",
										alignItems: "center",
									}} onPress={() => {
										this.setState({ alias: "work" });
									}}>
										<View style={{
											width: 20,
											height: 20,
											borderColor: this.state.alias == "work" ? constants.green : "lightgrey",
											borderWidth: this.state.alias == "work" ? 5 : 2,
											borderRadius: 15
										}}></View>
									</TouchableOpacity>
									<Text style={{ color: "black", fontFamily: constants.fonts.medium }}>Work</Text>
								</View>
							</View>
							<TouchableOpacity style={{
								width: this.state.screenWidth - 60,
								backgroundColor: constants.green,
								height: 40,
								borderRadius: 30,
								justifyContent: "center",
								alignItems: "center",
								marginBottom: 15
							}} onPress={() => this.verifyAddress()}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: constants.fonts.bold
								}}>Verify Address</Text>
								{
									(this.state.addressVerified) ?
										<Image source={Images.checked} resizeMode="contain" style={{
											position: "absolute",
											right: 10, top: 10,
											height: 20, width: 20
										}}></Image> : null
								}
							</TouchableOpacity>
							{/* {this.renderWhat3Words()} */}
						</CardView>
						<View style={{
							width: "100%",
							height: 70,
							justifyContent: "center",
							alignItems: "center",
							marginBottom: 50
						}}>
							<TouchableOpacity style={{
								backgroundColor: constants.green,
								height: 40,
								borderRadius: 30,
								justifyContent: "center",
								paddingLeft: 15,
								paddingRight: 15
							}} onPress={() => this.saveAddress()}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: constants.fonts.bold
								}}>{this.state._id ? "UPDATE" : "ADD"} ADDRESS</Text>
							</TouchableOpacity>
						</View>
					</ScrollView>
				</View>
			</KeyboardAwareView>
		)
	}

	renderWhat3Words = () => {
		return (
			<View style={{
				width: this.state.screenWidth - 60,
				height: 40,
				justifyContent: "flex-start",
				alignItems: "center",
				marginBottom: 15,
				flexDirection: "row",
				marginLeft: 5
			}}>
				<View style={{ width: "42 %" }}>
					<Text style={{
						fontSize: 13,
						color: "black", fontFamily: constants.fonts.semiBold, color: "black"
					}}>Enter What3Words</Text>
				</View>
				<View style={{
					width: "auto",
					flexDirection: "row",
					justifyContent: "space-between",
				}}>
					<View style={{
						height: 40, width: 90,
						borderColor: "lightgrey",
						borderWidth: 1,
						borderRadius: 35,
						alignItems: "center",
						justifyContent: "center",
						paddingLeft: 10,
						paddingRight: 5
					}}>
						<TextInput style={{ width: "100%", height: "100%" }}
							value={this.state.what_3_words}
							onChangeText={(text) => { this.setState({ what_3_words: text }) }}
							returnKeyType="next"
							autoCapitalize="none"
							ref="input6"
							onFocus={this.inputFocused.bind(this, 'input6')}
							returnKeyType="done"
							onSubmitEditing={() => this.saveAddress()}>
						</TextInput>
					</View>
					<TouchableOpacity style={{
						width: 60,
						height: 40,
						backgroundColor: constants.green,
						borderRadius: 25,
						justifyContent: "center",
						alignItems: "center",
						marginLeft: 10
					}} onPress={() => {
						// NavigationService.goBack()
					}}>
						<Text style={{ fontSize: 15, fontFamily: constants.fonts.bold, color: "white" }}>OK</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	goBack = () => {
		let params = this.props.navigation.state.params;
		if (params && params.backStateName) {
			NavigationService.navigate(params.backStateName);
		} else NavigationService.goBack()
	}

	render() {
		console.log("Address form");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.View
					style={{ flex: 1 }}>
					{this.renderAddressForm()}
				</Animated.View>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: { height: (getStatusBarHeight(true) + 40), }
					}),
					backgroundColor: '#1faf4b',
					paddingBottom: 10
				}}>
					<Animated.View style={{
						height: '100%', width: '10%',
						minWidth: 60,
						minHeight: 60,
						justifyContent: 'center',
						alignItems: 'center',
						position: "absolute",
						left: 0,
						top: 0,
						zIndex: 2,
						paddingLeft: 10
					}}>
						<TouchableOpacity style={{
							height: 45, width: 45,
							...Platform.select({
								ios: { marginTop: 50 },
								android: { marginTop: 19 }
							}),
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								this.goBack();
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableOpacity>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 60,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: this.state.screenWidth - 60,
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<Text style={{
							fontSize: 18, color: 'white',
							fontFamily: constants.fonts.bold,
							// backgroundColor: "red"
						}}>{this.state._id ? "Update Address" : "Add New Address"}</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}