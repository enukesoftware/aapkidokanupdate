import {Platform, StyleSheet } from 'react-native';
import constants from '../../constants/constants';


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    toolbar: {
        height: (Platform.OS === constants.PLATFORM.ios) ? constants.TOOLBAR_HEIGHT.ios : constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: constants.green,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15,
        width:'100%'
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: '#FFFFFF',
        marginLeft: 15,
    },
    logoImageStyle: {
        height: 85,
        width: 90,
        resizeMode: 'contain',
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    addressOuterViewStyle: {
        width: '90%',
        padding: 8,
        marginTop: 35,
        backgroundColor:'#92929A',
        borderRadius: 7
    },
    addressInnerViewStyle: { 
        paddingVertical: 10, 
        paddingLeft: 5, 
        backgroundColor: 'white', 
        borderRadius: 7 ,
        flexDirection:'row',
        alignItems:'center',
    },
    orViewStyle:{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:15},
    dividerStyle:{height:1,width:'30%',backgroundColor:'#92929A'},
    orTextStyle:{paddingBottom:5, marginHorizontal:6,fontSize:22,alignSelf:'center'},
   
})