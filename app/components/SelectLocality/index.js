import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, SafeAreaView, View, Image, Text, TouchableOpacity, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles'
// import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import Constants from '../../constants/constants';
import Images from '../../global/images';
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
// import { clearLocation } from '../../redux/actions'
import NavigationService from '../../global/NavigationService';
// import { locationSelected ,isLoginAction,userDataAction,addItemInBasketAction, deviceTokenAction} from '../../redux/actions'
import GlobalStyle from '../../global/styles';
import constants from '../../constants/constants';

// import GlobalStyle from '../../../styles'

let headerLeft = '';

class SelectLocality extends Component {

    static navigationOptions = {
        header: null
    };


    state = {
        address: 'Address',
        isCloseVisible: false,
    }

    clearAddress = () => {
        // this.props.clearLocation()
        this.setState({
            isCloseVisible: false
        })
    }


    addressFetched = data => {
		console.log("Address verified");
		console.log("Fetched data is :---" + JSON.stringify(data));
		this.setState(prevState => ({
			latitude: data.latitude,
			longitude: data.longitude,
			address: data.address,
			addressVerified: true
		}), () => {
			console.log("New state is :-" + JSON.stringify(this.state))
		});
	};

	useMyLocation = () => {
		NavigationService.navigate("SearchAddress", this.state.coordinates ? {
			addressFetched: this.addressFetched,
			latitude: this.state.coordinates.latitude,
			longitude: this.state.coordinates.longitude,
			address: this.state.gps_address
		} : { addressFetched: this.addressFetched });
	}

    constructor() {
        super()
        console.log("SELECT_ADDRESS");
    }

    componentDidMount() {
        //get last stored locaiton information
        AsyncStorage.getItem(Constants.STORAGE_KEY.selectedLocation, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                // this.setState({ userData: JSON.parse(result) })
                // console.warn(JSON.parse(result));
                if (result) {
                    console.log("RESULT:" + result)
                    result = JSON.parse(result)
                    // this.props.locationSelected({
                    //     address: result.address,
                    //     latitude: result.latitude,
                    //     longitude: result.longitude,
                    //     addressToDisplay: result.addressToDisplay,
                    //     city: result.city
                    // })
                    this.setState({
                        address: result.addressToDisplay,
                        isCloseVisible: true
                    })
                } else {
                    this.setState({
                        address: 'Address',
                        isCloseVisible: false
                    })
                }
            }
        })

        
    }


    renderCloseButton = () => {
        if (this.props.isCloseVisible) {
            return (
                <TouchableOpacity style={{ width: '10%' }} onPress={() => this.clearAddress()}>
                    <Image source={Images.ic_cross} style={{ width: 25, height: 25 }}></Image>
                </TouchableOpacity>
            )
        }
    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        const { navigation } = this.props;
        const navigateFrom = navigation.getParam('NavigateFrom', '');
        if (navigateFrom === '' || navigateFrom === 'TutorialScreen') {
            return null
        }
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.back}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.back}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={styles.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>
                    {this.renderBackButton()}


                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: "center",
                    width: '60%'
                }
                }>
                    <TextBold title={'Select Locality'} textStyle={styles.textTitle} />
                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>

                </View>
                
            </View>

        )
    }

    showRestaurant = () => {
        if (this.props.address === 'Address') {
            Alert.alert('Aapkidokan', "Address Can not be blank Or Select Use my location  Button for current location", [{
                text: "OK"
            }])
        } else if (this.props.city === '') {
            Alert.alert('Aapkidokan', "Area and City Can not be blank Or Select your location using My Location Button", [{
                text: "OK"
            }])
        } else {
            AsyncStorage.setItem(Constants.STORAGE_KEY.selectedLocation, JSON.stringify(this.props.location));
            NavigationService.navigate("Dashboard", { locationUpdate: this.props.location });
        }

    }

    render() {
        return (
            <View style={{ width: '100%', flex: 1 }}>
                <SafeAreaView style={{ backgroundColor: Constants.green }}></SafeAreaView>
                {this.renderToolbar()}
                <View style={styles.container}>


                <View style={{height:115,width:120,marginTop:100,backgroundColor:Constants.green,alignItems:'center',justifyContent:'center',borderRadius:60}}>
                    <Image source={Images.logo} style={styles.logoImageStyle}></Image>
                   </View>
                    <View style={styles.addressOuterViewStyle}>
                        <View style={styles.addressInnerViewStyle}>
                            <TextRegular title={this.state.address} numberOfLines={1} ellipsizeMode='tail' textStyle={{ paddingVertical: 5, width: '90%' }}></TextRegular>
                            {this.renderCloseButton()}
                        </View>
                    </View>
                    <View style={styles.orViewStyle}>
                        <View style={styles.dividerStyle}></View>
                        <TextRegular title={'or'} textStyle={styles.orTextStyle}></TextRegular>
                        <View style={styles.dividerStyle}></View>
                    </View>

                    <TouchableOpacity
                        onPress={() => this.useMyLocation()}
                        style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={Images.location} style={{ width: 30, height: 30 }}></Image>
                        <TextBold title={'Use my location'} textStyle={{ color: Constants.green, fontSize: 22, marginLeft: 10 }}></TextBold>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.showRestaurant()}
                        style={{ width: '90%', marginTop: 20, backgroundColor: Constants.green, borderRadius: 5, padding: 10, alignItems: 'center' }}>
                        <TextBold title={'Show Restaurants'} textStyle={{ color: 'white', padding: 10, fontSize: 16 }} />
                    </TouchableOpacity>
                    <SafeAreaView style={{ flex: 1, backgroundColor: Constants.green }}></SafeAreaView>
                </View>
            </View>
        );
    };
}

function mapStateToProps(state) {
    return {
        location: '',//state.location,
        address: '',//state.location.addressToDisplay,
        city: '',//state.location.city,
        isCloseVisible: false,//state.location.addressToDisplay === 'Address' ? false : true,
        isLogin:false,//state.isLogin,
    }
}

// export default connect(mapStateToProps, { clearLocation, locationSelected,isLoginAction,userDataAction,addItemInBasketAction,deviceTokenAction })(SelectLocality)
export default connect(mapStateToProps, {  })(SelectLocality)
