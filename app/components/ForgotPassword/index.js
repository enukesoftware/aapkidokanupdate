
import React, { Component } from 'react';
import { Image, View, Dimensions, findNodeHandle, Text, ScrollView, TextInput, TouchableOpacity, ImageBackground, Alert } from 'react-native'
import styles from '../../../app/global/styles';
import constants from '../../../app/constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Images from '../../global/images';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

class ForgotPasswordScreen extends Component {
	static navigationOptions = () => ({
		header: null
	});

	state = {
		contact_number: '',
		loading: false
	}

	sendOtp = () => {
		if (!this.state.contact_number || !this.state.contact_number.length) {
			Toast.showWithGravity("Please enter your Mobile Number", Toast.LONG, Toast.BOTTOM);
			return false;
		} else if (isNaN(this.state.contact_number) || (!isNaN(this.state.contact_number) && ((this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 11)) || (!this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 10))))) {
			Toast.showWithGravity("Mobile Number is not valid", Toast.LONG, Toast.BOTTOM);
			return false;
		}
		this.setState({ loading: true });
		ApiManager.forgotPassword({
			contact_number: this.state.contact_number
		}).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				if (res && res.success && res.code == 200) {
					let body = {
						verification_token: res.data.verification_token,
						contact_number: this.state.contact_number
					}
					if (this.props.navigation.state.routeName == "OtherForgotPassword") {
						NavigationService.navigate("OtherSendOtp", { data: body });
					} else {
						NavigationService.navigate("SendOtp", { data: body });
					}
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.LONG, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				Alert.alert(err.singleStringMessage);
			}, 500);
			console.log("Otp error" + JSON.stringify(err));
		})
	}

	inputFocused = (ref) => {
		this._scroll(ref, 100);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	render() {
		console.log(this.props.navigation.state.routeName);
		return (
			<KeyboardAwareView animated={true}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", width: 200 }}
				/>
				<ImageBackground source={Images.background} style={{ flex: 1 }}>
					<ScrollView showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}
						ref="myScrollView" keyboardDismissMode='interactive'>
						<View style={{ height: screenHeight, width: screenWidth, justifyContent: "center", alignItems: "center" }}>
							<View style={[styles.innerSection, { minHeight: 250, paddingBottom: 30 }]}>
								<View style={styles.logoSection}>
									<Image source={Images.logo} style={styles.logo}></Image>
								</View>
								<Text style={[styles.innerlabel, { fontSize: 15, marginBottom: 10, marginTop: 20 }]}>Enter Mobile Number to Send OTP</Text>
								<View style={[styles.inputSection, { marginBottom: 15 }]}>
									<Image source={Images.phoneImg} style={styles.inputLogo} />
									<TextInput
										style={styles.input}
										value={this.state.contact_number}
										maxLength={11}
										keyboardType="phone-pad"
										placeholder='Enter Mobile Number'
										placeholderTextColor={constants.placeHolderColor}
										onChangeText={(text) => { this.setState({ contact_number: text }) }}
										returnKeyType="done"
										onSubmitEditing={() => this.sendOtp()}
										autoCapitalize="none"
										ref="input"
										onFocus={this.inputFocused.bind(this, 'input')}
									/>
								</View>
								<TouchableOpacity style={styles.loginButton}
									onPress={() => this.sendOtp()}>
									<Text style={[styles.loginText, { color: "white" }]}>SEND OTP</Text>
								</TouchableOpacity>
							</View>
						</View>
					</ScrollView>
				</ImageBackground>
			</KeyboardAwareView>
		)
	}
}

export default ForgotPasswordScreen;