import { StyleSheet, Platform, Dimensions } from 'react-native';
import constants from '../../constants/constants';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';

export default StyleSheet.create({
	counterImage: { height: 10, width: 10 },
	productImage: { height: 80, width: 50, resizeMode: 'contain' },
	outerLayer: {
		height: 230,
		alignItems: 'center',
		justifyContent: "flex-start",
		shadowColor: 'dimgrey',
		marginLeft: 7,
		marginRight: 7,
		marginTop: 10,
		// marginBottom: 10
	},
	headerlayer: {
		height: 30,
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		// marginBottom: 7,
		paddingLeft: 7,
		paddingRight: 7
	},
	orderBox: {
		// flex: 1,
		marginTop: 10,
		backgroundColor: "white",
		height: 130,
		width: '95%',
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	productQuantity: {
		height: 25,
		minWidth: 40,
		borderRadius: 20,
		backgroundColor: "#d0efdd",
		justifyContent: "center",
		alignItems: "center"
	},
	productCounter: {
		borderColor: "lightgrey",
		borderWidth: 1,
		height: 30,
		alignItems: "center",
		width: "90%",
		justifyContent: "space-between",
		borderRadius: 30,
		flexDirection: "row",
		marginBottom: 10,
		borderLeftWidth: 0,
		borderRightWidth: 0,
	},
	counterButton: {
		height: 30, width: 30,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1, borderColor: "lightgrey",
		borderRadius: 30,
	},
	statusbar: {
		backgroundColor: constants.green,
		width: "100%",
		height: Platform.OS == "ios" ? getStatusBarHeight(true) : 0
	},
	headerView: {
		backgroundColor: constants.green,
		width: "100%", height: 50,
		alignItems: "center",
		justifyContent: "center"
	},
	backButton: {
		height: 50,
		width: 55,
		// backgroundColor: "red",
		position: "absolute",
		left: 0,
		justifyContent: "center",
		alignItems: "center"
	},
	headertext: {
		fontSize: 18, color: 'white',
		fontFamily: constants.fonts.semiBold,
	}
});