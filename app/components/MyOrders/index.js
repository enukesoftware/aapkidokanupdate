import React, { Component } from 'react';
import { View, FlatList, DeviceEventEmitter, ActivityIndicator, Text, Image, Dimensions, TouchableOpacity, Animated, TouchableHighlight, ScrollView, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import styles from './styles';
import CardView from 'react-native-cardview'
import Images from '../../global/images';
import constants from '../../constants/constants';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ApiManager from '../../apimanager/ApiManager';
import moment from "moment";
import Toast from 'react-native-simple-toast';
import { NavigationEvents } from "react-navigation";
import NavigationService from '../../global/NavigationService';

export default class MyOrders extends Component {
	static navigationOptions = {
		header: null
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			products: [1, 2, 3, 4, 5, 6],
			orderPlaceDate: 'Tue 31 Jan',
			orderScheduleDate: 'Tue 6 Feb',
			orderList: [],
			pageNo: 1,
			isLoading: false,
			isRefreshing: false,
			isListEnd: false,
			loadingMore: false,
		};

		this.orderNotificationHandler = this.orderNotificationHandler.bind(this);
	}

	comingFromTrack = () => {
		console.log("comingFromTrack");
		setTimeout(() => {
			this.setState({
				isLoading: false,
				isRefreshing: false,
				loadingMore: false,
			}, () => console.log("back:from:map"));
		}, 2000);
	}

	getOrders = () => {
		let { pageNo, orderList, isListEnd, isLoading, isRefreshing, loadingMore } = this.state;
		this.setState({ isLoading: true });
		ApiManager.getOrders(pageNo).then((res) => {
			setTimeout(() => {
				this.setState({
					isLoading: false,
					isRefreshing: false,
				}, () => console.log("isLoading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					if (!res.data.orders.length) {
						this.setState({
							loadingMore: false,
							isListEnd: true,
						})
					} else {
						this.setState({
							isListEnd: false,
							loadingMore: false,
							orderList: pageNo === 1 ? res.data.orders : [...orderList, ...res.data.orders],
						}, () => {
							console.log(this.state.orderList);
							// NavigationService.navigate("OrderDetails", { data: this.state.orderList[0] });
						});
					}
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 300);
		}).catch((err) => {
			this.setState({ isLoading: false }, () => {
				console.log(err);
			});
		});
	}

	renderOrder = (order, index) => {
		return (
			// <View style={{ width: "100%", }}>
			<CardView cardElevation={5} cardMaxElevation={5} cornerRadius={5}
				style={styles.orderBox}>
				<View style={{
					height: 30, width: '100%',
					backgroundColor: Constants.lightgreen,
					borderTopRightRadius: 6, borderTopLeftRadius: 6,
					flexDirection: 'row', justifyContent: "space-between",
					paddingLeft: 10, paddingRight: 10
				}}>
					<View style={{
						height: '100%', alignItems: 'center',
						width: '50%', flexDirection: 'row',
						justifyContent: 'flex-start'
					}}>
						<Text style={{
							fontSize: 10, color: "black",
							fontFamily: constants.fonts.medium
						}}>Placed On </Text>
						<Text style={{
							fontSize: 10, color: 'black',
							fontFamily: constants.fonts.bold
						}}>{moment(order.created_at).format('D MMM, h:mm a')}</Text>
					</View>
					<View style={{
						height: '100%', width: '50%',
						alignItems: 'center', flexDirection: 'row',
						justifyContent: "flex-end"
					}}>
						<Text style={{
							fontSize: 10, color: "black",
							fontFamily: constants.fonts.medium
						}}>Schedule On </Text>
						<Text style={{
							fontSize: 10, color: "black",
							fontFamily: constants.fonts.bold
						}}>{moment(order.deliver_end_time).format('D MMM, h:mm a')}</Text>
					</View>
				</View>
				<View style={{
					height: '100%', width: '100%',
				}}>
					<View style={{
						height: '55%', width: '100%',
						flexDirection: 'row'
					}}>
						<View style={{
							height: '100%', width: '78%',
							flex: 1, flexDirection: 'row'
						}}>
							<View style={{
								width: 60, height: 60,
								borderRadius: 30, backgroundColor: 'lightgrey',
								borderColor: 'red', justifyContent: 'center',
								alignItems: 'center', marginTop: 10, marginLeft: 10
							}}>
								<Image style={{
									width: 58, height: 58,
									borderRadius: 29, resizeMode: "cover"
								}} source={{ uri: constants.imageBaseURL + order.store.picture }} />
							</View>
							<View style={{
								marginTop: 20, marginLeft: 10
							}}>
								<Text style={{
									color: "black",
									fontFamily: constants.fonts.bold
								}}>{order.store.name}</Text>
								<Text style={{
									marginTop: 6, color: "black",
									fontFamily: constants.fonts.medium
								}}>Order Id :- <Text style={{ textTransform: "uppercase", color: "black" }}>#{order.order_id ? order.order_id : "N/A"}</Text></Text>
							</View>
						</View>
						<View style={{
							height: '100%',
							width: '22%', alignItems: 'flex-end'
						}}>
							<View style={{
								paddingLeft: "10%",
								backgroundColor: Constants.lightgreen,
								borderBottomLeftRadius: 20,
								borderTopLeftRadius: 20, width: "100%",
								maxWidth: 120, height: "auto",
								minHeight: 30, marginTop: 20,
								paddingBottom: 5, paddingTop: 5,
								justifyContent: 'center', alignItems: 'center'
							}}>
								<Text style={{ color: "black", fontFamily: constants.fonts.semiBold }}>{(order.total_amount_after_tax + order.delivery_charges) - order.discount} {constants.currency}</Text>
							</View>
						</View>
					</View>
					<View style={{
						height: '20%', width: '100%',
						flexDirection: 'row', alignItems: 'center'
					}}>
						<View style={{
							flexDirection: "row",
							width: "50%"
						}}>
							<Text style={{
								fontSize: 10, marginLeft: 10,
								color: "black", fontFamily: constants.fonts.medium
							}}>Order Status-:</Text>
							<Text style={{
								color: order.status == 5 ? constants.red : "black",
								fontFamily: constants.fonts.bold,
								fontSize: 10,
								marginLeft: 3, textTransform: "uppercase"
							}}>{order.orderStatus}</Text>
						</View>
						<View style={{
							flexDirection: "row",
							width: "50%",
							justifyContent: order.status == 6 ? "space-between" : "flex-end"
						}}>
							{
								order.status == 6 ?
									<TouchableOpacity style={{
										height: '100%',
										alignItems: 'center'
									}} onPress={() => NavigationService.navigate("TrackOrder", { order: order, backFuction: this.comingFromTrack })}>
										<Text style={{
											fontSize: 10,
											fontFamily: constants.fonts.bold,
											color: Constants.green, marginTop: 6
										}}>TRACK ORDER</Text>
									</TouchableOpacity> : null
							}
							<TouchableOpacity style={{
								height: '100%', alignItems: 'center',
								paddingRight: 5
							}}
								onPress={() => {
									if (this.props.navigation.state.routeName == "MainOrders") {
										NavigationService.navigate("MainOrderDetails", { order_id: order._id })
									} else {
										NavigationService.navigate("OrderDetails", { order_id: order._id })
									}
								}}>
								<Text style={{
									fontSize: 10, color: "black",
									fontFamily: constants.fonts.semiBold,
									marginTop: 6
								}}>VIEW DETAILS</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</CardView>
			// </View>
		)
	}

	keyExtractor = (item) => item._id;

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getOrders();
			});
		}
	};

	refreshOrder = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getOrders();
			})
		}
	}

	renderFooter = () => {
		let { isLoading, isListEnd, loadingMore } = this.state;
		if (!loadingMore || isListEnd) {
			console.log("loadingMore:-" + loadingMore)
			return null;
		}
		return (
			<View
				style={{
					width: "100%",
					height: 50,
					marginTop: 10,
					marginBottom: 10,
					borderColor: constants.lightgreen,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator
					color={constants.green}
					animating
					size="large" />
			</View>
		);
	}

	renderOrderList = () => {
		let { orderList, isRefreshing } = this.state;
		return (
			<View style={{ width: '100%', flex: 1 }}>
				<FlatList data={orderList}
					renderItem={({ item, index }) => this.renderOrder(item, index)}
					keyExtractor={this.keyExtractor}
					onEndReached={this.handleLoadMore}
					onEndReachedThreshold={1}
					onRefresh={this.refreshOrder}
					refreshing={isRefreshing} />
				{this.renderFooter()}
			</View>
		)
	}

	renderBlankOrder = () => {
		return (
			<View style={{
				flex: 1,
				...Platform.select({
					ios: { height: (Dimensions.get("window").height - (getStatusBarHeight(true) + 18)), },
					android: { height: Dimensions.get("window").height - 43 }
				}),
				// backgroundColor: "red",
				justifyContent: "center",
				alignItems: "center"
			}}>
				<Text style={{
					color: constants.black,
					fontFamily: constants.fonts.semiBold
				}}>Oops !! You dont have any orders</Text>
			</View>
		)
	}

	orderNotificationHandler() {
		console.log("EVENT")
		this.setState({
			pageNo: 1
		}, () => {
			this.getOrders()
		})
	}

	render() {
		let { isLoading, pageNo, isRefreshing } = this.state;
		return (
			<View style={{ flex: 1 }}>
				<Spinner size="large"
					visible={isLoading && pageNo == 1}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<NavigationEvents
					onWillFocus={() => {
						this.setState({
							pageNo: 1
						}, () => {
							DeviceEventEmitter.addListener("ORDER_NOTIFICATION", this.orderNotificationHandler)
							this.getOrders()
						})
						//Call whatever logic or dispatch redux actions and update the screen!
					}}

					onWillBlur={() => {
						DeviceEventEmitter.removeListener("ORDER_NOTIFICATION", this.orderNotificationHandler)
					}}
				/>
				<View style={styles.statusbar}></View>
				<View style={styles.headerView}>
					<TouchableOpacity style={styles.backButton}
						onPress={() => {
							NavigationService.navigate(constants.backStateForOrders);
						}}>
						<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
					</TouchableOpacity>
					<Text style={styles.headertext}>My Orders</Text>
				</View>
				<View style={{ flex: 1, backgroundColor: '#ffffff' }}>
					{this.renderOrderList()}
					{/* {this._renderFooter()} */}
				</View>
			</View>
		)
	}
}