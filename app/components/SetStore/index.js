import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, Dimensions, TouchableOpacity, DeviceEventEmitter, SafeAreaView, Alert, Animated, StatusBar, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';

export default class SetStoreScreen extends Component {
	static navigationOptions = {
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			categoryStores: [],
			categories: [],
			stores: [],
			loading: false,
			area: {},
			city: {},
			categoryId: 1,
		};
	}

	componentDidMount = () => {
		this.getStores();
	}

	getStores = () => {
		this.setState({ loading: true });
		ApiManager.getStoresForSetting().then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					this.setState({
						categories: res.data.categories ? res.data.categories : [],
						stores: res.data.stores ? res.data.stores : [],
						area: res.data.area,
						city: res.data.city
					}, () => console.log("Date has been set"));
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false });
			Alert.alert(err.singleStringMessage);
			console.log("Stores error" + JSON.stringify(err));
		})
	}

	setStoreAndNavigateToHome = (store) => {
		if (constants.store._id != store._id) {
			constants.store = store;
			AsyncStorage.multiSet([['store', JSON.stringify(store)]], () => {
				NavigationService.navigate("Home");
				DeviceEventEmitter.emit("storeUpdate", store._id);
			});
		} else NavigationService.navigate("Home");
	}

	renderStore = store => {
		let { screenWidth, area } = this.state;
		return (
			<TouchableOpacity style={{
				height: 100,
				width: "100%",
				flexDirection: "row",
				justifyContent: "flex-start",
				backgroundColor: "white",
				paddingLeft: 5,
				paddingRightt: 5,
				borderBottomWidth: 2,
				borderBottomColor: constants.backgroundPageColor
			}} onPress={async () => {
				console.log("Setting store id is:--" + store._id);
				constants.store = store;
				AsyncStorage.setItem('store', JSON.stringify(store), () => {
					// NavigationService.navigate("Home");
					if (this.props.navigation.state.routeName == "SetStoreInHome") {
						DeviceEventEmitter.emit("storeUpdate");
						NavigationService.navigate("Home");
					} else {
						NavigationService.navigate("MainStack");
					}
				});
			}}>
				<View style={{ width: 90, height: 100, justifyContent: "center", alignItems: "center" }}>
					<View style={{
						width: 60, height: 60, borderRadius: 30,
						borderWidth: 1,
						borderColor: constants.backgroundPageColor
					}}>
						<Image source={store.picture ? store.imgUrl : Images.gift}
							style={{
								width: 60, height: 60,
								resizeMode: "contain",
								borderRadius: 30,
							}}></Image>
					</View>
				</View>
				<View style={{
					width: (screenWidth - 90),
					flexDirection: "column",
					justifyContent: "space-evenly",
					alignItems: "center",
					height: "95%"
				}}>
					<View style={{
						width: "100%", height: 50,
						justifyContent: "flex-end", paddingRight: 15
					}}>
						<Text style={{
							fontSize: 15, color: "black",
							fontFamily: constants.fonts.bold
						}} numberOfLines={2}>{store.name}</Text>
					</View>
					<View style={{
						flexDirection: "row",
						width: "100%", height: 30,
						justifyContent: "flex-start",
						alignItems: "flex-end",
						// backgroundColor: "green",
					}}>
						<Image source={Images.clock_circle} style={{ width: 15, height: 15, marginRight: 5 }}></Image>
						<Text style={{
							fontSize: 12,
							color: "black", fontFamily: constants.fonts.medium
						}}>{store.has_express_delivery ? "Instant" : "Scheduled"} Delivery</Text>
					</View>
					<View style={{
						width: "100%", height: 50,
						maxHeight: "50%", justifyContent: "center"
					}}>
						<Text style={{
							fontSize: 12,
							color: "black", fontFamily: constants.fonts.medium
						}}>Shop no. {store.address.shop_no}, {area.name}</Text>
					</View>
				</View>
			</TouchableOpacity>
		)
	}

	keyExtractor = (item) => item._id;

	renderCategories = () => {
		let { categories, screenWidth } = this.state;
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-start',
				marginBottom: 50,
				backgroundColor: "white"
			}}>
				{
					categories.map((category, index) => (
						<View key={index} style={{
							width: screenWidth,
							justifyContent: "center",
							borderBottomWidth: 1,
							borderBottomColor: "lightgrey",
						}}>
							<View style={{
								backgroundColor: constants.backgroundPageColor,
								height: 35, paddingStart: 10,
								justifyContent: "center"
							}}>
								<Text style={{
									color: constants.green,
									fontFamily: constants.fonts.semiBold,
									fontSize: 14, textTransform: "capitalize"
								}}>{category.categoryDetails.name}</Text>
							</View>
							<FlatList data={category.stores}
								renderItem={({ item }) => this.renderStore(item)}
								keyExtractor={this.keyExtractor} />
						</View>
					))
				}
			</View>
		)
	}

	renderStores = () => {
		return (
			<View style={{ flex: 1, backgroundColor: "white" }}>
				<FlatList data={this.state.categoryStores}
					renderItem={({ item }) => this.renderStore(item)}
					keyExtractor={this.keyExtractor} />
			</View>
		)
	}

	renderButtonbars = () => {
		let { screenWidth, categories, categoryId } = this.state;

		return (
			<View style={{
				height: 40,
				width: screenWidth,
				backgroundColor: "white"
			}}>
				<ScrollView horizontal={true}
					keyboardShouldPersistTaps={'handled'}
					showsHorizontalScrollIndicator={false}>
					<TouchableOpacity style={[styles.categoryBtn, { borderBottomWidth: categoryId === 1 ? 1.5 : 0 }]} å
						onPress={() => {
							this.setState({ categoryId: 1 });
						}}>
						<Text style={{
							fontSize: 14,
							color: categoryId === 1 ? constants.green : constants.black,
							fontFamily: constants.fonts.semiBold,
						}}>All </Text>
					</TouchableOpacity>
					{
						categories.map((category, index) =>
							(
								<TouchableOpacity style={[styles.categoryBtn, { borderBottomWidth: categoryId === category._id ? 1.5 : 0 }]} key={index}
									onPress={() => {
										this.setState({ categoryId: category._id, categoryStores: category.stores });
									}}>
									<Text style={{
										fontSize: 14,
										color: categoryId === category._id ? constants.green : constants.black,
										fontFamily: constants.fonts.semiBold,
									}}>{category.categoryDetails.name} </Text>
								</TouchableOpacity>
							))
					}
				</ScrollView>
			</View>

		)
	}

	storeListRender = () => {
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-start',
				marginBottom: 50
			}}>
				{
					this.state.stores.map((store, index) =>
						(
							<TouchableOpacity style={{
								height: 100,
								width: "100%",
								flexDirection: "row",
								justifyContent: "flex-start",
								backgroundColor: "white",
								borderBottomWidth: 8,
								paddingLeft: 5,
								paddingRightt: 5,
								borderColor: constants.backgroundPageColor,
							}} key={index} onPress={async () => {
								console.log("Setting store id is:--" + store._id);
								// cart = cart ? JSON.parse(cart) : []
								// if (cart) {
								// 	constants.cartArea = constants.area
								// 	constants.cartCity = constants.city
								// 	await AsyncStorage.setItem("cartArea", JSON.stringify(this.state.area));
								// }
								constants.store = store;
								AsyncStorage.setItem('store', JSON.stringify(store), () => {
									if (this.props.navigation.state.routeName == "SetStoreInHome") {
										DeviceEventEmitter.emit("storeUpdate");
										NavigationService.navigate("Home");
									} else {
										NavigationService.navigate("MainStack");
									}
								});
							}}>
								<View style={{ width: 90, height: 100, justifyContent: "center", alignItems: "center" }}>
									<View style={{
										width: 60, height: 60, borderRadius: 30,
										borderWidth: 1,
										borderColor: constants.backgroundPageColor
									}}>
										<Image source={store.picture ? store.imgUrl : Images.gift}
											style={{
												width: 60, height: 60,
												resizeMode: "contain",
												borderRadius: 30,
											}}></Image>
									</View>
								</View>
								<View style={{
									width: (this.state.screenWidth - 90),
									flexDirection: "column",
									justifyContent: "space-evenly",
									alignItems: "center",
									height: "95%"
								}}>
									<View style={{
										width: "100%",
										flexDirection: "row",
										height: 50,
										justifyContent: "space-between",
										alignItems: "flex-end",
										paddingRight: 15
									}}>
										<Text style={{
											fontSize: 15,
											color: "black", fontFamily: constants.fonts.bold
										}}>{store.name}</Text>
										<View style={{
											flexDirection: "row",
											width: "auto",
											justifyContent: "center",
											alignItems: "center"
										}}>
											<Image source={Images.clock_circle} style={{ width: 15, height: 15, marginRight: 5 }}></Image>
											<Text style={{
												fontSize: 12,
												color: "black", fontFamily: constants.fonts.medium
											}}>{store.has_express_delivery ? "Instant" : "Scheduled"} Delivery</Text>
										</View>
									</View>
									<View style={{
										width: "100%",
										height: 50,
										maxHeight: "50%", justifyContent: "center"
									}}>
										<Text style={{
											fontSize: 12,
											color: "black", fontFamily: constants.fonts.medium
										}}>Shop no. {store.address.shop_no}, {this.state.area.name}</Text>
									</View>
								</View>
							</TouchableOpacity>
						))
				}
			</View >)
	}

	render() {
		let { screenWidth, categories, categoryId } = this.state;
		return (
			<SafeAreaView forceInset={{ bottom: 'never' }} style={{
				flex: 1,
				backgroundColor: constants.green,
				justifyContent: "flex-start",
				alignItems: "center"
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<View style={{
					height: 55, width: "100%",
					justifyContent: "center",
					alignItems: "center",
					// backgroundColor: "red"
					backgroundColor: constants.green,
				}}>
					<Text style={{
						fontSize: 20,
						color: 'white',
						fontFamily: constants.fonts.bold,
						marginTop: -10
					}}>Select Store</Text>
				</View>
				<View style={{
					width: "100%",
					height: (this.state.screenHeight - 55),
					justifyContent: "center",
					alignItems: "center",
					backgroundColor: "white"
				}}>
					{this.renderButtonbars()}
					<ScrollView style={{ width: "100%" }}>
						{
							categoryId === 1 ?
								this.renderCategories() :
								this.renderStores()
						}
						{/* {this.storeListRender()} */}
					</ScrollView>
				</View>
				<StatusBar backgroundColor={constants.green} barStyle="light-content" />
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	categoryBtn: {
		width: "auto",
		height: 40,
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
		paddingLeft: 15,
		paddingRight: 15,
		borderBottomColor: constants.green
	}
})