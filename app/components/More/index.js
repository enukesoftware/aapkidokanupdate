import React, { Component } from 'react';
import { View, Alert, Text, Image, Dimensions, TouchableOpacity, StyleSheet, Animated, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import * as SocialHelper from '../../apimanager/social';
import { GoogleSignin } from 'react-native-google-signin';
import { NavigationEvents } from "react-navigation";
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
import NotificationSetting from 'react-native-open-notification';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Images from '../../global/images';
import WebViewExample from '../../global/webview';

export default class MoreScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			user: {},
			headerHeight: 0,
			version: "",
			loading: false,
			visibleWebView: false,
			webviewTitle: "Frequently Asked Question's",
			webViewUrl: "https://www.google.com/",
			store: {}
		};
	}

	componentDidMount = async () => {
		await AsyncStorage.removeItem("fcmToken");
	}

	proceed = () => {
		constants.backStateForOrders = this.props.navigation.state.routeName;
		NavigationService.navigate("MyOrdersScreen")
	}

	updateFcmToken = (token) => {
		this.setState({ loading: true });
		ApiManager.updateFcmToken(token).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(async () => {
				if (res && res.success && res.code == 200) {
					this.proceed();
				} else {
					Toast.showWithGravity(res.singleStringMessage ? res.singleStringMessage : "Undefined error", Toast.LONG, Toast.CENTER);
					this.proceed();
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				console.log(JSON.stringify(err));
				this.proceed();
			}, 500);
		})
	}

	alertNotification = () => {
		Alert.alert(
			'',
			'App requires permission of notification to send you updates of order and status. Do you want to continue?',
			[
				{
					text: 'OK',
					onPress: () => {
						NotificationSetting.open();
					},
					style: 'cancel',
				},
				{
					text: 'CANCEL',
					onPress: () => {
						Toast.showWithGravity("You will not receive notifications for any orders", Toast.SHORT, Toast.BOTTOM);
						this.proceed();
					}
				}
			],
			{ cancelable: false },
		);
	}

	checkToken = async () => {
		const fcmToken = await AsyncStorage.getItem("fcmToken");
		if (fcmToken) {
			constants.FCM_TOKEN = fcmToken;
			this.updateFcmToken(fcmToken);
		} else {
			this.checkPermission();
		}
	}

	//1
	checkPermission = () => {
		firebase.messaging().hasPermission().then(enabled => {
			if (enabled) {
				this.getToken();
			} else {
				this.requestPermission();
			}
		});
	}

	//3
	getToken = () => {
		firebase.messaging().getToken().then(async (fcmToken) => {
			if (fcmToken) {
				// user has a device token
				console.log("fcm id is :--" + fcmToken);
				constants.FCM_TOKEN = fcmToken;
				await AsyncStorage.setItem('fcmToken', fcmToken);
				this.updateFcmToken(fcmToken);
				// this.checkLoginCase();
			}
		}).catch(err => {
			console.log("token error")
			throw new Error(err)
		})
	}

	//2
	requestPermission = () => {
		firebase.messaging().requestPermission()
			.then(() => {
				this.getToken();
			})
			.catch(error => {
				console.log("User has rejected permission");
				this.alertNotification();
			});
	}

	refreshPage = async () => {
		this.fetchUser();
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity("Internet is not available", Toast.SHORT, Toast.BOTTOM);
			return
		}
		SocialHelper._configureGoogleSignIn();
		const store = await AsyncStorage.getItem("store");
		// console.log("store is :--" + store);
		this.setState({
			store: store ? JSON.parse(store) : {}
		});
	}

	fetchUser = () => {
		AsyncStorage.getItem("user").then(res => {
			// console.log(res);
			this.setState({ user: res ? JSON.parse(res) : {}, version: constants.version }, () => {
				console.log(this.state.user);
			});
		})
	}

	logOut = () => {
		this.setState({ loading: true });
		ApiManager.logOut().then((res) => {
			this.setState({ loading: false });
			this.setState({ user: {} }, () => {
				constants.user = {};
				constants.coupon = {};
				constants.selectAddress = {};
				constants.selectedSlot = {};
			}, () => {
				console.log("Your are now logged out");
			});
		}).catch((err) => {
			this.setState({ loading: false });
			console.log(JSON.stringify(err));
		})
	}

	renderFaqs = () => {
		let { store } = this.state;
		let url = constants.baseUrl + "store-info/" + (store ? store._id : "");
		console.log("url is:--" + url);
		return (
			<View>
				<View style={{ height: 35, justifyContent: "center", paddingLeft: 20 }}>
					<Text style={styles.btnText}>Aapkidokan</Text>
				</View>
				<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
					onPress={() => {
						this.setState({
							visibleWebView: true,
							webviewTitle: "FAQ's",
							webViewUrl: constants.baseUrl + "faq"
						}, () => {
							console.log("url is:--" + this.state.webViewUrl);
						})
					}}>
					<Text style={styles.btnText}>FAQ's</Text>
				</TouchableOpacity>
				<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
					onPress={() => {
						this.setState({
							visibleWebView: true,
							webviewTitle: "Term's & Condition's",
							webViewUrl: constants.baseUrl + "terms-and-conditions"
						}, () => {
							console.log("url is:--" + this.state.webViewUrl);
						})
					}}>
					<Text style={styles.btnText}>Term's & Condition's</Text>
				</TouchableOpacity>
				<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
					onPress={() => {
						this.setState({
							visibleWebView: true,
							webviewTitle: "Privacy Policie's",
							webViewUrl: constants.baseUrl + "privacy-policy"
						}, () => {
							console.log("url is:--" + this.state.webViewUrl);
						})
					}}>
					<Text style={styles.btnText}>Privacy Policie's</Text>
				</TouchableOpacity>
				<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
					onPress={() => {
						this.setState({
							visibleWebView: true,
							webviewTitle: "Contact's Info",
							webViewUrl: constants.baseUrl + "contact-us"
						}, () => {
							console.log("url is:--" + this.state.webViewUrl);
						})
					}}>
					<Text style={styles.btnText}>Contact's Info</Text>
				</TouchableOpacity>
				{
					store._id ?
						<>
							<View style={{ height: 35, justifyContent: "center", paddingLeft: 20 }}>
								<Text style={styles.btnText}>Store:- {store.name}</Text>
							</View>
							<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
								onPress={() => {
									this.setState({
										visibleWebView: true,
										webviewTitle: "FAQ's",
										webViewUrl: url + "/faq"
									}, () => {
										console.log("url is:--" + this.state.webViewUrl);
									})
								}}>
								<Text style={styles.btnText}>FAQ's</Text>
							</TouchableOpacity>
							<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
								onPress={() => {
									this.setState({
										visibleWebView: true,
										webviewTitle: "Term's & Condition's",
										webViewUrl: url + "/terms-and-conditions"
									}, () => {
										console.log("url is:--" + this.state.webViewUrl);
									})
								}}>
								<Text style={styles.btnText}>Term's & Condition's</Text>
							</TouchableOpacity>
							<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
								onPress={() => {
									this.setState({
										visibleWebView: true,
										webviewTitle: "Privacy Policie's", webViewUrl: url + "/privacy-policy"
									}, () => {
										console.log("url is:--" + this.state.webViewUrl);
									})
								}}>
								<Text style={styles.btnText}>Privacy Policie's</Text>
							</TouchableOpacity>
							<TouchableOpacity activeOpacity={0.8} style={styles.linkView}
								onPress={() => {
									this.setState({
										visibleWebView: true,
										webviewTitle: "Contact's Info", webViewUrl: url + "/contact-us"
									}, () => {
										console.log("url is:--" + this.state.webViewUrl);
									})
								}}>
								<Text style={styles.btnText}>Contact's Info</Text>
							</TouchableOpacity></> : <View />
				}
			</View>
		)
	}

	moreList = () => {
		let { user } = this.state;
		return (
			<View style={{
				flex: 1,
				paddingTop: (this.state.headerHeight - 15),
				alignItems: 'center',
				justifyContent: 'flex-start',
				backgroundColor: constants.backgroundPageColor,
			}}>
				{
					user._id ?
						<View style={{ width: "100%", height: "auto" }}>
							<TouchableOpacity activeOpacity={0.8} style={styles.buttonView} onPress={() => this.checkToken()}>
								<View style={{ width: 70, height: 50, justifyContent: "center", alignItems: "center" }}>
									<Image source={Images.file} style={{ width: 23, height: 28 }}></Image>
								</View>
								<View>
									<Text style={styles.btnText}>My Orders</Text>
								</View>
							</TouchableOpacity>
							<TouchableOpacity activeOpacity={0.8} style={styles.buttonView} onPress={() => {
								NavigationService.navigate('Profile', {
									onNavigateBack: this.fetchUser
								});
							}}>
								<View style={{ width: 70, height: 50, justifyContent: "center", alignItems: "center" }}>
									<Image source={Images.user} style={{ width: 27, height: 27 }}></Image>
								</View>
								<View>
									<Text style={styles.btnText}>My Profile</Text>
								</View>
							</TouchableOpacity>
							<TouchableOpacity activeOpacity={0.8} style={styles.buttonView} onPress={() => NavigationService.navigate("MyAddress")}>
								<View style={{ width: 70, height: 50, justifyContent: "center", alignItems: "center" }}>
									<Image source={Images.pin} style={{ width: 15, height: 25 }}></Image>
								</View>
								<View>
									<Text style={styles.btnText}>My Address</Text>
								</View>
							</TouchableOpacity>
							{this.renderFaqs()}
							<TouchableOpacity activeOpacity={0.8} style={styles.buttonView} onPress={() => this.logOut()}>
								<View style={{ width: 70, height: 50, justifyContent: "center", alignItems: "center" }}>
									<Image source={require("../../assets/images/logout.png")} style={{ width: 25, height: 20 }}></Image>
								</View>
								<View>
									<Text style={styles.btnText}>Logout</Text>
								</View>
							</TouchableOpacity>

						</View> :
						<View style={{ width: "100%", height: "auto" }}>
							{this.renderFaqs()}
							<TouchableOpacity activeOpacity={0.8} style={styles.buttonView} onPress={() => {
								constants.previousStateForLogin = "Home";
								NavigationService.navigate("OtherLoginStack")
							}}>
								<View style={{ width: 70, height: 50, justifyContent: "center", alignItems: "center" }}>
									<Image source={Images.logout} style={{ width: 25, height: 20 }}></Image>
								</View>
								<View>
									<Text style={styles.btnText}>Log In</Text>
								</View>
							</TouchableOpacity>
						</View>
				}
				<View style={{
					height: 60,
					width: "100%",
					justifyContent: "center",
					alignItems: "center"
				}}>
					<Text style={styles.btnText}>Version {this.state.version}</Text>
				</View>
			</View>
		)
	}

	closeWebView = () => {
		this.setState({ visibleWebView: false });
	}

	render() {
		let { visibleWebView, webviewTitle, webViewUrl } = this.state;
		console.log("More page");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<WebViewExample visible={visibleWebView}
					title={webviewTitle}
					closeWebView={this.closeWebView}
					webViewUrl={webViewUrl}></WebViewExample>
				<NavigationEvents
					onWillFocus={() => {
						this.refreshPage();
						//Call whatever logic or dispatch redux actions and update the screen!
					}} />
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.moreList()}
				</Animated.ScrollView>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: {
							height: (getStatusBarHeight(true) + 40),
						}
					}),
					backgroundColor: '#1faf4b',
					// backgroundColor: 'blue',
					paddingBottom: 10
				}}>
					<Animated.View style={{
						height: '100%',
						marginLeft: 25,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						// backgroundColor: 'blue',
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<Text style={{
							fontSize: 18, color: 'white',
							fontFamily: constants.fonts.bold,
						}}>More</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	buttonView: {
		height: 60,
		width: "100%",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "white",
		borderBottomWidth: 3,
		borderColor: constants.backgroundPageColor
	},
	linkView: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "white",
		borderBottomWidth: 3,
		borderColor: constants.backgroundPageColor,
		paddingLeft: 35,
		height: 45
	},
	btnText: { fontSize: 14, color: "black", fontFamily: constants.fonts.medium }
})