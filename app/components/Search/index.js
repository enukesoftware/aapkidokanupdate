import React, { Component } from 'react';
import { View, Dimensions, TextInput, ActivityIndicator, FlatList, Text, Alert, Animated, StatusBar, Image, TouchableOpacity, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import styles from './styles';
import CardView from 'react-native-cardview';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import OffScreen from '../../global/off';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class SearchScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	state = {
		scrollY: new Animated.Value(0.01),
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		products: [],
		autoFocus: true,
		headerHeight: 0,
		searching: true,
		searchText: "",
		screenMessage: "Search something in your store . . .",
		pageNo: 1,
		isLoading: false,
		isRefreshing: false,
		isListEnd: false,
		loadingMore: false,
	};

	// searchMessage = "Search something in store . . ."
	searchWaiting = null;

	getProducts = () => {
		console.log("getting products");
		let { pageNo, products, searchText, screenMessage } = this.state;
		this.setState({ isLoading: true });
		ApiManager.getProducts(null, searchText, pageNo).then((res) => {
			// console.clear();
			// console.log(JSON.stringify(res.data.products));
			setTimeout(() => {
				this.setState({
					isLoading: false,
					isRefreshing: false,
				}, () => console.log("loading paused"));
			}, 100)
			setTimeout(() => {
				if (res && res.success) {
					this.setState({
						isListEnd: (!res.data.products.length || res.data.products.length < 30),
						loadingMore: false,
						products: pageNo === 1 ? res.data.products : [...products, ...res.data.products],
						screenMessage: !res.data.products.length ? "No results found for this product\nPlease search something else \n or \n change the store . . ." : ""
					}, () => {
						console.log(products.length + " products has been set into state")
						this.calculateCartItem();
					});
				} else {
					this.setState({
						screenMessage: err.singleStringMessage
					}, () => {
						console.log(screenMessage);
					})
				}
			}, 200);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ isLoading: false });
			}, 100);
			this.setState({
				screenMessage: err.singleStringMessage
			}, () => {
				console.log(this.state.screenMessage);
			})
			// Alert.alert(err.singleStringMessage);
			console.log("Getting products error" + JSON.stringify(err));
		})
	}

	calculateCartItem = () => {
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			counter = counter + cart[i].count;
		}
		this.setState({ totalCartItem: counter }, () => {
			console.log("total items" + this.state.totalCartItem);
		});
	}

	removeProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				if (stateProducts[index].count && stateProducts[index].count > 0) {
					stateProducts[index].count--;
				}
				cart[i].count = stateProducts[index].count;
				if (cart[i].count == 0) cart.splice(i, 1);
				break;
			}
			if (!cart[i].count || cart[i].count < 1) {
				cart.splice(i, 1);
			}
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts,
			}, () => {
				console.log("update featured products");
				this.calculateCartItem();
			})
		});
	}

	addProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				counter++;
				stateProducts[index].count++;
				cart[i].count = stateProducts[index].count;
				break;
			}
		}
		if (counter == 0) {
			stateProducts[index].count = 1;
			cart.push(stateProducts[index]);
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts
			}, async () => {
				console.log("update featured products");
				this.calculateCartItem();
				constants.cartStore = constants.store;
				constants.cartCity = constants.city;
				constants.cartArea = constants.area;
				await AsyncStorage.setItem("cartStore", JSON.stringify(constants.store));
				await AsyncStorage.setItem("cartCity", JSON.stringify(constants.city));
				await AsyncStorage.setItem("cartArea", JSON.stringify(constants.area));
			})
		});
	}

	changedStoreCheck = () => {
		if (this.state.storeChanged) {
			Alert.alert(
				'Alert',
				'Your basket has products from another store. Clear basket to shop from this store?',
				[
					{
						text: 'CLEAR',
						onPress: () => {
							AsyncStorage.removeItem("cart").then(() => {
								this.props.setCart([]);
								// this.addProduct(index, type)
								this.calculateCartItem();
							})
						},
						style: 'cancel',
					},
					{
						text: 'CANCEL',
						onPress: () => console.log('Cancel Pressed')
					}
				],
				{ cancelable: false },
			);
		} else this.goToCart();
	}

	goToCart = () => {
		constants.previousStateForCart = this.props.navigation.state.routeName;
		NavigationService.navigate("SearchCart");
	}

	renderProduct = (product, index) => {
		return (
			<CardView cardElevation={5}
				cardMaxElevation={5}
				cornerRadius={5}
				style={[styles.productBox, { marginBottom: 15 }]}>
				<OffScreen offValue={product.off}></OffScreen>
				<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }}
					style={styles.productImage}></Image>
				<View style={styles.productQuantity}>
					<Text style={{ fontSize: 10, color: "black", fontFamily: constants.fonts.semiBold, color: "black" }}>{product.size}</Text>
				</View>
				<Text style={{
					color: "black", marginLeft: 3, marginRight: 3,
					fontFamily: constants.fonts.medium, fontSize: 13,
					textAlign: "center"
				}} numberOfLines={2}>{product.name}</Text>
				<View style={{
					flexDirection: "row", width: "100%", flexWrap: "wrap",
					justifyContent: "center", paddingLeft: 5, paddingRight: 5
				}}>
					{
						(product.price.cost_price && product.price.cost_price != product.price.sale_price) ?
							<Text style={{
								color: "black", textDecorationLine: "line-through",
								fontFamily: constants.fonts.semiBold, fontSize: 11
							}}>{product.price.cost_price} {constants.currency}</Text> : null
					}
					<Text style={{
						color: constants.green, marginLeft: 5,
						fontFamily: constants.fonts.semiBold, fontSize: 11
					}}>{product.price.sale_price} {constants.currency}</Text>
				</View>
				<View style={[styles.productCounter, {
					borderColor: product.count > 0 ? constants.green : "lightgrey"
				}]}>
					<TouchableOpacity style={[styles.counterButton, {
						borderColor: product.count > 0 ? constants.green : "lightgrey",
						backgroundColor: product.count > 0 ? constants.green : "transparent"
					}]} onPress={() => this.removeProduct(index)}>
						<Text style={{
							fontSize: 25,
							marginTop: -4,
							color: product.count > 0 ? "white" : "black"
						}}>-</Text>
					</TouchableOpacity>
					<Text style={{ fontSize: 11, color: "black", fontFamily: constants.fonts.medium }}>
						{product.count > 0 ? product.count : "Add To Cart"}
					</Text>
					<TouchableOpacity style={[styles.counterButton, {
						backgroundColor: product.count > 0 ? constants.green : "transparent",
						borderColor: product.count > 0 ? constants.green : "lightgrey"
					}]} onPress={() => {
						if (product.count >= product.order_max) {
							Toast.showWithGravity("Your maximum limit has been reached for this product . . .", Toast.SHORT, Toast.CENTER);
						} else if (product.count >= product.stock_quantity) {
							Toast.showWithGravity("Product unavailable now . . .", Toast.SHORT, Toast.CENTER);
						} else this.addProduct(index)
					}}>
						<Text style={{
							fontSize: 20,
							marginTop: -4,
							color: product.count > 0 ? "white" : "black"
						}}>+</Text>
					</TouchableOpacity>
				</View>
			</CardView>
		)
	}

	keyExtractor = (item) => item._id;

	renderPageItems = () => {
		let { products, isRefreshing } = this.state;
		return (
			<View style={{
				flex: 1,
				paddingLeft: 7.5,
				paddingRight: 7.5
			}}>
				<FlatList data={products}
					showsVerticalScrollIndicator={false}
					renderItem={({ item, index }) => this.renderProduct(item, index)}
					numColumns={2}
					extraData={this.state}
					keyExtractor={this.keyExtractor}
					onEndReached={this.handleLoadMore}
					onEndReachedThreshold={.5}
					onRefresh={this.refreshProducts}
					refreshing={isRefreshing}
					renderFooter={this.renderFooter} />
				{/* {this.renderFooter()} */}
			</View>
		)
	}

	renderFooter = () => {
		let { isListEnd, loadingMore } = this.state;
		if (!loadingMore || isListEnd) {
			console.log("loadingMore:-" + loadingMore)
			return null;
		}
		return (
			<View
				style={{
					width: "100%",
					height: 50,
					marginTop: 10,
					marginBottom: 10,
					borderColor: constants.lightgreen,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator
					color={constants.green}
					animating
					size="large" />
			</View>
		);
	}

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getProducts();
			});
		}
	};

	refreshProducts = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getProducts();
			})
		}
	}

	renderBlankPage = () => {
		if (!this.state.products.length) {
			return (
				<View style={{
					flex: 1,
					height: ((Dimensions.get('window').height) - (this.state.headerHeight + 70)),
					justifyContent: "center",
					alignItems: "center"
				}}>
					<Text style={{
						fontSize: 17,
						color: "black",
						textAlign: "center",
						color: "black", fontFamily: constants.fonts.medium
					}}>{this.state.screenMessage}</Text>
				</View>
			)
		} else return null
	}

	render() {
		let { isLoading, isRefreshing } = this.state;

		const Screen = {
			heignt: Dimensions.get('window').height,
			width: Dimensions.get('window').width,
			scale: Dimensions.get('window').scale,
			fontScale: Dimensions.get('window').fontScale,
		};
		let animatBackgroundColor = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: [constants.green, '#1faf4b'],
			extrapolate: 'clamp'
		});

		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<NavigationEvents onWillFocus={() => {
					this.setState({
						autoFocus: true,
						searching: true,
						searchText: "",
						screenMessage: "Search something in store . . .",
						pageNo: 1,
						isRefreshing: false,
						isLoading: false
					}, () => {
						this.calculateCartItem();
						// this.getProducts();
					})
					//Call whatever logic or dispatch redux actions and update the screen!
					// }}
					const store = constants.store;
					let cart = JSON.parse(JSON.stringify(this.props.cart));
					let changedStore = false;
					for (let i = 0; i < cart.length; i++) {
						if (store._id != cart[i].store_id) {
							changedStore = true;
							break;
						}
					}
					this.setState({ storeChanged: changedStore }, () => {
						console.log("store changed:--" + this.state.storeChanged);
					});
					//Call whatever logic or dispatch redux actions and update the screen!
				}} />

				<Spinner size="large"
					visible={isLoading || isRefreshing}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.View style={{
					flex: 1,
					...Platform.select({
						ios: { paddingTop: (getStatusBarHeight(true) + 30), },
						android: { paddingTop: (getStatusBarHeight(true) + 30), }
					})
				}}>
					{this.renderBlankPage()}
					{this.renderPageItems()}
				</Animated.View>
				<Animated.View style={{
					top: 0, left: 0, right: 0, bottom: 0,
					position: 'absolute', flexDirection: 'row',
					backgroundColor: animatBackgroundColor,
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 45), },
						android: { height: (getStatusBarHeight(true) + 40), }
					}),
					alignItems: "center"
					// paddingBottom: 7
				}} onLayout={(event) => {
					this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
						console.log("Header height is:--" + this.state.headerHeight);
					})
					console.log(event.nativeEvent.layout);
				}}>
					<Animated.View style={{
						height: '100%',
						justifyContent: 'center',
						width: "100%",
						// backgroundColor: "blue",
						alignItems: "center"
					}}>
						{this.state.searching ?
							<View style={{
								width: "100%",
								height: "100%",
								justifyContent: "flex-end",
								// backgroundColor: "yellow",
								paddingBottom: "5%",
								marginLeft: 5
							}}>
								<TextInput
									style={{
										width: "73%",
										height: 42,
										paddingStart: 15,
										borderColor: constants.backgroundPageColor,
										borderWidth: 1,
										backgroundColor: "white",
										borderRadius: 25
									}}
									value={this.state.searchText}
									placeholder='Search Item in Grocery Shop'
									placeholderTextColor={constants.placeHolderColor}
									autoFocus={this.state.autoFocus}
									onChangeText={(text) => {
										this.setState({ searchText: text, pageNo: 1 }, () => {
											if (this.state.searchText.length >= 3) {
												this.searchWaiting = setTimeout(() => {
													this.searchWaiting = clearTimeout(this.searchWaiting);
													this.setState({ products: [] }, () => {
														this.getProducts();
													})
												}, 2000);
											}
											if (this.state.searchText == "") {
												this.setState({ products: [], screenMessage: "Search Something in Store . . ." });
											}
										})
									}}></TextInput>
							</View> :
							<Text style={{
								fontSize: 16,
								color: 'white', fontFamily: constants.fonts.bold,
								...Platform.select({ ios: { marginTop: 30 }, android: {} })
							}}>Search Result</Text>
						}
					</Animated.View>
					<Animated.View style={{
						height: '100%', width: '25%',
						justifyContent: 'flex-end',
						alignItems: 'center',
						position: "absolute",
						right: 0,
					}}>
						<View style={{
							width: "100%",
							height: 42,
							flexDirection: 'row',
							justifyContent: "center",
							alignItems: "flex-end",
							paddingBottom: "5%",
						}}>
							<TouchableOpacity style={{
								height: 35, width: 35, marginLeft: 5,
								// ...Platform.select({
								// 	ios: { marginTop: 40 },
								// 	android: { marginTop: 10 }
								// })
							}} underlayColor='transparent' onPress={() => {
								this.setState({ searching: !this.state.searching, searchText: "" });
							}}>
								{this.state.searching ?
									<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.closeImg} />
									:
									<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.search} />
								}
							</TouchableOpacity>
							<TouchableOpacity style={{
								height: 35,
								width: 35,
								...Platform.select({
									ios: { marginTop: 40 },
									android: { marginTop: 10 }
								})
							}} underlayColor='transparent' onPress={() => {
								{
									this.state.totalCartItem ?
										this.changedStoreCheck() :
										Alert.alert("Please add few items in cart");
								}
							}}>
								{
									this.state.totalCartItem ?
										<View style={{
											backgroundColor: "#e9001f",
											width: "auto",
											height: "auto",
											paddingTop: this.state.storeChanged ? 5 : 2,
											paddingBottom: this.state.storeChanged ? 4 : 3,
											paddingLeft: 7,
											paddingRight: 8,
											position: "absolute",
											zIndex: 999,
											top: -12,
											left: -8,
											justifyContent: "center",
											alignItems: "center",
											borderRadius: 15
										}}>
											{
												this.state.storeChanged ?
													<Image source={Images.closeImg} style={{ height: 10, width: 10 }} /> :
													<Text style={{
														color: "white",
														fontFamily: constants.fonts.bold, fontSize: 10
													}}>{this.state.totalCartItem}</Text>
											}
										</View> : null
								}
								<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
							</TouchableOpacity>

						</View>
					</Animated.View>
				</Animated.View>
				<StatusBar backgroundColor={constants.green} barStyle="light-content" />
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)