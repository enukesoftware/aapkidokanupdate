import React, { Component } from 'react';
import { Dimensions, View, DeviceEventEmitter, TouchableOpacity, Text, Alert, Animated, StatusBar, Image, TouchableHighlight, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Images from '../../global/images';
import constants from '../../constants/constants';
import styles from './styles';
import CardView from 'react-native-cardview'
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import OffScreen from '../../global/off';
import NavigationService from '../../global/NavigationService';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class HomeScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			featured_products: [],
			best_selling_products: [],
			categories: [],
			loading: false,
			totalCartItem: 0,
			storeData: {},
			area: {},
			city: {},
			headerHeight: 0,
			storeChanged: false
		};
	}

	static navigationOptions = () => {
		return {
			header: null
		}
	};

	componentWillMount = () => {
		console.log("Home page");
		this.calculateCartItem();
		// this.getStores();
	}

	componentDidMount = async () => {
		console.log("current route name is :-- " + this.props.navigation.state.routeName);
		DeviceEventEmitter.addListener("storeUpdate", this.handleOnNavigationFromStore);
		let token = await AsyncStorage.getItem("token");
		if (token) {
			ApiManager.getProfile();
		}
	}

	getStores = () => {
		console.log("getting stores");
		this.setState({
			loading: true,
			city: constants.city,
			area: constants.area
		});
		ApiManager.getStoresForHome().then((res) => {
			console.log(res)
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(async () => {
				if (res && res.success) {
					this.setState({
						storeData: res.data.store,
						featured_products: res.data.featured_products,
						best_selling_products: res.data.best_selling_products,
						categories: res.data.categories,
					}, () => {
						this.changedStoreCheck()
						this.calculateCartItem();
						// this.goToSubCategory(res.data.categories[0]);
					});
					res.data.delivery_charges.unshift({ order_amount: 0, charges: 0 });
					constants.delivery_charges = res.data.delivery_charges;
					await AsyncStorage.setItem("delivery_charges", JSON.stringify(res.data.delivery_charges));
					await AsyncStorage.setItem("storeData", JSON.stringify(res.data.store));
					constants.store = res.data.store;
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false });
			}, 100);
			Alert.alert(err.singleStringMessage);
			console.log("Otp error" + JSON.stringify(err));
		})
	}

	calculateCartItem = () => {
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let totalCartItem = 0;
		for (let i = 0; i < cart.length; i++) {
			totalCartItem += cart[i].count;
		}
		this.setState({ totalCartItem }, () => {
			console.log("total items" + this.state.totalCartItem);
		});
	}

	checkStoreForAdd2Cart = (index, type) => {
		let { best_selling_products, featured_products } = this.state;
		let selectedProducts = type === 2 ? best_selling_products : featured_products;
		product = selectedProducts[index];
		let { cart } = this.props;
		let changedStore = false;
		for (let i = 0; i < cart.length; i++) {
			if (product.store_id != cart[i].store_id) {
				changedStore = true;
				break;
			}
		}
		if (changedStore) {
			this.clearCartAlert();
		} else this.addProduct(index, type);
	}

	clearCartAlert = () => {
		Alert.alert(
			'Watch Out',
			'You have an active shopping cart from a different store. That cart will be cleared. Do you want to continue?',
			[
				{
					text: 'CLEAR CART',
					onPress: () => {
						AsyncStorage.removeItem("cart").then(() => {
							// this.addProduct(index, type)
							this.props.setCart([]);
							this.setState({ storeChanged: false }, () => {
								this.calculateCartItem();
							})
						})
					}
				},
				{
					text: 'CANCEL',
					onPress: () => console.log('Cancel Pressed'),
					style: 'cancel',
				}
			],
			{ cancelable: false },
		);
	}

	changedStoreCheck = (data) => {
		if (this.state.storeChanged) {
			Alert.alert(
				'Alert',
				'Your basket has products from another store. Clear basket to shop from this store?',
				[
					{
						text: 'CLEAR',
						onPress: () => {
							AsyncStorage.removeItem("cart").then(() => {
								// this.addProduct(index, type)
								this.props.setCart([]);
								this.setState({ storeChanged: false }, () => {
									this.calculateCartItem();
									this.getStores();
								})
							})
						},
					},
					{
						text: 'CANCEL',
						onPress: () => {
							if (constants.area != constants.cartArea) {
								constants.city = constants.cartCity;
								constants.area = constants.cartArea;
								this.setState({
									area: constants.cartArea,
									city: constants.cartCity
								})
							}
							this.setState({ loading: false, storeChanged: false }, async () => {
								await AsyncStorage.setItem("store", JSON.stringify(constants.cartStore));
								constants.store = constants.cartStore;
								this.getStores();
							})
						},
						style: 'cancel',
					}
				],
				{ cancelable: false },
			);
		} else if (data && data == 1) this.goToCart();
	}

	addProduct = async (index, type) => {
		let { best_selling_products, featured_products } = this.state;
		let selectedProducts = type === 2 ? best_selling_products : featured_products;
		product = selectedProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				counter++;
				selectedProducts[index].count++;
				cart[i].count = selectedProducts[index].count;
				break;
			}
		}
		if (counter == 0) {
			selectedProducts[index].count = 1;
			cart.push(selectedProducts[index]);
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			let unSelectedProducts = type === 1 ? best_selling_products : featured_products;
			for (let i = 0; i < selectedProducts.length; i++) {
				for (let j = 0; j < unSelectedProducts.length; j++) {
					if (selectedProducts[i]._id === unSelectedProducts[j]._id) {
						unSelectedProducts[j].count = selectedProducts[i].count;
					}
				}
			}
			let bestselling = []
			let featureProduct = []
			if (type === 2) {
				console.log(selectedProducts)
				bestselling = best_selling_products;
				featureProduct = unSelectedProducts;
			}
			else {
				bestselling = unSelectedProducts;
				featureProduct = featured_products
			}
			this.setState({
				featured_products: featureProduct,
				best_selling_products: bestselling
			}, async () => {
				this.calculateCartItem();
				constants.cartStore = constants.store;
				constants.cartCity = constants.city;
				constants.cartArea = constants.area;
				await AsyncStorage.setItem("cartStore", JSON.stringify(constants.store));
				await AsyncStorage.setItem("cartCity", JSON.stringify(constants.city));
				await AsyncStorage.setItem("cartArea", JSON.stringify(constants.area));
				console.log("update featured products");
			})
		});
	}

	removeProduct = (index, type) => {
		let { best_selling_products, featured_products } = this.state;
		let selectedProducts = type === 2 ? best_selling_products : featured_products;
		product = selectedProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				// counter++;
				if (selectedProducts[index].count && selectedProducts[index].count > 0) {
					selectedProducts[index].count--;
				}
				cart[i].count = selectedProducts[index].count;
				if (cart[i].count == 0) cart.splice(i, 1);
				break;
			}
			if (!cart[i].count || cart[i].count < 1) {
				cart.splice(i, 1);
			}
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			let unSelectedProducts = type == 1 ? best_selling_products : featured_products;
			for (let i = 0; i < selectedProducts.length; i++) {
				for (let j = 0; j < unSelectedProducts.length; j++) {
					if (selectedProducts[i]._id == unSelectedProducts[j]._id) {
						unSelectedProducts[j].count = selectedProducts[i].count;
					}
				}
			}
			let bestselling = []
			let featureProduct = []
			if (type === 2) {
				console.log(selectedProducts)
				bestselling = best_selling_products;
				featureProduct = unSelectedProducts;
			}
			else {
				bestselling = unSelectedProducts;
				featureProduct = featured_products
			}
			this.setState({
				featured_products: featureProduct,
				best_selling_products: bestselling
			}, () => {
				console.log("update featured products");
				this.calculateCartItem();
			})
		});
	}

	renderFeaturedProductList = () => {
		return (
			<View style={{
				height: 290,
				alignItems: 'flex-start',
				justifyContent: "flex-start",
				shadowColor: 'dimgrey',
				marginLeft: 7,
				marginRight: 7,
				marginTop: 10,
			}}>
				<View style={styles.headerlayer}>
					<Text style={{ fontSize: 15, color: "black", fontFamily: constants.fonts.medium, }}>Featured Products</Text>
					<TouchableOpacity style={{
						height: 30, width: 50,
						justifyContent: "center",
						alignItems: "center"
					}} onPress={() => {
						constants.products = this.state.featured_products;
						NavigationService.navigate("AllProducts", { title: "Featured Products", storeChanged: this.state.storeChanged });
					}}>
						<Text style={{ fontSize: 10, color: 'black', color: "black", fontFamily: constants.fonts.medium }}>View All</Text>
					</TouchableOpacity>
				</View>
				<ScrollView horizontal={true}
					showsHorizontalScrollIndicator={false}>
					{
						this.state.featured_products.map((product, index) =>
							(
								<CardView cardElevation={5}
									cardMaxElevation={5}
									cornerRadius={5}
									style={styles.productBox} key={index}>
									<OffScreen offValue={product.off}></OffScreen>
									<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }}
										style={styles.productImage}></Image>
									<View style={styles.productQuantity}>
										<Text style={{
											fontSize: 10,
											color: "black", fontFamily: constants.fonts.semiBold, color: "black"
										}}>{product.size}</Text>
									</View>
									<Text style={{
										color: "black", fontSize: 13,
										fontFamily: constants.fonts.medium,
										marginLeft: 3, marginRight: 3, textAlign: "center"
									}} numberOfLines={2}>{product.name} </Text>
									<View style={{
										flexDirection: "row", width: "100%", flexWrap: "wrap",
										justifyContent: "center", paddingLeft: 5, paddingRight: 5
									}}>
										{
											(product.price.cost_price && product.price.cost_price != product.price.sale_price) ?
												<Text style={{
													color: "black", textDecorationLine: "line-through",
													fontFamily: constants.fonts.semiBold,
													fontSize: 11
												}}>{product.price.cost_price} {constants.currency}</Text> : null
										}
										<Text style={{
											color: constants.green, marginLeft: 5,
											fontFamily: constants.fonts.semiBold,
											fontSize: 11
										}}>{product.price.sale_price} {constants.currency}</Text>
									</View>
									<View style={[styles.productCounter, {
										borderColor: product.count > 0 ? constants.green : "lightgrey"
									}]}>
										<TouchableOpacity style={[styles.counterButton, {
											borderColor: product.count > 0 ? constants.green : "lightgrey",
											backgroundColor: product.count > 0 ? constants.green : "transparent"
										}]} onPress={() => this.removeProduct(index, 1)}>
											<Text style={{
												fontSize: 25,
												marginTop: -4,
												color: product.count > 0 ? "white" : "black"
											}}>-</Text>
										</TouchableOpacity>
										<Text style={{ fontSize: 11, color: "black", fontFamily: constants.fonts.medium }}>
											{product.count > 0 ? product.count : "Add To Cart"}
										</Text>
										<TouchableOpacity style={[styles.counterButton, {
											backgroundColor: product.count > 0 ? constants.green : "transparent",
											borderColor: product.count > 0 ? constants.green : "lightgrey"
										}]} onPress={() => {
											if (product.count >= product.order_max) {
												Toast.showWithGravity("Your maximum limit has been reached for this product . . .", Toast.SHORT, Toast.CENTER);
											} else if (product.count >= product.stock_quantity) {
												Toast.showWithGravity("Product unavailable now . . .", Toast.SHORT, Toast.CENTER);
											} else this.checkStoreForAdd2Cart(index, 1)
										}}>
											<Text style={{
												fontSize: 20,
												marginTop: -4,
												color: product.count > 0 ? "white" : "black"
											}}>+</Text>
										</TouchableOpacity>
									</View>
								</CardView>
							))
					}
				</ScrollView>
			</View>
		)
	}

	renderBestSellingProductList = () => {
		return (
			<View style={{
				height: 290,
				alignItems: 'flex-start',
				justifyContent: "flex-start",
				marginLeft: 7,
				marginRight: 7,
			}}>
				<View style={styles.headerlayer}>
					<Text style={{ fontSize: 15, color: "black", color: "black", fontFamily: constants.fonts.medium }}>Best Selling Products</Text>
					<TouchableOpacity style={{
						height: 30, width: 50,
						justifyContent: "center",
						alignItems: "center"
					}} onPress={() => {
						constants.products = this.state.best_selling_products;
						NavigationService.navigate("AllProducts", { title: "Best Selling Products", storeChanged: this.state.storeChanged })
					}}>
						<Text style={{ fontSize: 10, color: "black", color: "black", fontFamily: constants.fonts.medium }}>View All</Text>
					</TouchableOpacity>
				</View>
				<ScrollView horizontal={true}
					showsHorizontalScrollIndicator={false}>
					{
						this.state.best_selling_products.map((product, index) =>
							(
								<CardView cardElevation={5}
									cardMaxElevation={5}
									cornerRadius={5}
									style={styles.productBox} key={index}>
									<OffScreen offValue={product.off}></OffScreen>
									<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }}
										style={styles.productImage}></Image>
									<View style={styles.productQuantity}>
										<Text style={{ fontSize: 10, color: "black", fontFamily: constants.fonts.semiBold }}>{product.size}</Text>
									</View>
									<Text style={{
										fontSize: 13, textAlign: "center",
										color: "black", fontFamily: constants.fonts.medium,
										marginLeft: 3, marginRight: 3,
									}} numberOfLines={2}>{product.name}</Text>
									<View style={{
										flexDirection: "row", width: "100%", flexWrap: "wrap",
										justifyContent: "center", paddingLeft: 5, paddingRight: 5
									}}>
										{
											(product.price.cost_price && product.price.cost_price != product.price.sale_price) ?
												<Text style={{
													color: "black", textDecorationLine: "line-through",
													fontFamily: constants.fonts.semiBold,
													fontSize: 11
												}}>{product.price.cost_price} {constants.currency}</Text> : null
										}
										<Text style={{
											color: constants.green, marginLeft: 5,
											fontFamily: constants.fonts.semiBold,
											fontSize: 11
										}}>{product.price.sale_price} {constants.currency}</Text>
									</View>
									<View style={[styles.productCounter, {
										borderColor: product.count > 0 ? constants.green : "lightgrey"
									}]}>
										<TouchableOpacity style={[styles.counterButton, {
											backgroundColor: product.count > 0 ? constants.green : "transparent",
											borderColor: product.count > 0 ? constants.green : "lightgrey"
										}]} onPress={() => this.removeProduct(index, 2)}>
											<Text style={{
												fontSize: 25, marginTop: -4,
												color: product.count > 0 ? "white" : "black"
											}}>-</Text>
										</TouchableOpacity>
										<Text style={{ fontSize: 11, color: "black", fontFamily: constants.fonts.medium }}>
											{product.count > 0 ? product.count : "Add To Cart"}
										</Text>
										<TouchableOpacity style={[styles.counterButton, {
											backgroundColor: product.count > 0 ? constants.green : "transparent",
											borderColor: product.count > 0 ? constants.green : "lightgrey"
										}]} onPress={() => {
											if (product.count >= product.order_max) {
												Toast.showWithGravity("Your maximum limit has been reached for this product . . .", Toast.SHORT, Toast.CENTER);
											} else if (product.count >= product.stock_quantity) {
												Toast.showWithGravity("Product unavailable now . . .", Toast.SHORT, Toast.CENTER);
											} else this.checkStoreForAdd2Cart(index, 2)
										}}>
											<Text style={{
												fontSize: 20, marginTop: -4,
												color: product.count > 0 ? "white" : "black"
											}}>+</Text>
										</TouchableOpacity>
									</View>
								</CardView>
							))
					}
				</ScrollView>
			</View>
		)
	}

	renderCategories = () => {
		return (
			<View style={[styles.outerLayer, { marginTop: -5, alignItems: "flex-start" }]}>
				<View style={styles.headerlayer}>
					<Text style={{ fontSize: 15, color: "black", color: "black", fontFamily: constants.fonts.medium }}>Categories</Text>
					<TouchableOpacity style={{
						height: 30, width: 50,
						justifyContent: "center",
						alignItems: "center"
					}} onPress={() => NavigationService.navigate("Categories")}>
						<Text style={{ fontSize: 10, color: "black", color: "black", fontFamily: constants.fonts.medium }}>View All</Text>
					</TouchableOpacity>
				</View>
				<ScrollView horizontal={true}
					showsHorizontalScrollIndicator={false}>
					{
						this.state.categories.map((category, index) =>
							(
								<CardView cardElevation={5}
									cardMaxElevation={5}
									cornerRadius={10}
									style={{
										backgroundColor: "white",
										height: 170,
										width: 150,
										marginTop: 10,
										alignItems: "center",
										justifyContent: "space-between",
										marginLeft: 7,
										marginRight: 7
									}} key={index}>
									<TouchableOpacity style={{
										backgroundColor: "white",
										height: 170,
										width: "100%",
										borderRadius: 5
									}} onPress={() => {
										this.goToSubCategory(category);
										// NavigationService.navigate('HomeSubCategory', { data: category });
									}}>
										<Image source={{ uri: constants.imageBaseURL + category.picture }}
											style={{ width: "100%", height: 120, resizeMode: "contain" }}></Image>
										<View style={{
											height: 40, width: "100%",
											justifyContent: "center", alignItems: "center",
											paddingLeft: 3, paddingRight: 3,
										}}>
											<Text style={{
												fontSize: 14, color: "black", textAlign: "center",
												fontFamily: constants.fonts.medium
											}} numberOfLines={2}>{category.name}</Text>
										</View>
									</TouchableOpacity>
								</CardView>
							))
					}
				</ScrollView>
			</View>
		)
	}

	renderPageItems = () => {
		return (
			<View style={{
				flext: 1,
				justifyContent: "space-between",
				backgroundColor: '#f5f5f5',
				width: '100%'
			}}>
				{
					this.state.featured_products.length ?
						this.renderFeaturedProductList() : null
				}
				{
					this.state.best_selling_products.length ?
						this.renderBestSellingProductList() : null
				}
				{
					this.state.categories.length ?
						this.renderCategories() : null
				}
			</View>
		)
	}

	goToCart = () => {
		constants.previousStateForCart = this.props.navigation.state.routeName;
		NavigationService.navigate("Cart");
	}

	comingFromNextpage = () => {
		console.log("Came here from cart");
		let { best_selling_products, featured_products } = this.state;
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let bestProducts = best_selling_products;
		let featureProducts = featured_products;
		for (let j = 0; j < featureProducts.length; j++) {
			featureProducts[j].count = 0;
			for (let i = 0; i < cart.length; i++) {
				if (cart[i]._id == featureProducts[j]._id) {
					featureProducts[j].count = cart[i].count;
				}
			}
		}
		for (let j = 0; j < bestProducts.length; j++) {
			bestProducts[j].count = 0;
			for (let i = 0; i < cart.length; i++) {
				if (cart[i]._id == bestProducts[j]._id) {
					bestProducts[j].count = cart[i].count;
				}
			}
		}
		this.setState({
			featured_products: featureProducts,
			best_selling_products: bestProducts
		}, () => {
			this.calculateCartItem();
		})
	}

	goToSubCategory = (category) => {
		constants.category = category;
		constants.previousStateForSubCategory = this.props.navigation.state.routeName;
		NavigationService.navigate("HomeSubCategory", { storeChanged: this.state.storeChanged });
	}

	render() {
		const Screen =
		{
			heignt: Dimensions.get('window').height,
			width: Dimensions.get('window').width,
			scale: Dimensions.get('window').scale,
			fontScale: Dimensions.get('window').fontScale,
		};
		let animatBackgroundColor = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: ['transparent', '#1faf4b'],
			extrapolate: 'clamp'
		});
		let translateY = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: [0, -((Screen.heignt / 3) - (getStatusBarHeight(false) + 44))],
			extrapolate: 'clamp',
		});

		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<NavigationEvents onWillFocus={() => {
					const store = constants.store;
					AsyncStorage.getItem("cart").then((data) => {
						let cart = data ? JSON.parse(data) : [];
						let changedStore = false;
						for (let i = 0; i < cart.length; i++) {
							if (store._id != cart[i].store_id) {
								changedStore = true;
								break;
							}
						}
						this.setState({ storeChanged: changedStore }, () => {
							this.getStores();
							this.comingFromNextpage();
							console.log("store changed:--" + this.state.storeChanged);
						});
						// if (!changedStore) {
						// } else {
						// }
					});
					//Call whatever logic or dispatch redux actions and update the screen!
				}}
				/>
				< Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={
						Animated.event([
							{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
						])
					}
					contentContainerStyle={{ paddingTop: ((Screen.heignt / 3) - this.state.headerHeight) + 45 }}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderPageItems()}
				</Animated.ScrollView>

				<Animated.Image style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					zIndex: -1,
					height: Screen.heignt / 3.5,
					width: '100%',
					alignItems: 'center',
					justifyContent: 'flex-end',
					transform: [{ translateY }],
					backgroundColor: '#f5f5f5'
				}}
					resizeMode='stretch'
					source={Images.backgroundHeader}
				/>
				<Animated.View style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: Screen.heignt / 3.4,
					maxHeight: 230,
					backgroundColor: 'transparent',
					alignItems: 'center',
					justifyContent: 'center',
					transform: [{ translateY }],
					paddingTop: this.state.headerHeight + 45,
				}}>
					<View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 50, marginTop: 30 }}>
						<CardView cardElevation={2}
							cardMaxElevation={2}
							cornerRadius={10}
							style={{
								width: 180,
								height: 85,
								marginBottom: 10,
								justifyContent: "center",
								alignItems: "center"
							}}>
							<Image style={{
								width: 175, height: 80,
								resizeMode: "stretch",
								borderRadius: 7,
							}} source={{ uri: constants.imageBaseURL + this.state.storeData.picture }} />
						</CardView>
						<View style={{ height: 50, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
							<View style={{ height: 50 }}>
								<TouchableOpacity style={{
									flexDirection: "row",
									height: "100%",
									width: "100%",
									alignItems: 'center',
								}} onPress={() => {
									NavigationService.navigate("Store");
								}}>
									<Text style={{
										color: "black", fontFamily: constants.fonts.medium,
										fontSize: 18,
									}}>{this.state.storeData.name}</Text>
									<Image source={Images.downArrow} style={{
										marginLeft: 10,
										width: 20, height: 20,
										marginTop: 3
									}}></Image>
								</TouchableOpacity>
							</View>
						</View>

					</View>
				</Animated.View>

				<Animated.View style={{
					top: 0, left: 0, right: 0,
					position: 'absolute', flexDirection: 'row',
					backgroundColor: animatBackgroundColor,
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: { height: (getStatusBarHeight(true) + 30), }
					})
				}}>
					<Animated.View style={{
						height: '100%', width: "10%",
						paddingLeft: 15, justifyContent: 'center',
						alignItems: 'center',
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<TouchableHighlight style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 40 }, android: { marginTop: 10 } }) }} underlayColor='transparent' onPress={() => {
							NavigationService.navigate("SelectLocationInHome");
						}}>
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.locationwhite} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%', marginLeft: 5,
						width: "65%",
					}}>
						<TouchableOpacity style={{
							width: "100%",
							height: "100%",
							justifyContent: 'center',
						}} onPress={() => NavigationService.navigate("SelectLocationInHome")}>
							<Text style={{
								fontSize: 16, color: 'white',
								fontFamily: constants.fonts.semiBold, textTransform: "capitalize",
								...Platform.select({ ios: { marginTop: 30 }, android: {} })
							}}>{this.state.area.name + ", " + this.state.city.name}</Text>
						</TouchableOpacity>
					</Animated.View>
					<Animated.View style={{
						height: '100%', width: '25%',
						justifyContent: 'center',
						alignItems: 'center', flexDirection: 'row'
					}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							...Platform.select({
								ios: { marginTop: 40 },
								android: { marginTop: 10 }
							})
						}} underlayColor='transparent' onPress={() => {
							NavigationService.navigate("Search");
						}}>
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.search} />
						</TouchableHighlight>
						<TouchableOpacity style={{
							height: 35,
							width: 35,
							...Platform.select({
								ios: { marginTop: 40 },
								android: { marginTop: 10 }
							})
						}} underlayColor='transparent' onPress={() => {
							{
								this.state.totalCartItem ?
									this.changedStoreCheck(1) :
									Toast.showWithGravity("Please add few items in cart", Toast.SHORT, Toast.CENTER);
							}
						}}>
							{
								this.state.totalCartItem ?
									<View style={{
										backgroundColor: "#e9001f",
										width: "auto",
										height: "auto",
										paddingTop: this.state.storeChanged ? 5 : 2,
										paddingBottom: this.state.storeChanged ? 4 : 3,
										paddingLeft: 7,
										paddingRight: 8,
										position: "absolute",
										zIndex: 999,
										top: -12,
										left: -8,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: 15
									}}>
										{
											this.state.storeChanged ?
												<Image source={Images.closeImg} style={{ height: 10, width: 10 }} /> :
												<Text style={{
													color: "white",
													fontFamily: constants.fonts.bold, fontSize: 10
												}}>{this.state.totalCartItem}</Text>
										}
									</View> : null
							}
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
						</TouchableOpacity>
					</Animated.View>
				</Animated.View>
				<StatusBar backgroundColor={constants.green} barStyle="light-content" />
			</View >
		)
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)