import { StyleSheet } from 'react-native';
export default StyleSheet.create({
	counterImage: { height: 10, width: 10 },
	productImage: { height: 100, width: "97%", resizeMode: 'contain', margin: 3 },
	outerLayer: {
		height: 230,
		alignItems: 'center',
		justifyContent: "flex-start",
		shadowColor: 'dimgrey',
		marginLeft: 7,
		marginRight: 7,
		marginTop: 10,
	},
	headerlayer: {
		height: 30,
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingLeft: 7,
		paddingRight: 7
	},
	productBox: {
		marginTop: 10,
		backgroundColor: "white",
		height: 235,
		width: 150,
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: 7,
		marginRight: 7
	},
	productQuantity: {
		height: 25,
		minWidth: 40,
		borderRadius: 20,
		paddingLeft: 10,
		paddingRight: 10,
		backgroundColor: "#d0efdd",
		justifyContent: "center",
		alignItems: "center"
	},
	productCounter: {
		borderWidth: 1,
		height: 30,
		alignItems: "center",
		width: "90%",
		justifyContent: "space-between",
		borderRadius: 30,
		flexDirection: "row",
		marginBottom: 10,
		borderLeftWidth: 0,
		borderRightWidth: 0,
	},
	counterButton: {
		height: 30, width: 30,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1,
		borderRadius: 30,
	}
});