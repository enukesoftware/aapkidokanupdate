import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    bgImage:{ 
      width: "100%", 
      height: "100%", 
      position: "absolute" 
    },
    outerlayer:{ 
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center' 
    },
    innerImage:{ 
      width: 100, height: 100, 
      borderRadius: 50, 
      zIndex: 1, marginTop: -510 
    },
    innerlabel:{ 
      marginTop: 60, fontSize: 18, 
      color: '#212121', color:"black", fontFamily: constants.fonts.bold 
    },
    innerSection:{ 
      height: '60%', width: '90%', backgroundColor: 'white', 
      alignItems: 'center', shadowOpacity: 1, shadowRadius: 3, 
      shadowOffset: { height: 0, width: 0 }, 
      elevation: 1, position: 'absolute', zIndex: 0, borderRadius: 10 
    },
    input:{ marginLeft: 10, width: 180 },
    inputLogo:{ 
      width: 20, 
      height: 20, 
      resizeMode: 'contain', 
      marginTop: 14, 
      marginLeft: 15 
    },
    inputSection:{ 
      borderWidth: 1, 
      borderColor: 'lightgrey', 
      borderRadius: 25, width: '80%', 
      height: 50, marginTop: 25, 
      flexDirection: "row" 
    },
    forgotLine:{
      width:"auto",height:30,
      alignContent:"flex-end",
      alignItems:"center",
      flexDirection:"row",
      color: '#212121'
    },
    loginText:{ 
      fontSize: 20, 
      color: 'white', 
      color:"black", fontFamily: constants.fonts.bold 
    },
    loginButton:{ 
      borderWidth: 1, 
      width: '80%', height: 50, 
      alignItems: 'center', 
      justifyContent: 'center', 
      marginTop: 5, borderRadius: 25 
    },
    socialSection:{
      width:"auto",
      height:70,
      alignContent:"center",
      alignItems:"center",
      flexDirection:"row",
      color: '#212121'
    },
    socialButton:{
      height:50,
      width:50,
      alignItems:"center",
      justifyContent:"center",
      borderWidth:1,
      borderRadius:50,
      marginRight:5
    },
    signUpBar:{
      width:"80%",
      height:40,
      alignContent:"flex-end",
      alignItems:"center",
      flexDirection:"row",
      color: '#212121'
  },
  signUpLline:{
    height:1,
    backgroundColor:"lightgrey",
    flex:1
  }
});