import React, { Component } from 'react';
import { View, KeyboardAvoidingView, Alert, Text, Image, Dimensions, TouchableOpacity, StyleSheet, Animated, TouchableHighlight, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import AsyncStorage from '@react-native-community/async-storage';
import CardView from 'react-native-cardview';
import constants from '../../constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from "moment";
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';

export default class SelectAddress extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	state = {
		loading: false,
		selectedIndex: -1,
		scrollY: new Animated.Value(0.01),
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		addressList: [],
		slots: [],
		selectedDateIndex: 0,
		selectedTimeIndex: -1,
		deliveryTimes: [],
		daysHeight: 100,
		totalCartItem: 0,
		store: constants.cartStore,
		timeDiff: 0
	};

	componentDidMount = () => {
		constants.selectedSlot = {};
		this.getAddressList();
	}

	getAddressList = () => {
		this.setState({ loading: true });
		ApiManager.getAddressList().then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					if (constants.selectAddress && res.data.address) {
						for (let i = 0; i < res.data.address.length; i++) {
							if (res.data.address[i]._id == constants.selectAddress._id) {
								this.setState({ selectedIndex: i });
								break;
							}
						}
					}
					this.setState({ addressList: res.data.address ? res.data.address : [] }, () => {
						console.log(this.state.addressList);
						this.getSlots();
					});
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			});
		});
	}

	calculateCartItem = () => {
		AsyncStorage.getItem("cart").then((data) => {
			let cart = data ? JSON.parse(data) : [];
			let counter = 0;
			for (let i = 0; i < cart.length; i++) {
				counter = counter + cart[i].count;
			}
			this.setState({ totalCartItem: counter }, () => {
				console.log("total items" + this.state.totalCartItem);
			});
		})
	}

	getSlots = async () => {
		let store = await AsyncStorage.getItem("cartStore");
		store = JSON.parse(store);
		// console.log(JSON.stringify(store));
		if (!this.state.loading) {
			this.setState({ loading: true });
		}
		ApiManager.getSlots(store._id).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					let now = moment(new Date());
					let end = moment(moment().format('YYYY-MM-DD') + ' ' + res.data.store.timings.close_time, 'YYYY-MM-DD HH:mm');
					var duration = end.diff(now, "hours");
					this.setState({ timeDiff: duration });
					console.log("end time " + duration);
					if (res.data.slots.length) {
						let todayIndex = -1;
						for (let i = 0; i < res.data.slots.length; i++) {
							if (moment(Date.now()).format('dddd') === moment(res.data.slots[i].date).format('dddd')) {
								todayIndex = i;
								break;
							}
						}
						if (this.state.timeDiff >= 2 && res.data.store.has_express_delivery) {
							constants.selectedSlot = { name: "Within 2 Hours" };
							// this.setState({ selectedTimeIndex: 0 })
							if (todayIndex >= 0) {
								res.data.slots[todayIndex].slots.unshift(constants.selectedSlot);
							} else {
								res.data.slots.unshift({
									date: moment(new Date()),
									slots: [constants.selectedSlot]
								});
							}
						}
						this.setState({
							slots: res.data.slots,
							deliveryTimes: res.data.slots[this.state.selectedDateIndex].slots
						}, () => {
							if (constants.selectedSlot._id) {
								for (let i = 0; i < this.state.deliveryTimes.length; i++) {
									if (this.state.deliveryTimes[i]._id == constants.selectedSlot._id) {
										this.setState({ selectedTimeIndex: i }, () => {
											console.log("slot fetched");
										})
									}
								}
							}
							console.log(this.state.slots);
						})
					} else {
						Toast.showWithGravity("Slots not available for this store", Toast.SHORT, Toast.BOTTOM);
					}
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			})
		})
	}

	renderAddressPage = () => {
		return (
			<KeyboardAvoidingView
				style={{
					...Platform.select({
						ios: { marginTop: (getStatusBarHeight(true) + 30), },
						android: { marginTop: 38 }
					}),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-start',
					backgroundColor: constants.backgroundPageColor,
					marginLeft: 15,
					marginRight: 15
				}}>
				< CardView cardElevation={5}
					cardMaxElevation={5}
					cornerRadius={5}
					style={{
						height: "auto",
						width: "100%",
						backgroundColor: "white",
						borderBottomWidth: 3,
						borderColor: constants.backgroundPageColor,
						marginBottom: 25,
						marginTop: 5,
						height: "auto",
						justifyContent: "flex-start",
						alignItems: "center"
					}}>
					<View style={{
						width: "100%",
						height: 40,
						backgroundColor: constants.lightgreen,
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between",
						paddingLeft: 5,
						// paddingRight: 5,
					}}>
						<Text style={{
							color: "black",
							fontSize: 14, color: "black", fontFamily: constants.fonts.semiBold,
							marginLeft: 10,
						}}>Saved Address</Text>
						<TouchableOpacity style={{
							borderWidth: 1,
							borderColor: constants.green,
							borderRadius: 20,
							height: 30,
							justifyContent: "center",
							paddingLeft: 10,
							paddingRight: 10,
							marginRight: 10
						}} onPress={() => {
							if (this.props.navigation.state.routeName == "SelectAddressInHome") {
								NavigationService.navigate("AddAddressFormInHome", { backStateName: this.props.navigation.state.routeName })
							} else {
								NavigationService.navigate("AddressForm", { backStateName: this.props.navigation.state.routeName })
							}
						}}>
							<Text style={{
								color: "black",
								fontSize: 14, color: "black", fontFamily: constants.fonts.medium,
							}}>Add Address</Text>
						</TouchableOpacity>
					</View>
					{
						this.state.addressList.map((address, index) => (
							<TouchableOpacity key={index} onPress={() => {
								AsyncStorage.setItem("selectAddress", JSON.stringify(this.state.addressList[index])).then(() => {
									this.setState({ selectedIndex: index }, () => {
										constants.selectAddress = this.state.addressList[index];
										console.log(constants.selectAddress);
										Toast.showWithGravity(
											'Address has been saved . . .',
											Toast.LONG,
											Toast.CENTER //TOP,BOTTOM
										);
									});
								})
							}} style={{
								width: "90%",
								borderTopWidth: index != 0 ? 1 : 0,
								borderTopColor: index != 0 ? "lightgray" : "transparent"
							}}>
								<View style={{
									width: "100%",
									minHeight: 70,
									flexDirection: "row"
								}}>
									<View style={{
										width: "10%",
										minHeight: 90,
										justifyContent: "center",
										alignItems: "flex-start"
									}}>
										<TouchableOpacity style={{
											width: 40,
											height: 40,
											justifyContent: "center",
											alignItems: "flex-start",
										}} onPress={() => {
											AsyncStorage.setItem("selectAddress", JSON.stringify(this.state.addressList[index])).then(() => {
												this.setState({ selectedIndex: index }, async () => {
													constants.selectAddress = this.state.addressList[index];
													console.log(constants.selectAddress);
													Toast.showWithGravity(
														'Address has been saved . . .',
														Toast.LONG,
														Toast.CENTER //TOP,BOTTOM
													);
												});
											})
										}}>
											<View style={{
												width: 20,
												height: 20,
												borderColor: this.state.selectedIndex == index ? constants.green : "lightgrey",
												borderWidth: this.state.selectedIndex == index ? 5 : 2,
												borderRadius: 15
											}}></View>
										</TouchableOpacity>
									</View>
									<View style={{
										width: "90%",
										minHeight: 70,
										justifyContent: "center",
										paddingLeft: 10,
										paddingBottom: 10,
										paddingTop: 10
									}}>
										<Text style={{
											color: "black", fontFamily: constants.fonts.semiBold,
											fontSize: 18, color: "black"
										}}>{address.full_name}</Text>
										<Text style={{
											fontSize: 14,
											color: "black",
											marginTop: 10,
											color: "black", fontFamily: constants.fonts.medium,
										}}>
											{address.house_no + ', ' + address.city.name + ', ' + address.locality + (address.landmark ? (', ' + address.landmark) : '')}
										</Text>
									</View>
								</View>
							</TouchableOpacity>
						))
					}
				</ CardView>
				< CardView cardElevation={5}
					cardMaxElevation={5}
					cornerRadius={5}
					style={{
						height: "auto",
						width: "100%",
						backgroundColor: "white",
						borderColor: constants.backgroundPageColor,
						marginBottom: 25,
						height: "auto",
						justifyContent: "flex-start",
						alignItems: "center"
					}}>
					<View style={{
						width: "100%",
						height: 40,
						backgroundColor: constants.lightgreen,
						justifyContent: "center"
					}}>
						<Text style={{
							color: "black",
							fontSize: 14, color: "black", fontFamily: constants.fonts.semiBold,
							marginLeft: 10,
						}}>Delivery Date And Time</Text>
					</View>
					<View style={{
						width: "100%",
						height: "auto",
						flexDirection: "row",
					}}>
						<View style={{
							width: "30%",
							height: "auto",
							// justifyContent: "center"
						}}>
							{
								this.state.slots.map((slot, index) => (
									<TouchableOpacity key={index} style={{
										width: "100%",
										height: 40,
										backgroundColor: this.state.selectedDateIndex == index ? constants.green : constants.lightgreen,
										justifyContent: "center",
										borderTopWidth: index != 0 ? 2 : 0,
										borderTopColor: "white",
										alignItems: "flex-start",
										paddingLeft: 7,
										paddingRight: 7
									}} onPress={() => {
										this.setState({ selectedDateIndex: index, deliveryTimes: this.state.slots[index].slots, selectedTimeIndex: -1 }, () => {
											if (constants.selectedSlot._id) {
												for (let i = 0; i < this.state.deliveryTimes.length; i++) {
													if (this.state.deliveryTimes[i]._id == constants.selectedSlot._id) {
														this.setState({ selectedTimeIndex: i }, () => {
															console.log("slot fetched");
														})
													}
												}
											}
											if (this.state.slots[index].is_express_delivery && constants.selectedSlot.name && constants.selectedSlot.name == "Within 2 Hours") {
												this.setState({ selectedTimeIndex: index }, () => {
													console.log("slot set to express delivery");
												})
											}
										})
									}}>
										<Text style={{
											color: this.state.selectedDateIndex == index ? "white" : "black",
											fontSize: 14,
											fontFamily: constants.fonts.semiBold,
											// marginLeft: 15
										}}>{
												moment(Date.now()).format('dddd') === moment(slot.date).format('dddd') ? "Today" :
													moment(slot.date).format('dddd')
											}</Text>
										{
											this.state.selectedDateIndex == index ?
												<View style={styles.triangle}></View> : null
										}
									</TouchableOpacity>
								))
							}
						</View>
						<View style={{
							width: "70%",
							justifyContent: "center",
							// backgroundColor: "green",
						}}>
							<View style={{
								width: "100%",
								flexDirection: "row",
								flexWrap: "wrap",
								marginRight: "1.5%",
							}}>
								{
									this.state.deliveryTimes.map((time, index) => (
										<TouchableOpacity key={index} style={{
											width: "43%",
											height: "auto",
											marginLeft: "1.5%",
											marginRight: "1.5%",
											flexDirection: "row",
											alignItems: "center",
											marginTop: 5
										}} onPress={() => {
											AsyncStorage.setItem("selectedSlot", JSON.stringify(this.state.deliveryTimes[index])).then(() => {
												this.setState({ selectedTimeIndex: index }, () => {
													constants.selectedSlot = this.state.deliveryTimes[index];
												});
											})
										}}>
											<View style={{
												width: 30,
												height: 30,
												justifyContent: "center",
												alignItems: "center",
											}} >
												<View style={{
													width: 15,
													height: 15,
													borderWidth: this.state.selectedTimeIndex == index ? 5 : 2,
													borderColor: this.state.selectedTimeIndex == index ? constants.green : "lightgrey",
													borderRadius: 15
												}}></View>
											</View>
											<Text numberOfLines={2} style={{
												color: "black",
												fontSize: 13,
												flexDirection: 'column',
												fontFamily: constants.fonts.medium,
												flex: 1
											}}>{
													time.name ? time.name :
														moment(time.start_time).format('ha') + " to " + moment(time.end_time).format('ha')
												}</Text>
										</TouchableOpacity>
									))
								}
							</View>
						</View>
					</View>
				</CardView>
				<View style={{
					width: "100%",
					height: 70,
					flexDirection: "row",
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 30
				}}>
					<TouchableOpacity style={{
						backgroundColor: constants.green,
						height: 40,
						width: 100,
						borderRadius: 30,
						justifyContent: "center",
						alignItems: "center"
					}} onPress={() => {
						if (this.state.selectedIndex < 0) {
							Alert.alert("Please select address for delivery . . .")
						} else if (this.state.selectedTimeIndex < 0) {
							Alert.alert("Please select time slot for delivery . . .")
						} else {
							let slot = this.state.deliveryTimes[this.state.selectedTimeIndex];
							if (this.props.navigation.state.routeName == "SelectAddressInHome") {
								NavigationService.navigate("PaymentInHome", {
									slot_id: slot.name ? slot.name : slot._id
								});
							} else if (this.props.navigation.state.routeName == "SelectAddress") {
								NavigationService.navigate("PaymentInCategory", {
									slot_id: slot.name ? slot.name : slot._id
								});
							} else if (this.props.navigation.state.routeName == "SelectAddressInSearch") {
								NavigationService.navigate("PaymentInSearch", {
									slot_id: slot.name ? slot.name : slot._id
								});
							} else {
								NavigationService.navigate("PaymentInStore", {
									slot_id: slot.name ? slot.name : slot._id
								});
							}
						}
					}}>
						<Text style={{
							color: "white",
							fontSize: 16,
							fontFamily: constants.fonts.bold
						}}>CONFIRM</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						backgroundColor: "transparent",
						height: 40,
						width: 100,
						borderRadius: 30,
						justifyContent: "center",
						alignItems: "center",
						marginLeft: 25,
						borderWidth: 1,
						borderColor: "lightgray"
					}} onPress={() => {
						NavigationService.navigate(constants.previousStateForSelectAddress);
						// NavigationService.goBack()
					}}>
						<Text style={{
							color: "black",
							fontSize: 16,
							color: "black", fontFamily: constants.fonts.medium,
						}}>CANCEL</Text>
					</TouchableOpacity>
				</View>
			</KeyboardAvoidingView>
		)
	}

	render() {
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<NavigationEvents onWillFocus={() => {
					constants.selectedSlot = {};
					this.getAddressList();
					this.calculateCartItem();
					//Call whatever logic or dispatch redux actions and update the screen!
				}}
				/>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderAddressPage()}
				</Animated.ScrollView>
				<Animated.View style={{
					top: 0, left: 0, right: 0,
					position: 'absolute', flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: { height: (getStatusBarHeight(true) + 40), }
					}),
					backgroundColor: '#1faf4b'
				}}>
					<Animated.View style={{
						height: '100%', width: 60,
						paddingLeft: 10, justifyContent: 'center',
						alignItems: 'center', flexDirection: 'row'
					}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							...Platform.select({
								ios: { marginTop: 45 },
								android: { marginTop: 14 }
							})
						}} underlayColor='transparent'
							onPress={() => {
								NavigationService.navigate(constants.previousStateForSelectAddress);
								// NavigationService.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						paddingLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: this.state.screenWidth - 120,
					}}>
						<Text style={{
							fontSize: 18,
							color: 'white',
							fontFamily: constants.fonts.bold,
						}}>Select Address</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						width: 60,
						justifyContent: 'flex-end',
						alignItems: 'center',
						flexDirection: 'row'
					}}>
						<TouchableOpacity style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 40 }, android: { marginTop: 10 } }) }} underlayColor='transparent'
							onPress={() => {
								{
									this.state.totalCartItem ?
										NavigationService.navigate(constants.previousStateForSelectAddress) :
										Alert.alert("Please add few items in cart");
								}
							}}>
							{
								this.state.totalCartItem ?
									<View style={{
										backgroundColor: "#e9001f",
										width: "auto",
										height: "auto",
										paddingTop: 2,
										paddingBottom: 3,
										paddingLeft: 7,
										paddingRight: 8,
										position: "absolute",
										zIndex: 999,
										top: -12,
										left: -8,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: 15
									}}>
										<Text style={{
											color: "white",
											fontFamily: constants.fonts.bold, fontSize: 10
										}}>{this.state.totalCartItem}</Text>
									</View> : null
							}
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
						</TouchableOpacity>
					</Animated.View>
				</Animated.View>
			</View >
		)
	}
}

const styles = StyleSheet.create({
	triangle: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 7,
		borderRightWidth: 7,
		borderBottomWidth: 12,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: constants.green,
		transform: [
			{ rotate: '90deg' }
		],
		position: "absolute",
		right: -10,
		top: 12
	},
	productCounter: {
		borderColor: constants.green,
		borderWidth: 1,
		height: 20,
		alignItems: "center",
		width: "100%",
		minWidth: 50,
		justifyContent: "space-between",
		borderRadius: 30,
		flexDirection: "row",
		borderLeftWidth: 0,
		borderRightWidth: 0,
	},
	counterButton: {
		height: 20, width: 20,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1,
		backgroundColor: constants.green,
		borderColor: constants.green,
		borderRadius: 30
	}
})