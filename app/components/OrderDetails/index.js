import React, { Component } from 'react';
import { View, TouchableOpacity, KeyboardAvoidingView, Alert, Text, Image, Dimensions, Animated, TouchableHighlight, ScrollView, Platform, DeviceEventEmitter } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import AsyncStorage from '@react-native-community/async-storage';
import Images from '../../global/images';
import CardView from 'react-native-cardview';
import constants from '../../constants/constants';
import moment from "moment";
import styles from './styles';
import NavigationService from '../../global/NavigationService';
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';

class OrderDetailsScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.orderNotificationHandler = this.orderNotificationHandler.bind(this);
	}

	state = {
		selectedIndex: 0,
		scrollY: new Animated.Value(0.01),
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		products: [],
		loading: false,
		totalProducts: 0,
		logined: false,
		order: {},
		order_id: ""
	};

	cancelAlert = () => {
		Alert.alert(
			'Are you sure',
			'you want to cancel your order?',
			[
				{
					text: 'NO',
					onPress: () => console.log('Cancel Pressed'),
					style: 'cancel',
				},
				{
					text: 'YES',
					onPress: () => this.cancelOrder()
				}
			],
			{ cancelable: true },
		);
	}

	cancelOrder = () => {
		this.setState({ loading: true });
		ApiManager.cancelOrder(this.state.order._id).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					NavigationService.goBack();
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			});
		});
	}

	getInvoice = () => {
		this.setState({ loading: true });
		ApiManager.getInvoice(this.state.order._id).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && (res.success || res.status) && res.data && res.data.mail && res.data.mail.data) {
					Toast.showWithGravity("Invoice sent on your mail", Toast.SHORT, Toast.BOTTOM);
					// NavigationService.goBack();
				} else {
					Toast.showWithGravity("Invoice not sent on your mail", Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			});
		});
	}

	checkAvailability = () => {
		let { order } = this.state;
		let products = JSON.parse(JSON.stringify(this.state.products))
		let data = { store_id: order.store_id, products: [] };
		for (let i = 0; i < products.length; i++) {
			data.products.push({
				count: products[i].count,
				_id: products[i]._id
			})
		}
		this.setState({ loading: true });
		ApiManager.checkProductsAvailability(data).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(async () => {
				if (res && res.success && res.code == 200) {
					if (res.data.outOfStockProducts && res.data.outOfStockProducts.length) {
						Toast.showWithGravity(res.data.message, Toast.LONG, Toast.CENTER);
						let newProducts = res.data.outOfStockProducts;
						for (let i = 0; i < newProducts.length; i++) {
							for (let j = 0; j < products.length; j++) {
								if (products[j]._id == newProducts[i]._id) {
									products[j].outOfStock = true;
								}
							}
						}
						this.setState({ products }, () => {
							this.reOrder();
						})
					} else this.reOrder();
				} else {
					Toast.showWithGravity(res.singleStringMessage ? res.singleStringMessage : "Undefined error", Toast.LONG, Toast.CENTER);
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
		})
	}

	reOrder = async () => {
		await AsyncStorage.removeItem("cart");
		this.props.setCart(this.state.products);
		constants.previousStateForCart = "Home";
		NavigationService.navigate("Cart", { reorder: true });
	}

	componentWillMount = () => {
		let params = this.props.navigation.state.params;
		// this.setState({ order: params.data, products: params.data.products }, () => {
		// 	// console.log(JSON.stringify(this.state.order));
		// })
		this.setState({
			order_id: params.order_id
		}, () => {
			console.log("order_id received:--" + this.state.order_id);
			this.getOrderDetails();
		})
	}

	componentDidMount = () => {
		DeviceEventEmitter.addListener("ORDER_DETAIL_NOTIFICATION", this.orderNotificationHandler)
	}

	componentWillUnmount = () => {
		DeviceEventEmitter.removeListener("ORDER_DETAIL_NOTIFICATION", this.orderNotificationHandler)
	}

	orderNotificationHandler(data) {
		console.log("EVENT")
		if (data && data.orderId && data.orderId == this.state.order_id) {
			this.getOrderDetails();
		}
	}

	getOrderDetails = () => {
		this.setState({ loading: true });
		ApiManager.getOrderDetails(this.state.order_id).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && (res.success || res.status) && res.data && res.data.order && res.data.order._id) {
					this.setState({
						order: res.data.order,
						products: res.data.order.products
					}, () => console.log("data has been set"))
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false }, () => {
				console.log(err);
			});
		});
	}

	renderDetailsPage = () => {
		let { order } = this.state;
		return (
			<KeyboardAvoidingView
				style={{
					...Platform.select({
						ios: { marginTop: (getStatusBarHeight(true) + 30), },
						android: { marginTop: 38 }
					}),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-start',
					backgroundColor: constants.backgroundPageColor,
					marginLeft: 15,
					marginRight: 15,
				}}>
				{
					order._id ?
						this.renderOrder() :
						<View style={{
							flex: 1, justifyContent: "center", alignItems: "center"
						}}>
							<Text>Loading . . .</Text>
						</View>
				}
			</KeyboardAvoidingView>
		)
	}

	renderOrder = () => {
		let { order, products } = this.state;
		return (
			<ScrollView style={{ width: "100%" }}
				contentContainerStyle={{ justifyContent: "center", marginBottom: 50 }}>
				<CardView cardElevation={5}
					cardMaxElevation={5} cornerRadius={5} style={styles.productBox} >
					<View style={{
						height: 30, width: '100%',
						backgroundColor: Constants.lightgreen,
						borderTopRightRadius: 6, borderTopLeftRadius: 6,
						flexDirection: 'row', justifyContent: "space-between",
						paddingLeft: 10, paddingRight: 10
					}}>
						<View style={{
							height: '100%', alignItems: 'center',
							width: '50%', flexDirection: 'row',
							justifyContent: 'flex-start'
						}}>
							<Text style={{
								fontSize: 10, color: "black",
								fontFamily: constants.fonts.medium
							}}>Placed On </Text>
							<Text style={{
								fontSize: 10, color: "black",
								fontFamily: constants.fonts.bold
							}}>{moment(order.created_at).format('D MMM, h:mm a')}</Text>
						</View>
						<View style={{
							height: '100%', alignItems: 'center',
							width: '50%', flexDirection: 'row',
							justifyContent: "flex-end"
						}}>
							<Text style={{
								fontSize: 10, color: "black",
								fontFamily: constants.fonts.medium
							}}>Schedule On </Text>
							<Text style={{
								fontSize: 10, color: "black",
								fontFamily: constants.fonts.bold
							}}>{moment(order.deliver_end_time).format('D MMM, h:mm a')}</Text>
						</View>
					</View>
					<View style={{ height: '55%', width: '100%', flexDirection: 'row' }}>
						<View style={{ height: '100%', width: '78%', flex: 1, flexDirection: 'row' }}>
							<View style={{ width: 60, height: 60, borderRadius: 30, backgroundColor: 'lightgrey', borderColor: 'red', justifyContent: 'center', alignItems: 'center', marginTop: 10, marginLeft: 10 }}>
								<Image style={{ width: 58, height: 58, borderRadius: 29, }} resizeMode="cover" source={{ uri: constants.imageBaseURL + order.store.picture }} />
							</View>
							<View style={{ flexDirection: 'column', marginTop: 20, marginLeft: 10 }}>
								<Text style={{ color: "black", fontFamily: constants.fonts.bold, color: 'black' }}>{this.state.order.store.name}</Text>
								<Text style={{ marginTop: 6, color: 'black', fontSize: 13, fontFamily: constants.fonts.medium, }}>Order Id :- <Text style={{ textTransform: "uppercase" }}>#{order.order_id ? order.order_id : "N/A"}</Text></Text>
							</View>
						</View>
						<View style={{ height: '100%', width: '22%', alignItems: 'flex-end' }}>
							<View style={{ paddingLeft: "10%", backgroundColor: Constants.lightgreen, borderBottomLeftRadius: 20, borderTopLeftRadius: 20, width: "100%", maxWidth: 120, height: "auto", minHeight: 30, marginTop: 20, paddingBottom: 5, paddingTop: 5, justifyContent: 'center', alignItems: 'center' }}>
								<Text style={{ color: "black", fontFamily: constants.fonts.semiBold, }}>{(order.total_amount_after_tax + order.delivery_charges) - order.discount} {constants.currency}</Text>
							</View>
						</View>
					</View>
					<View style={{ height: '20%', width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
						<View style={{
							flexDirection: "row",
							width: "auto"
						}}>
							<Text style={{ fontSize: 10, marginLeft: 10, color: "black", fontFamily: constants.fonts.medium }}>Order Status-:</Text>
							<Text style={{
								fontFamily: constants.fonts.bold, fontSize: 10,
								color: 'black', marginLeft: 3, textTransform: "uppercase"
							}}>{order.orderStatus}</Text>
						</View>
						<View style={{
							flexDirection: "row",
							width: "auto",
							justifyContent: "flex-end"
						}}>
							<Text style={{ fontSize: 10, color: "black", fontFamily: constants.fonts.medium, marginTop: 6, marginRight: 3 }}>Mode Of Payment-: </Text>
							<Text style={{ fontSize: 10, color: "black", fontFamily: constants.fonts.bold, marginTop: 6, marginRight: 5, fontWeight: "600" }}>{order.payment_type == 1 ? "COD" : "CREDIT CARD"}</Text>
						</View>
					</View>
				</CardView>
				< CardView cardElevation={5}
					cardMaxElevation={5}
					cornerRadius={5}
					style={{
						height: "auto",
						width: "100%",
						backgroundColor: "white",
						borderBottomWidth: 3,
						borderColor: constants.backgroundPageColor,
						marginBottom: 25,
						marginTop: 5,
						height: "auto",
						justifyContent: "flex-start",
						alignItems: "center"
					}}>
					<View style={{
						width: "100%",
						height: 40,
						backgroundColor: constants.lightgreen,
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between",
						paddingLeft: 5,
						paddingRight: 5,
					}}>
						<View style={{ width: "45%" }}>
							<Text style={{
								color: "black", marginLeft: 5,
								fontSize: 12, color: "black", fontFamily: constants.fonts.semiBold
							}}>ITEM</Text>
						</View>
						{/* <View style={{ width: "20%", alignItems: "center" }}>
								<Text style={{
									color: "black", textAlign: "center",
									fontSize: 12, fontFamily: constants.fonts.semiBold
								}}>UNIT PRICE</Text>
							</View> */}
						<View style={{ width: "20%", alignItems: "center" }}>
							<Text style={{
								color: "black",
								fontSize: 12, color: "black", fontFamily: constants.fonts.semiBold
							}}>SIZE</Text>
						</View>
						<View style={{ width: "10%", alignItems: "center" }}>
							<Text numberOfLines={1} style={{
								color: "black",
								fontSize: 12, color: "black", fontFamily: constants.fonts.semiBold,
								textAlign: "left"
							}}>QTY</Text>
						</View>
						<View style={{ width: "25%" }}>
							<Text style={{
								color: "black",
								fontSize: 12, color: "black", fontFamily: constants.fonts.semiBold,
								textAlign: "right"
							}}>TOTAL PRICE</Text>
						</View>
					</View>
					{
						products.map((product, index) => (
							<View style={{
								width: "100%",
								minHeight: 60,
								flexDirection: "row",
								alignItems: "center",
								justifyContent: "space-between",
								paddingLeft: 5,
								paddingRight: 5,
								borderTopWidth: index != 0 ? 3 : 0,
								borderTopColor: index != 0 ? constants.backgroundPageColor : "transparent"
							}} key={index}>
								<View style={{ width: "45%", flexDirection: "row", maxHeight: 70 }}>
									<View style={{ width: "40%", padding: 5, }}>
										<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }} style={{ height: 60, width: "100%", resizeMode: "contain" }}></Image>
									</View>
									<View style={{
										width: "60%",
										justifyContent: "center",
										paddingLeft: 2,
										paddingRight: 2,
										flexShrink: 1
									}}>
										<Text numberOfLines={1} style={{
											fontSize: 12,
											color: "black", fontFamily: constants.fonts.medium
										}}>{product.name}</Text>
										<Text numberOfLines={1} style={{
											fontSize: 12,
											color: "black", fontFamily: constants.fonts.medium, color: "black",
											marginTop: 5
										}}>{product.price} {constants.currency}/Unit</Text>
									</View>
								</View>
								{/* <View style={{ width: "20%", alignItems: "center" }}>
										<Text style={{
											color: "black",
											fontSize: 13, color: "black", fontFamily: constants.fonts.medium,
											textAlign: "center"
										}}>{product.price}</Text>
									</View> */}
								<View style={{ width: "20%", alignItems: "center" }}>
									<Text style={{
										color: "black",
										fontSize: 13, fontFamily: constants.fonts.medium,
										textAlign: "center"
									}}>{product.size}</Text>
								</View>
								<View style={{ width: "10%", alignItems: "center", justifyContent: "center" }}>
									<Text style={{ fontSize: 13, color: "black" }}>
										{product.count}
									</Text>
								</View>
								<View style={{ width: "25%" }}>
									<Text numberOfLines={2} style={{
										color: constants.green,
										fontSize: 13, color: "black", fontFamily: constants.fonts.medium,
										textAlign: "right"
									}}>{product.price * product.count} {constants.currency}</Text>
								</View>
							</View>
						))
					}
				</ CardView>
				< CardView cardElevation={5}
					cardMaxElevation={5}
					cornerRadius={5}
					style={{
						height: "auto",
						width: "100%",
						backgroundColor: "white",
						borderBottomWidth: 3,
						borderColor: constants.backgroundPageColor,
						marginBottom: 25,
						height: "auto",
						justifyContent: "flex-start",
						alignItems: "center"
					}}>
					<View style={{
						width: "100%",
						height: 40,
						backgroundColor: constants.lightgreen,
						justifyContent: "center"
					}}>
						<Text style={{
							color: "black",
							fontSize: 14, color: "black", fontFamily: constants.fonts.semiBold,
							marginLeft: 10,
						}}>Payment Summary</Text>
					</View>
					<View style={{
						width: "100%",
						height: "auto",
						justifyContent: "space-around",
						alignItems: "center",
						borderTopWidth: 2,
						paddingTop: 5,
						paddingBottom: 5,
						borderTopColor: constants.backgroundPageColor
					}}>
						<View style={{
							width: "100%",
							height: 35,
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center"
						}}>
							<Text style={{
								fontSize: 13, marginLeft: 10,
								color: "black", fontFamily: constants.fonts.medium
							}}>Total Price (Inc. Tax)</Text>
							<Text style={{
								fontSize: 15,
								color: "black", fontFamily: constants.fonts.semiBold,
								marginRight: 10
							}}>{order.total_amount} {constants.currency}</Text>
						</View>
						{
							order.taxes.map((tax, index) => (
								<View style={{
									width: "100%",
									height: 35,
									flexDirection: "row",
									justifyContent: "space-between",
									alignItems: "center"
								}} key={index}>
									<Text style={{
										fontSize: 13, marginLeft: 10,
										color: "black", fontFamily: constants.fonts.medium
									}}>{tax.name}</Text>
									<Text style={{
										fontSize: 13,
										color: "black", fontFamily: constants.fonts.semiBold,
										marginRight: 10
									}}>{tax.value} {constants.currency}</Text>
								</View>
							))
						}
						<View style={{
							width: "100%",
							height: 35,
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center"
						}} >
							<Text style={{
								fontSize: 13, marginLeft: 10,
								color: "black", fontFamily: constants.fonts.medium
							}}>Delivery Charge</Text>
							{
								order.delivery_charges ?
									<Text style={{
										fontSize: 13,
										color: "black", fontFamily: constants.fonts.semiBold,
										marginRight: 10
									}}>{order.delivery_charges} {Constants.currency}</Text> :
									<Text style={{
										fontSize: 13,
										fontFamily: constants.fonts.semiBold,
										color: "black",
										marginRight: 10
									}}>FREE</Text>
							}
						</View>
						{
							order.coupon ?
								<View style={{
									width: "100%",
									height: 35,
									flexDirection: "row",
									justifyContent: "space-between",
									alignItems: "center",
									borderTopWidth: 2,
									borderTopColor: constants.backgroundPageColor
								}}>
									<Text style={{
										fontSize: 15,
										color: "black", fontFamily: constants.fonts.bold,
										color: constants.green,
										marginLeft: 10
									}}>{this.state.order.coupon.code}
										<Text style={{
											fontSize: 15,
											color: "black", fontFamily: constants.fonts.semiBold,
											color: "black",
											marginLeft: 10
										}}> Applied</Text>
									</Text>
									<Text style={{
										fontSize: 15,
										color: "black", fontFamily: constants.fonts.semiBold,
										color: "black",
										marginRight: 10
									}}>- {this.state.order.discount} {constants.currency}</Text>
								</View> : null
						}
					</View>
					<View style={{
						width: "100%",
						height: 50,
						flexDirection: "row",
						justifyContent: "space-between",
						alignItems: "center",
						borderTopWidth: 2,
						borderTopColor: constants.backgroundPageColor
					}}>
						<Text style={{
							fontSize: 15,
							color: "black", fontFamily: constants.fonts.semiBold,
							color: "black",
							marginLeft: 10
						}}>Grand Total</Text>
						<Text style={{
							fontSize: 15,
							color: "black", fontFamily: constants.fonts.semiBold,
							color: "black",
							marginRight: 10
						}}>{(this.state.order.total_amount_after_tax + this.state.order.delivery_charges) - this.state.order.discount} {constants.currency}</Text>
					</View>
				</CardView>
				{
					order.status == 1 ?
						<TouchableOpacity style={{
							backgroundColor: constants.red,
							height: 50,
							width: this.state.screenWidth - 30,
							borderRadius: 30,
							justifyContent: "center",
							alignItems: "center"
						}} onPress={() => this.cancelAlert()}>
							<Text style={{
								color: "white",
								fontSize: 16,
								fontFamily: constants.fonts.bold
							}}>CANCEL ORDER</Text>
						</TouchableOpacity> : null
				}
				{/* {
						(order.status == 5 || order.status == 3) ?
							<TouchableOpacity style={{
								backgroundColor: "gold",
								height: 50,
								width: this.state.screenWidth - 30,
								borderRadius: 30,
								justifyContent: "center",
								alignItems: "center",
								marginTop: 15
							}} onPress={() => this.checkAvailability()}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: constants.fonts.bold
								}}>REORDER</Text>
							</TouchableOpacity> : null
					} */}
				{
					order.status == 3 ?
						<TouchableOpacity style={{
							backgroundColor: constants.green,
							height: 50,
							width: this.state.screenWidth - 30,
							borderRadius: 30,
							justifyContent: "center",
							alignItems: "center",
							marginTop: 15
						}} onPress={() => this.getInvoice()}>
							<Text style={{
								color: "white",
								fontSize: 16,
								fontFamily: constants.fonts.bold
							}}>GET INVOICE</Text>
						</TouchableOpacity> : null
				}
			</ScrollView>
		)
	}

	render() {
		console.log("Order details");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderDetailsPage()}
				</Animated.ScrollView>
				<Animated.View style={{ top: 0, left: 0, right: 0, position: 'absolute', flexDirection: 'row', ...Platform.select({ ios: { height: (getStatusBarHeight(true) + 40), }, android: { height: (getStatusBarHeight(true) + 40), } }), backgroundColor: '#1faf4b' }}>
					<Animated.View style={{ height: '100%', width: '10%', marginLeft: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
						<TouchableHighlight style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 45 }, android: { marginTop: 14 } }) }} underlayColor='transparent'
							onPress={() => {
								NavigationService.goBack();
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
					}}>
						<Text style={{
							fontSize: 18,
							color: 'white', fontFamily: constants.fonts.bold,
						}}>Order Detail</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(null, mapDispatchToProps)(OrderDetailsScreen)