import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity, Alert, Animated, TouchableHighlight, ScrollView, Platform } from 'react-native';
import constants from '../../constants/constants';
import AsyncStorage from '@react-native-community/async-storage';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Images from '../../global/images';
import { NavigationEvents } from "react-navigation";
import CardView from 'react-native-cardview';
import styles from "./styles";
import Toast from 'react-native-simple-toast';
import OffScreen from '../../global/off';
import NavigationService from '../../global/NavigationService';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class AllProductsScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			stores: [],
			loading: false,
			headerHeight: 0,
			totalCartItem: 0,
			products: [],
			title: "All Products",
			storeChanged: false
		};
	}

	componentDidMount = () => {
		let params = this.props.navigation.state.params;

		this.setState({
			products: constants.products,
			title: params.title,
			storeChanged: params.storeChanged ? true : false
		}, () => {
			console.log(JSON.stringify(this.state.products))
		})
		this.calculateCartItem();
	}

	calculateCartItem = () => {
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			counter = counter + cart[i].count;
		}
		this.setState({ totalCartItem: counter }, () => {
			console.log("total items" + this.state.totalCartItem);
		});
	}

	removeProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				if (stateProducts[index].count && stateProducts[index].count > 0) {
					stateProducts[index].count--;
				}
				cart[i].count = stateProducts[index].count;
				if (cart[i].count == 0) cart.splice(i, 1);
				break;
			}
			if (!cart[i].count || cart[i].count < 1) {
				cart.splice(i, 1);
			}
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts,
			}, () => {
				console.log("update featured products");
				this.calculateCartItem();
			})
		});
	}

	clearCartAlert = () => {
		Alert.alert(
			'Watch Out',
			'You have an active shopping cart from a different store. That cart will be cleared. Do you want to continue?',
			[
				{
					text: 'CLEAR CART',
					onPress: () => {
						AsyncStorage.removeItem("cart").then(() => {
							// this.addProduct(index, type)
							this.props.setCart([]);
							this.setState({ storeChanged: false }, () => {
								this.calculateCartItem();
							})
						})
					}
				},
				{
					text: 'CANCEL',
					onPress: () => console.log('Cancel Pressed'),
					style: 'cancel',
				}
			],
			{ cancelable: false },
		);
	}

	changedStoreCheck = () => {
		if (this.state.storeChanged) {
			Alert.alert(
				'Alert',
				'Your basket has products from another store. Clear basket to shop from this store?',
				[
					{
						text: 'CLEAR',
						onPress: () => {
							AsyncStorage.removeItem("cart").then(() => {
								// this.addProduct(index, type)
								this.props.setCart([]);
								this.setState({ storeChanged: false }, () => {
									this.calculateCartItem();
								})
							})
						},
						style: 'cancel',
					},
					{
						text: 'CANCEL',
						onPress: () => console.log('Cancel Pressed')
					}
				],
				{ cancelable: false },
			);
		} else this.goToCart();
	}

	checkStoreForCart = (index) => {
		let selectedProducts = this.state.products;
		product = selectedProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let changedStore = false;
		for (let i = 0; i < cart.length; i++) {
			if (product.store_id != cart[i].store_id) {
				changedStore = true;
				break;
			}
		}
		if (changedStore) {
			this.clearCartAlert();
		} else this.addProduct(index);
	}

	addProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				counter++;
				stateProducts[index].count++;
				cart[i].count = stateProducts[index].count;
				break;
			}
		}
		if (counter == 0) {
			stateProducts[index].count = 1;
			cart.push(stateProducts[index]);
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts
			}, async () => {
				console.log("update featured products");
				this.calculateCartItem();
				constants.cartStore = constants.store;
				constants.cartCity = constants.city;
				constants.cartArea = constants.area;
				await AsyncStorage.setItem("cartStore", JSON.stringify(constants.store));
				await AsyncStorage.setItem("cartCity", JSON.stringify(constants.city));
				await AsyncStorage.setItem("cartArea", JSON.stringify(constants.area));
			})
		});
	}

	goToCart = () => {
		constants.previousStateForCart = this.props.navigation.state.routeName;
		if (this.props.navigation.state.routeName == "AllProducts") {
			NavigationService.navigate("Cart");
		} else NavigationService.navigate("StoreCart");
	}

	renderPageItems = () => {
		let { products } = this.state
		return (
			<ScrollView style={{
				width: "100%",
			}} showsVerticalScrollIndicator={false}>
				<View style={{
					flexDirection: "row",
					flexWrap: 'wrap',
					marginBottom: "10%",
					paddingLeft: 7.5,
					paddingRight: 7.5
				}}>
					{
						products.map((product, index) =>
							(
								<CardView cardElevation={5}
									cardMaxElevation={5}
									cornerRadius={5}
									style={styles.productBox} key={index}>
									<OffScreen offValue={product.off}></OffScreen>
									<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }}
										style={styles.productImage}></Image>
									<View style={styles.productQuantity}>
										<Text style={{ fontSize: 10, fontWeight: "600", color: "black" }}>{product.size}</Text>
									</View>
									<Text style={{
										fontSize: 13, textAlign: "center",
										color: "black", fontFamily: constants.fonts.medium,
										marginLeft: 3, marginRight: 3,
									}} numberOfLines={2}>{product.name}</Text>
									<View style={{
										flexDirection: "row", width: "100%", flexWrap: "wrap",
										justifyContent: "center", paddingLeft: 5, paddingRight: 5
									}}>
										{
											(product.price.cost_price && product.price.cost_price != product.price.sale_price) ?
												<Text style={{
													color: "black", textDecorationLine: "line-through",
													fontFamily: constants.fonts.semiBold,
													fontSize: 11
												}}>{product.price.cost_price} {constants.currency}</Text> : null
										}
										<Text style={{
											color: constants.green, marginLeft: 5,
											fontFamily: constants.fonts.semiBold,
											fontSize: 11
										}}>{product.price.sale_price} {constants.currency}</Text>
									</View>
									{/* <Text style={{ color: constants.green }}>{product.price.sale_price} {constants.currency}</Text> */}
									<View style={[styles.productCounter, {
										borderColor: product.count > 0 ? constants.green : "lightgrey"
									}]}>
										<TouchableOpacity style={[styles.counterButton, {
											borderColor: product.count > 0 ? constants.green : "lightgrey",
											backgroundColor: product.count > 0 ? constants.green : "transparent"
										}]} onPress={() => this.removeProduct(index)}>
											<Text style={{
												fontSize: 25,
												marginTop: -4,
												color: product.count > 0 ? "white" : "black"
											}}>-</Text>
										</TouchableOpacity>
										<Text style={{ fontSize: 11, color: "black" }}>
											{product.count > 0 ? product.count : "Add To Cart"}
										</Text>
										<TouchableOpacity style={[styles.counterButton, {
											backgroundColor: product.count > 0 ? constants.green : "transparent",
											borderColor: product.count > 0 ? constants.green : "lightgrey"
										}]} onPress={() => {
											if (product.count >= product.order_max) {
												Toast.showWithGravity("Your maxmimum limit has been reached for this product . . .", Toast.SHORT, Toast.CENTER);
											} else if (product.count >= product.stock_quantity) {
												Toast.showWithGravity("Product unavailable now . . .", Toast.SHORT, Toast.CENTER);
											} else this.checkStoreForCart(index)
										}}>
											<Text style={{
												fontSize: 20,
												marginTop: -4,
												color: product.count > 0 ? "white" : "black"
											}}>+</Text>
										</TouchableOpacity>
									</View>
								</CardView>
							))
					}
				</View>
			</ScrollView>
		)
	}

	render() {
		console.log("All produxts");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<NavigationEvents
					onWillFocus={() => {
						this.calculateCartItem();
						//Call whatever logic or dispatch redux actions and update the screen!
					}}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					contentContainerStyle={{ paddingTop: (this.state.headerHeight) - 10 }}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderPageItems()}
				</Animated.ScrollView>
				<Animated.View style={{ top: 0, left: 0, right: 0, position: 'absolute', flexDirection: 'row', ...Platform.select({ ios: { height: (getStatusBarHeight(true) + 40), }, android: { height: (getStatusBarHeight(true) + 40), } }), backgroundColor: '#1faf4b' }}>
					<Animated.View style={{
						height: '100%',
						width: 60,
						justifyContent: 'center',
						alignItems: 'center',
						flexDirection: 'row',
						...Platform.select({
							ios: { marginTop: 15 }, android: { marginTop: 0 }
						}),
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							justifyContent: "center",
							alignItems: "center"
						}} underlayColor='transparent'
							onPress={() => NavigationService.goBack()}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						...Platform.select({
							ios: { marginTop: 15 }, android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: (this.state.screenWidth - 110),
						// alignItems: "center"
					}}>
						<Text style={{ fontSize: 16, color: 'white', fontFamily: constants.fonts.semiBold }}>{this.state.title}</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						width: 50,
						justifyContent: "space-around",
						alignItems: 'center', flexDirection: 'row',
						...Platform.select({
							ios: { marginTop: 15 }, android: { marginTop: 0 }
						}),
					}}
						onLayout={(event) => {
							this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
								console.log("Header height is:--" + this.state.headerHeight);
							})
							console.log(event.nativeEvent.layout);
						}}>

						<TouchableOpacity style={{
							height: 35, width: 35,
							justifyContent: "center",
							alignItems: "center"
						}} underlayColor='transparent'
							onPress={() => {
								{
									this.state.totalCartItem ?
										this.changedStoreCheck() :
										Toast.showWithGravity("Please add few items in cart", Toast.SHORT, Toast.CENTER);
								}
							}}>
							{
								this.state.totalCartItem ?
									<View style={{
										backgroundColor: "#e9001f",
										width: "auto",
										height: "auto",
										paddingTop: this.state.storeChanged ? 5 : 2,
										paddingBottom: this.state.storeChanged ? 4 : 3,
										paddingLeft: 7,
										paddingRight: 8,
										position: "absolute",
										zIndex: 999,
										top: -5,
										left: -5,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: 15
									}}>
										{
											this.state.storeChanged ?
												<Image source={Images.closeImg} style={{ height: 10, width: 10 }} /> :
												<Text style={{
													color: "white",
													fontFamily: constants.fonts.bold, fontSize: 10
												}}>{this.state.totalCartItem}</Text>
										}
									</View> : null
							}
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
						</TouchableOpacity>

					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AllProductsScreen)