import { StyleSheet, Platform } from 'react-native';
import Constants from '../../constants/constants'

export default StyleSheet.create({
	toolbar: {
		height: (Platform.OS === "ios") ? 44 : 56,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: Constants.green,
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.2,
		elevation: 3,
		shadowRadius: 0,
	},
	container: {
		flex: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		// height: '100%',
		width: '100%',
	},
	textTitle: {
		fontSize: 20,
		color: "white",
		marginLeft: 15,
	},
	map: {
		flex: 1,
		width: '100%'
	},
	mapText: {
		borderWidth: 1,
		borderColor: "#aaa",
		borderRadius: 5,
		padding: 5,
		backgroundColor: "#ffffff",
		textAlign: "center"
	}
})