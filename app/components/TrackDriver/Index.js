import React from 'react'
import { connect } from 'react-redux'
import { Platform, Alert, DeviceEventEmitter, View, Dimensions, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import PolylineDecoder from '@mapbox/polyline';

import styles from './Styles';
import FirebaseDatabase from '../../firebase-database';
import { TextBold } from '../../global/text';
import DriverComponent from './Driver';
import Constants from '../../constants/constants';
import Images from '../../global/images';
import * as ApiManager from '../../apimanager/ApiManager';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const latDelta = 0.10
const longDelta = latDelta * ASPECT_RATIO

class TrackDriver extends React.Component {

	static navigationOptions = {
		header: null
	};

	constructor() {
		super()
		interval = 0;
	}

	state = {
		initialized: false,
		deliverLatitude: 0,
		deliverLongitude: 0,
		mLattitude: 0,
		mLongitude: 0,
		oldLattitude: 0,
		oldLongitude: 0,
		pickUpLatitude: 0,
		pickUpLongitude: 0,
		orderStatus: "0",
		polylineList: [],
		tempPolylineList: [],
		currentBearing: 0,
		newBearing: 0,
		oldBearing: 0,
		driverLatitude: 0,
		driverLongitude: 0,
		distance: "",
		driverLocation: "",
		duration: {}
	}

	componentWillMount() {
		const { navigation } = this.props;
		const order = navigation.getParam('order', null);
		this.order = order
		console.log("ORDER_DRIVER:" + JSON.stringify(order))
	}

	componentDidMount() {
		DeviceEventEmitter.addListener(Constants.EVENTS.liveLocation, (data1) => {
			data1 = JSON.stringify(data1)
			let data = JSON.parse(data1)
			if (!data.mLongitude || !data.mLattitude) {
				data.mLattitude = data.deliverLatitude;
				data.mLongitude = data.deliverLongitude;
				console.log("LIVE LOCATION NOT FOUND:" + data.mLattitude + " " + data.mLongitude)
				// Alert.alert()
			} else {
				console.log("CHANGE_LOC:" + data.mLattitude + " " + data.mLongitude)
			}

			this.setState(prevState => ({
				deliverLatitude: data.deliverLatitude,
				deliverLongitude: data.deliverLongitude,
				mLattitude: data.mLattitude,
				mLongitude: data.mLongitude,
				oldLattitude: prevState.mLattitude == 0 ? data.mLattitude : prevState.mLattitude,
				oldLongitude: prevState.mLongitude == 0 ? data.mLongitude : prevState.mLongitude,
				pickUpLatitude: data.pickUpLatitude,
				pickUpLongitude: data.pickUpLongitude,
				orderStatus: data.orderStatus,
				newBearing: this.getBearing(prevState.oldLattitude, prevState.oldLongitude, data.mLattitude, data.mLongitude),
				oldBearing: prevState.newBearing,
				initialized: true,
			}), () => {
				this.callGoogleMapDirectionApi()
			})
		})
	}

	componentWillUnmount() {
		DeviceEventEmitter.removeAllListeners();
		window.clearInterval(this.interval);
	}

	onBackClick = () => {
		const { navigation } = this.props;
		navigation.goBack();
		this.props.navigation.state.params.backFuction();
		// DeviceEventEmitter.emit("back:from:map");
	}

	// Converts from degrees to radians.
	toRadians(degrees) {
		return degrees * Math.PI / 180;
	};

	// Converts from radians to degrees.
	toDegrees(radians) {
		return radians * 180 / Math.PI;
	}

	getBearing(startLat, startLng, destLat, destLng) {
		startLat = this.toRadians(startLat);
		startLng = this.toRadians(startLng);
		destLat = this.toRadians(destLat);
		destLng = this.toRadians(destLng);

		y = Math.sin(destLng - startLng) * Math.cos(destLat);
		x = Math.cos(startLat) * Math.sin(destLat) -
			Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
		brng = Math.atan2(y, x);
		brng = this.toDegrees(brng);
		return (brng + 360) % 360;
	}

	animatePolyline = () => {
		// this.interval = setInterval(() => this.animatePolylineStart(), 100);
	};

	animatePolylineStart = () => {
		console.log("PATH1:" + this.interval + "     " + JSON.stringify(this.state.polylineList))
		if (this.state.polylineList.length > 0) {
			const Direction = this.state.polylineList;
			const polylineList = [
				...Direction.slice(0, this.state.polylineList.length - 1)
			];
			this.setState({ polylineList });
		} else {
			clearInterval(this.interval)
		}
	};

	animateDriver = () => {
		this.oldBearing = this.state.oldBearing
		this.newBearing = this.state.newBearing
		this.currentBearing = this.oldBearing
		this.setState({
			currentBearing: this.newBearing
		}, () => {
			this.setState(prevState => ({
				driverLatitude: prevState.mLattitude,
				driverLongitude: prevState.mLongitude,
				polylineList: this.state.tempPolylineList
			}))
		})
	}

	animateDriverStart = (startTimeInMillis, duration) => {
		let elapsed = (new Date).getTime() - startTimeInMillis
		let animationTime = elapsed / duration
		let rot = animationTime * this.newBearing + (1 - animationTime) * this.oldBearing;
		this.currentBearing = (-rot > 180) ? (rot / 2) : rot

		this.setState({
			currentBearing: this.currentBearing
		})
		if (animationTime >= 1.0) {
			// Post again 16ms later.
			clearInterval(this.driverInterval)
			this.setState(prevState => ({
				driverLatitude: prevState.mLattitude,
				driverLongitude: prevState.mLongitude,
				polylineList: this.state.tempPolylineList
			}))
		}
	}

	callGoogleMapDirectionApi = () => {
		let url = Constants.googleMapDirectionApiUrl + "mode=driving&transit_routing_preference=less_driving&origin=" + this.state.mLattitude + "," + this.state.mLongitude + "&destination=" + this.state.deliverLatitude + "," + this.state.deliverLongitude + "&key=" + Constants.GOOGLE_API_KEY
		console.log("URL:" + url)
		ApiManager.getDirectionsJson(url).then(res => {
			if (res && res.routes) {
				res.routes.forEach(route => {
					let polyLine = route.overview_polyline.points
					let array = PolylineDecoder.decode(polyLine)
					let polylist = array.map((point) => {
						return {
							latitude: point[0],
							longitude: point[1]
						}
					})
					this.setState({
						tempPolylineList: polylist,
						distance: res.routes[0].legs[0].distance.text,
						driverLocation: res.routes[0].legs[0].end_address,
						duration: res.routes[0].legs[0].duration
					}, () => {
						// this.animatePolyline()
						this.animateDriver()
					})
				});
			}
		}).catch(err => {
			setTimeout(() => {
				if (err) {
					if (err != '') {
						alert(err);
					}
				}
			}, 100);
		});
	}

	renderBackButton() {
		if (Platform.OS === "ios") {
			return (
				<TouchableOpacity activeOpacity={0.8}
					onPress={() => this.onBackClick()}>
					<Image source={Images.ic_back_ios}
						style={{ marginLeft: 5, height: 22, width: 22, }}
					></Image>
				</TouchableOpacity>
			)
		} else {
			return (
				<TouchableOpacity activeOpacity={0.8}
					onPress={() => this.onBackClick()}>
					<Image source={Images.ic_back_android}
						style={{ marginLeft: 15, height: 35, width: 25, }}
					></Image>
				</TouchableOpacity>
			)
		}
	}

	renderToolbar() {
		return (
			<View style={styles.toolbar}>
				<View style={{
					flexDirection: 'row',
					alignItems: 'center',
					width: '20%'
				}
				}>
					{this.renderBackButton()}
				</View>
				<View style={{
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: "center",
					width: '60%'
				}}>
					<TextBold title={"ORDER: " + this.order.order_id.toUpperCase()} textStyle={styles.textTitle} />
				</View>
				<View style={{
					flexDirection: 'row',
					alignItems: 'center',
					width: '20%'
				}}>
				</View>
			</View>
		)
	}

	renderMapAndroid() {
		return (
			<MapView
				ref={(mapView) => { this.mapView = mapView; }}
				style={styles.map}
				provider={PROVIDER_GOOGLE}
				showsMyLocationButton={false}
				showsUserLocation={true}
				region={{
					latitudeDelta: latDelta,
					longitudeDelta: longDelta,
					latitude: Number(this.state.mLattitude),
					longitude: Number(this.state.mLongitude)
				}}
			>
				<Marker
					coordinate={{
						latitudeDelta: latDelta,
						longitudeDelta: longDelta,
						latitude: this.state.pickUpLatitude,
						longitude: this.state.pickUpLongitude
					}}
				>
					<View style={{ alignItems: 'center' }}>
						<TextBold title={this.order.store.name} textStyle={styles.mapText} />
						<Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
					</View>

				</Marker>

				<Marker
					coordinate={{
						latitudeDelta: latDelta,
						longitudeDelta: longDelta,
						latitude: this.state.deliverLatitude,
						longitude: this.state.deliverLongitude
					}}
				>
					{this.renderCustomerMarker()}
				</Marker>

				<DriverComponent driver={{
					location: {
						latitudeDelta: latDelta,
						longitudeDelta: longDelta,
						latitude: this.state.driverLatitude,
						longitude: this.state.driverLongitude
					},
					bearing: this.state.currentBearing
				}} />

				{
					this.state.polylineList.length > 0 ?
						<MapView.Polyline
							coordinates={this.state.polylineList}
							strokeWidth={5}
							strokeColor={Constants.green} />
						: null
				}
			</MapView>
		)
	}

	renderCustomerMarker = () => {
		let { duration, distance } = this.state;
		let text = "Distance:- " + distance + "\nApprox Time:- " + duration.text
		if (duration.value < 2) {
			text = "Reached !!";
		}
		return (
			<View style={{ alignItems: 'center', width: 200 }}>
				<TextBold title={text} textStyle={styles.mapText} />
				<Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
			</View>
		)
	}

	renderMapIos() {
		return (
			<MapView
				ref={(mapView) => { this.mapView = mapView; }}
				style={styles.map}
				mapType={Platform.OS == "android" ? "none" : "standard"}
				provider={PROVIDER_GOOGLE}
				showsMyLocationButton={false}
				region={{
					latitudeDelta: latDelta,
					longitudeDelta: longDelta,
					latitude: this.state.mLattitude,
					longitude: this.state.mLongitude
				}}
			>
				<Marker
					coordinate={{
						altitudeDelta: latDelta,
						longitudeDelta: longDelta,
						latitude: this.state.pickUpLatitude,
						longitude: this.state.pickUpLongitude
					}}>
					<View style={{ alignItems: 'center' }}>
						<TextBold title={this.order.store.name} textStyle={styles.mapText} />
						<Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
					</View>
				</Marker>

				<Marker
					coordinate={{
						altitudeDelta: latDelta,
						longitudeDelta: longDelta,
						latitude: this.state.deliverLatitude,
						longitude: this.state.deliverLongitude
					}}
				>
					{this.renderCustomerMarker()}
				</Marker>
				<DriverComponent driver={{
					location: {
						altitudeDelta: latDelta,
						longitudeDelta: longDelta,
						latitude: this.state.driverLatitude,
						longitude: this.state.driverLongitude
					},
					bearing: this.state.currentBearing
				}} />

				{
					this.state.polylineList.length > 0 ?
						<MapView.Polyline
							coordinates={this.state.polylineList}
							strokeWidth={5}
							strokeColor={Constants.green} />
						: null
				}
			</MapView>
		)
	}

	renderMap() {
		return (
			<View style={{ flex: 1, width: '100%' }}>
				{
					Platform.OS == 'android'
						? this.renderMapAndroid()
						: this.renderMapIos()
				}
			</View>
		)
	}

	render() {
		return (

			<View style={{ width: '100%', flex: 1, flexDirection: 'column' }}>
				<SafeAreaView style={{ backgroundColor: Constants.green }}></SafeAreaView>
				<FirebaseDatabase driverId={this.order.driver_id ? this.order.driver_id : 0} orderNumber={this.order.order_id} />
				{this.renderToolbar()}

				<View style={styles.container}>
					{this.renderMap()}
				</View>
			</View>

		);
	};
}

function mapStateToProps(state) {
	return {
		getLiveLocation: state.getLiveLocation,
	}
}

export default connect(mapStateToProps, {})(TrackDriver)
