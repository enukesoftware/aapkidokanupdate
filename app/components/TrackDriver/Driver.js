import React, { Component } from 'react'
import { Animated } from 'react-native';
import Images from '../../global/images';
import { Marker, AnimatedRegion } from 'react-native-maps'

const latDelta = 0.015 * 5
const longDelta = 0.0121 * 5

class DriverComponent extends Component {

	constructor(props) {
		super(props)
	}

	render() {
		return (
			<Marker.Animated
				coordinate={new AnimatedRegion({
					latitudeDelta: latDelta,
					longitudeDelta: longDelta,
					latitude: this.props.driver.location.latitude,
					longitude: this.props.driver.location.longitude
				})}
				anchor={{ x: 0.50, y: 0.50 }}
				ref={marker => { this.marker = marker }}
				styles={{ width: 60, height: 60 }}>
				<Animated.Image source={Images.ic_delivery_boy} style={{ height: 60, width: 60, transform: [{ rotate: `${this.props.driver.bearing}deg` }] }} />
			</Marker.Animated>
		);
	};
}

export default DriverComponent