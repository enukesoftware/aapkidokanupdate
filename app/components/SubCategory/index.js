import React, { Component } from 'react';
import { View, TouchableOpacity, Keyboard, FlatList, ActivityIndicator, Text, Alert, Animated, Image, TouchableHighlight, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import { TextInput } from 'react-native-gesture-handler';
import styles from './styles';
import { Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import OffScreen from '../../global/off';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';
import GlobalStyle from '../../global/styles';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';
const CategoryJson = {
	"status": 1,
	"parent": null,
	"_id": "5da583755a367e70790df9f2",
	"store_id": "5d9eda647a30b1642ce9cb77",
	"name": "Snacks",
	"picture": "3399-216x120.png",
	"created_at": "2019-10-15T08:29:41.407Z",
	"updated_at": "2019-10-15T08:29:41.407Z",
	"imgUrl": {
		"uri": "http://storage.x-e.com.cy/cache/1/zbS2uMlupbIj49yl…7K8f9Q7gH3.jpg?s=5796994b519916a04d4538ba86076236"
	}
};

class SubCategoryScreen extends Component {
	static navigationOptions = {
		header: null
		// header:null //hide the header
	};

	state = {
		products: [],
		index: 0,
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		scrollY: new Animated.Value(0.01),
		category: {},
		subcategories: [],
		searchString: '',
		totalCartItem: 0,
		headerHeight: 0,
		searchTyped: false,
		screenMessage: "",
		storeChanged: false,
		pageNo: 1,
		isLoading: false,
		isRefreshing: false,
		isListEnd: false,
		loadingMore: false,
	};

	componentWillMount = () => {
		let params = this.props.navigation.state.params;
		console.log("params:-" + JSON.stringify(params));
		if (params) {
			this.setState({
				storeChanged: params.storeChanged ? true : false
			})
		}
		this.setState({ category: (constants.category && constants.category._id) ? constants.category : CategoryJson }, () => {
			console.log(this.state.category);
			this.getSubcategories();
		});
	}

	getSubcategories = () => {
		console.log("getting subcategories");
		this.setState({ isLoading: true });
		ApiManager.getSubcategories(this.state.category._id).then((res) => {
			setTimeout(() => {
				this.setState({ isLoading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					this.setState({
						subcategories: res.data.category.subcategories,
					}, () => {
						console.log(this.state.subcategories);
						if (this.state.subcategories.length) {
							this.getProducts();
						}
					});
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 200);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ isLoading: false });
			}, 100);
			Alert.alert(err.singleStringMessage);
			console.log("Otp error" + JSON.stringify(err));
		})
	}

	getProducts = () => {
		let { subcategories, isLoading, searchString, screenMessage, index, pageNo, products } = this.state;
		if (!subcategories.length) {
			this.setState({ screenMessage: "There is not any subcategories or products here . . ." })
			return;
		};
		if (!isLoading) {
			this.setState({ isLoading: true });
		}
		Keyboard.dismiss();
		ApiManager.getProducts(
			subcategories[index]._id,
			searchString, pageNo
		).then((res) => {
			console.log(res);
			setTimeout(() => {
				this.setState({
					isLoading: false,
					isRefreshing: false,
				}, () => console.log("loading paused"));
			}, 100)
			setTimeout(() => {
				if (res && res.success) {
					this.setState({
						isListEnd: (!res.data.products.length || res.data.products.length < 30),
						loadingMore: false,
						products: pageNo === 1 ? res.data.products : [...products, ...res.data.products],
						screenMessage: !res.data.products.length ? "No results found for this product\nPlease search something else \n or \n change the store . . ." : ""
					}, () => {
						console.log(products.length + " products has been set into state")
						this.calculateCartItem();
					});
				} else {
					this.setState({
						screenMessage: res.singleStringMessage
					}, () => {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					})
				}
			}, 500);
		}).catch((err) => {
			this.setState({
				searchTyped: false, loadingMore: false,
				isLoading: false, isRefreshing: false,
				screenMessage: err.singleStringMessage
			}, () => {
				Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				console.log(this.state.screenMessage);
			});
			console.log(err);
		})
	}

	checkStoreForCart = (index) => {
		let selectedProducts = this.state.products;
		product = selectedProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let changedStore = false;
		for (let i = 0; i < cart.length; i++) {
			if (product.store_id != cart[i].store_id) {
				changedStore = true;
				break;
			}
		}
		if (changedStore) {
			this.clearCartAlert();
		} else this.addProduct(index);
	}

	addProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				counter++;
				stateProducts[index].count++;
				cart[i].count = stateProducts[index].count;
				break;
			}
		}
		if (counter == 0) {
			stateProducts[index].count = 1;
			cart.push(stateProducts[index]);
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts
			}, async () => {
				console.log("update featured products");
				this.calculateCartItem();
				constants.cartStore = constants.store;
				constants.cartCity = constants.city;
				constants.cartArea = constants.area;
				await AsyncStorage.setItem("cartStore", JSON.stringify(constants.store));
				await AsyncStorage.setItem("cartCity", JSON.stringify(constants.city));
				await AsyncStorage.setItem("cartArea", JSON.stringify(constants.area));
			})
		});
	}

	calculateCartItem = () => {
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			counter = counter + cart[i].count;
		}
		this.setState({ totalCartItem: counter }, () => {
			console.log("total items" + this.state.totalCartItem);
		});
	}

	removeProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				if (stateProducts[index].count && stateProducts[index].count > 0) {
					stateProducts[index].count--;
				}
				cart[i].count = stateProducts[index].count;
				if (cart[i].count == 0) cart.splice(i, 1);
				break;
			}
			if (!cart[i].count || cart[i].count < 1) {
				cart.splice(i, 1);
			}
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts,
			}, () => {
				console.log("update featured products");
				this.calculateCartItem();
			})
		});
	}

	clearCartAlert = () => {
		Alert.alert(
			'',
			'You have an active shopping cart from a different store. That cart will be cleared. Do you want to continue?',
			[
				{
					text: 'CLEAR CART',
					onPress: () => {
						AsyncStorage.removeItem("cart").then(() => {
							// this.addProduct(index, type)
							this.props.setCart([]);
							this.setState({ storeChanged: false }, () => {
								this.calculateCartItem();
							})
						})
					}
				},
				{
					text: 'CANCEL',
					onPress: () => console.log('Cancel Pressed'),
					style: 'cancel',
				}
			],
			{ cancelable: false },
		);
	}

	changedStoreCheck = () => {
		if (this.state.storeChanged) {
			Alert.alert(
				'',
				'Your basket has products from another store. Clear basket to shop from this store?',
				[
					{
						text: 'CLEAR',
						onPress: () => {
							AsyncStorage.removeItem("cart").then(() => {
								// this.addProduct(index, type)
								this.props.setCart([]);
								this.setState({ storeChanged: false }, () => {
									this.calculateCartItem();
								})
							})
						},
						style: 'cancel',
					},
					{
						text: 'CANCEL',
						onPress: () => console.log('Cancel Pressed')
					}
				],
				{ cancelable: false },
			);
		} else this.goToCart();
	}

	goToCart = () => {
		constants.previousStateForCart = this.props.navigation.state.routeName;
		if (this.props.navigation.state.routeName == "HomeSubCategory") {
			NavigationService.navigate("Cart");
		} else NavigationService.navigate("CategoryCart");
	}

	refreshCart = () => {
		console.log("Came here from cart");
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let stateProducts = this.state.products;
		for (let j = 0; j < stateProducts.length; j++) {
			stateProducts[j].count = 0;
			for (let i = 0; i < cart.length; i++) {
				if (cart[i]._id == stateProducts[j]._id) {
					stateProducts[j].count = cart[i].count;
				}
			}
		}
		this.setState({
			products: stateProducts
		}, () => {
			this.calculateCartItem();
		})
	}

	renderBlankPage = () => {
		let { products, screenMessage, headerHeight } = this.state;
		if (products.length) {
			return null;
		}
		return (
			<View style={{
				flex: 1,
				justifyContent: "center",
				alignItems: "center"
			}}>
				<Text style={{
					fontSize: 17,
					color: "black",
					textAlign: "center",
					fontWeight: "600"
				}}>{screenMessage}</Text>
			</View>
		)
	}

	renderProducts = () => {
		let { products, isRefreshing } = this.state;
		return (
			<View style={{
				flex: 1,
				// marginBottom: 50,
				paddingLeft: 7.5,
				paddingRight: 7.5
			}}>
				<FlatList data={products}
					showsVerticalScrollIndicator={false}
					renderItem={({ item, index }) => this.renderProduct(item, index)}
					numColumns={2}
					extraData={this.state}
					keyExtractor={this.keyExtractor}
					onEndReached={this.handleLoadMore}
					onEndReachedThreshold={.5}
					onRefresh={this.refreshProducts}
					refreshing={isRefreshing}
					renderFooter={this.renderFooter} />
			</View>
		)
	}

	renderProduct = (product, index) => {
		return (
			<View style={[styles.productBox, {
				marginTop: index == 0 || index == 1 ? 20 : 0
			}]} key={index}>
				<OffScreen offValue={product.off}></OffScreen>
				<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }}
					style={styles.productImage}></Image>
				<View style={styles.productQuantity}>
					<Text style={{ fontSize: 10, color: "black", fontFamily: constants.fonts.semiBold }}>{product.size}</Text>
				</View>
				<Text style={{
					color: "black", textAlign: "center", fontSize: 13,
					fontFamily: constants.fonts.medium, marginLeft: 5, marginRight: 5,
				}} numberOfLines={2}>{product.name} </Text>
				<View style={{
					flexDirection: "row", width: "100%",
					justifyContent: "center", paddingLeft: 5, paddingRight: 5,
					flexWrap: "wrap"
				}}>
					{
						(product.price.cost_price && product.price.cost_price != product.price.sale_price) ?
							<Text style={{
								color: "black", textDecorationLine: "line-through",
								fontFamily: constants.fonts.semiBold,
								fontSize: 11,
							}}>{product.price.cost_price} {constants.currency}</Text> : null
					}
					<Text style={{
						color: constants.green, marginLeft: 5,
						fontFamily: constants.fonts.semiBold,
						fontSize: 11,
					}}>{product.price.sale_price} {constants.currency}</Text>
				</View>
				<View style={[styles.productCounter, {
					borderColor: product.count > 0 ? constants.green : "lightgrey"
				}]}>
					<TouchableOpacity style={[styles.counterButton, {
						borderColor: product.count > 0 ? constants.green : "lightgrey",
						backgroundColor: product.count > 0 ? constants.green : "transparent"
					}]} onPress={() => this.removeProduct(index)}>
						<Text style={{
							fontSize: 25,
							marginTop: -4,
							color: product.count > 0 ? "white" : "black"
						}}>-</Text>
					</TouchableOpacity>
					<Text style={{ fontSize: 11, color: "black", color: "black", fontFamily: constants.fonts.medium }}>
						{product.count > 0 ? product.count : "Add To Cart"}
					</Text>
					<TouchableOpacity style={[styles.counterButton, {
						backgroundColor: product.count > 0 ? constants.green : "transparent",
						borderColor: product.count > 0 ? constants.green : "lightgrey"
					}]} onPress={() => {
						if (product.count >= product.order_max) {
							Toast.showWithGravity("Your maxmimum limit has been reached for this product . . .", Toast.SHORT, Toast.CENTER);
						} else if (product.count >= product.stock_quantity) {
							Toast.showWithGravity("Product unavailable now . . .", Toast.SHORT, Toast.CENTER);
						} else this.checkStoreForCart(index)
					}}>
						<Text style={{
							fontSize: 20,
							marginTop: -4,
							color: product.count > 0 ? "white" : "black"
						}}>+</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	renderFooter = () => {
		let { isListEnd, loadingMore } = this.state;
		if (!loadingMore || isListEnd) {
			console.log("loadingMore:-" + loadingMore)
			return null;
		}
		return (
			<View
				style={{
					width: "100%",
					height: 50,
					marginTop: 10,
					marginBottom: 10,
					borderColor: constants.lightgreen,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator
					color={constants.green}
					animating
					size="large" />
			</View>
		);
	}

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getProducts();
			});
		}
	};

	refreshProducts = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getProducts();
			})
		}
	}

	keyExtractor = (item) => item._id;

	renderPageItems = () => {
		let { searchString, searchTyped, subcategories } = this.state;
		return (
			<View style={{
				flex: 1, alignItems: 'center',
				justifyContent: 'flex-start',
				backgroundColor: constants.backgroundPageColor,
				marginTop: getStatusBarHeight(true) + 10
			}}>
				<View style={{
					height: 50,
					width: this.state.screenWidth,
					backgroundColor: "white"
				}}>
					<ScrollView horizontal={true}
						keyboardShouldPersistTaps={'handled'}
						showsHorizontalScrollIndicator={false}>
						{
							subcategories.map((item, index) =>
								(
									<TouchableOpacity style={{
										width: "auto",
										height: 50,
										backgroundColor: "white",
										justifyContent: "center",
										alignItems: "center",
										paddingLeft: 15,
										paddingRight: 15,
										borderBottomWidth: this.state.index === index ? 1.5 : 0,
										borderBottomColor: constants.green
									}} key={index}
										onPress={() => {
											this.setState({ searchString: "", pageNo: 1, index, products: [] }, () => {
												this.getProducts();
											})
										}}>
										<Text style={{
											fontSize: 16,
											color: this.state.index === index ? constants.green : "#4d4d4d",
											color: "black", fontFamily: constants.fonts.semiBold,
											textTransform: "capitalize",
										}}>{item.name} </Text>
									</TouchableOpacity>
								))
						}
					</ScrollView>
				</View>
				<View style={{
					width: this.state.screenWidth,
					height: 50,
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
					paddingBottom: 30,
					paddingTop: 30,
					paddingLeft: 15,
					paddingRight: 15,
					marginTop: 20,
				}}>
					<View style={{
						height: 50,
						backgroundColor: "white",
						width: "100%",
						borderRadius: 30,
						flexDirection: "row",
						justifyContent: "space-between",
					}}>
						<View style={{
							width: "70%", height: 50,
							justifyContent: "center", alignItems: "center",
							paddingLeft: 10
						}}>
							<TextInput style={{ width: '100%' }}
								placeholder="Search Item in Grocery Shop"
								placeholderTextColor="#4d4d4d"
								value={searchString}
								onChangeText={(text) => {
									this.setState({ searchString: text }, () => {
										if (this.state.searchTyped && this.state.searchString.length == 0) {
											this.setState({
												pageNo: 1, products: []
											}, () => {
												this.getProducts();
											})
										}
									})
								}}
								returnKeyType="done"
								onSubmitEditing={() => {
									if (searchString.length > 0) {
										this.setState({ pageNo: 1, products: [], searchTyped: true }, () => {
											this.getProducts();
										})
									}
								}}
							></TextInput>
						</View>
						<TouchableOpacity style={{
							width: 100, height: 50,
							backgroundColor: constants.green,
							justifyContent: "center",
							alignItems: "center",
							borderRadius: 30
						}} onPress={() => {
							if (searchString.length > 0) {
								this.setState({ pageNo: 1, products: [], searchTyped: true }, () => {
									this.getProducts();
								})
							}
						}}>
							<Text style={{ fontSize: 15, color: "black", fontFamily: constants.fonts.bold, color: "white" }}>SEARCH</Text>
						</TouchableOpacity>
					</View>
				</View>

				{this.renderProducts()}
				{this.renderBlankPage()}
			</View>
		)
	}

	render() {
		let { isLoading, isRefreshing } = this.state;
		console.log("Subcategory screen");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={isLoading || isRefreshing}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={GlobalStyle.loader}
				/>
				<NavigationEvents onWillFocus={() => {
					this.setState({ searchString: "", searchTyped: false, screenMessage: "" }, () => {
						// this.getProducts();
						this.refreshCart();
					})
					//Call whatever logic or dispatch redux actions and update the screen!
				}}
				/>
				<Animated.View
					style={{ flex: 1 }}>
					{this.renderPageItems()}
				</Animated.View>
				<Animated.View style={{ top: 0, left: 0, right: 0, position: 'absolute', flexDirection: 'row', ...Platform.select({ ios: { height: (getStatusBarHeight(true) + 40), }, android: { height: (getStatusBarHeight(true) + 40), } }), backgroundColor: '#1faf4b' }}>

					<Animated.View style={{ height: '100%', width: '10%', paddingLeft: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}
						onLayout={(event) => {
							this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
								console.log("Header height is:--" + this.state.headerHeight);
							})
							console.log(event.nativeEvent.layout);
						}}>
						<TouchableHighlight style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 45 }, android: { marginTop: 14 } }) }} underlayColor='transparent' onPress={() => {
							NavigationService.navigate(constants.previousStateForSubCategory);
						}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center', width: '70%',
						// alignItems: "center",
						// backgroundColor: "red"
					}}>
						<Text style={{
							fontSize: 16,
							color: 'white', fontFamily: constants.fonts.bold,
						}}>{this.state.category.name}</Text>
					</Animated.View>

					<Animated.View style={{ height: '100%', width: '20%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
						<TouchableHighlight style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 40 }, android: { marginTop: 10 } }) }} underlayColor='transparent' onPress={() => {
							NavigationService.navigate("Search");
							// NavigationService.navigate(constants.previousStateForSubCategory);
						}}>
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.search} />
						</TouchableHighlight>
						<TouchableOpacity style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 40 }, android: { marginTop: 10 } }) }} underlayColor='transparent'
							onPress={() => {
								{
									this.state.totalCartItem ?
										this.changedStoreCheck() :
										Toast.showWithGravity("Please add few items in cart", Toast.SHORT, Toast.CENTER);
								}
							}}>
							{
								this.state.totalCartItem ?
									<View style={{
										backgroundColor: "#e9001f",
										width: "auto",
										height: "auto",
										paddingTop: this.state.storeChanged ? 5 : 2,
										paddingBottom: this.state.storeChanged ? 4 : 3,
										paddingLeft: 7,
										paddingRight: 8,
										position: "absolute",
										zIndex: 999,
										top: -12,
										left: -8,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: 15
									}}>
										{
											this.state.storeChanged ?
												<Image source={Images.closeImg} style={{ height: 10, width: 10 }} /> :
												<Text style={{
													color: "white",
													fontFamily: constants.fonts.bold, fontSize: 10
												}}>{this.state.totalCartItem}</Text>
										}
									</View> : null
							}
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
						</TouchableOpacity>

					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SubCategoryScreen)