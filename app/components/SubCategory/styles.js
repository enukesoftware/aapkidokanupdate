import { StyleSheet, Dimensions } from 'react-native';
export default StyleSheet.create({
	counterImage: { height: 10, width: 10 },
	productImage: { height: 100, width: "97%", resizeMode: 'contain', marginTop: 5 },
	productBox: {
		backgroundColor: "white",
		height: 235,
		width: (((Dimensions.get("window").width) / 2) - 23),
		borderRadius: 10,
		shadowColor: 'dimgrey',
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .5,
		shadowRadius: 2,
		elevation: 2,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: 7.5,
		marginRight: 7.5,
		marginBottom: 15
	},
	productQuantity: {
		height: 25,
		minWidth: 40,
		borderRadius: 20,
		paddingLeft: 10,
		paddingRight: 10,
		backgroundColor: "#d0efdd",
		justifyContent: "center",
		alignItems: "center"
	},
	productCounter: {
		borderColor: "lightgrey",
		borderWidth: 1,
		height: 30,
		alignItems: "center",
		width: "90%",
		justifyContent: "space-between",
		borderRadius: 30,
		flexDirection: "row",
		marginBottom: 10,
		borderLeftWidth: 0,
		borderRightWidth: 0,
	},
	counterButton: {
		height: 30, width: 30,
		// borderLeftColor: 'transparent',
		alignItems: "center", justifyContent: "center",
		borderWidth: 1, borderColor: "lightgrey",
		borderRadius: 15
	}
});