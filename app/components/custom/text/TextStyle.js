import React, { Component } from 'react'
import { Text } from 'react-native';
import constants from '../../../constants/constants';


export const TextBold = (props) => {

    return (
        <Text allowFontScaling={false} ellipsizeMode={props.ellipsizeMode} numberOfLines={props.numberOfLines} style={[{fontFamily: constants.fonts.bold}, props.textStyle]}>{props.title}</Text>
    );
}

export const TextRegular = (props) => {

    return (
        <Text allowFontScaling={false} ellipsizeMode={props.ellipsizeMode} numberOfLines={props.numberOfLines} style={[{fontFamily: constants.fonts.regular}, props.textStyle]}>{props.title}</Text>
    );
}

export const TextLite = (props) => {

    return (
        <Text allowFontScaling={false} ellipsizeMode={props.ellipsizeMode} numberOfLines={props.numberOfLines} style={[{fontFamily: constants.fonts.light}, props.textStyle]}>{props.title}</Text>
    );
}
export const TextThin = (props) => {

    return (
        <Text allowFontScaling={false} ellipsizeMode={props.ellipsizeMode} numberOfLines={props.numberOfLines} style={[{fontFamily: constants.fonts.thin}, props.textStyle]}>{props.title}</Text>
    );
}