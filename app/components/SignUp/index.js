
import React, { Component } from 'react';
import {
	Image, View, Text, TextInput, TouchableOpacity, ImageBackground,
	ScrollView, findNodeHandle, Dimensions
} from 'react-native'
import styles from '../../global/styles';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import * as SocialHelper from '../../apimanager/social';
import constants from '../../constants/constants';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import Images from '../../global/images';

export default class SignUpScreen extends Component {
	static navigationOptions = () => ({
		header: null
	});

	state = {
		email: '',
		password: '',
		full_name: '',
		cPassword: '',
		contact_number: '',
		loading: false
	}

	componentDidMount() {
		SocialHelper._configureGoogleSignIn();
	}

	googleSignIn = async () => {
		try {
			this.setState({ loading: true });
			const result = await SocialHelper.googleLogin();
			ApiManager.googleLogin({
				id_token: result.idToken
			}).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false, }, () => console.log("Loading paused"));
				}, 100);
				setTimeout(() => {
					if (res && res.success && res.code == 200) {
						constants.user = res.data.user;
						if (this.props.navigation.state.routeName == "OtherSignUp") {
							NavigationService.navigate("MainStack", { logined: true });
						} else {
							NavigationService.navigate(constants.previousStateForLogin, { logined: true });
						}
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
					}
				}, 500)
			}).catch((err) => {
				this.setState({ loading: false }, () => {
					console.log(JSON.stringify(err));
				})
			})
		} catch (error) {
			this.setState({ loading: false }, () => {
				console.log('error in google sign in' + JSON.stringify(error));
			})
		}
	};

	fbLogin = async () => {
		try {
			this.setState({ loading: true });
			const data = await SocialHelper.fbLogin();
			if (data) {
				ApiManager.fbLogin({
					user_id: data.userID,
					access_token: data.accessToken
				}).then((res) => {
					setTimeout(() => {
						this.setState({ loading: false, }, () => console.log("Loading paused"));
					}, 100);
					setTimeout(() => {
						if (res && res.success) {
							console.log("facebook login succes" + JSON.stringify(res));
							constants.user = res.data.user;
							if (this.props.navigation.state.routeName == "OtherSignUp") {
								NavigationService.navigate("MainStack", { logined: true });
							} else {
								NavigationService.navigate(constants.previousStateForLogin, { logined: true });
							}
						} else {
							Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
						}
					}, 500);
				}).catch((err) => {
					this.setState({ loading: false }, () => {
						console.log(JSON.stringify(err));
					})
				})
			}
		} catch (error) {
			this.setState({ loading: false }, () => {
				console.log('error in fb sign in' + JSON.stringify(error));
			})
		}
	}

	register = () => {
		let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (!this.state.full_name || !this.state.full_name.length) {
			Toast.showWithGravity("Please enter your full name", Toast.SHORT, Toast.BOTTOM);
		} else if (!this.state.email || !this.state.email.length) {
			Toast.showWithGravity("Please enter your email ID", Toast.SHORT, Toast.BOTTOM);
		} else if (!emailcontext.test(this.state.email)) {
			Toast.showWithGravity("Please enter valid email ID", Toast.SHORT, Toast.BOTTOM);
		} else if (!this.state.contact_number) {
			Toast.showWithGravity('Please enter contact number', Toast.LONG, Toast.CENTER);
		} else if (isNaN(this.state.contact_number) || (this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 11)) || (!this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 10))) {
			Toast.showWithGravity('Contact number is not valid', Toast.LONG, Toast.CENTER);
		} else if (!this.state.password || !this.state.password.length) {
			Toast.showWithGravity("Please enter your password", Toast.SHORT, Toast.BOTTOM);
		} else if (this.state.password.length < 6) {
			Toast.showWithGravity("Password should be minimum 6 digit long", Toast.SHORT, Toast.BOTTOM);
		} else if (!this.state.cPassword || !this.state.cPassword.length) {
			Toast.showWithGravity("Please confirm your password", Toast.SHORT, Toast.BOTTOM);
		} else if (this.state.password != this.state.cPassword) {
			Toast.showWithGravity("Password mismatch", Toast.SHORT, Toast.BOTTOM);
		} else {
			this.setState({ loading: true });
			ApiManager.register(this.state).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false, }, () => console.log("Loading paused"));
				}, 100);
				setTimeout(() => {
					if (res && res.success) {
						let body = {
							verification_token: res.data.verification_token,
							contact_number: this.state.contact_number,
							message: res.data.message ? res.data.message : "An OTP was sent to your mobile no."
						}
						if (this.props.navigation.state.routeName == "OtherSignUp") {
							NavigationService.navigate("OtherOtp", { data: body });
						} else {
							NavigationService.navigate("Otp", { data: body })
						}
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					}
				}, 500);
			}).catch((err) => {
				this.setState({ loading: false });
				console.log("signup error" + JSON.stringify(err));
			})
		}
	}

	inputFocused = (ref) => {
		this._scroll(ref, ref == "input4" ? 300 : 180);
	}

	inputBlurred = (ref) => {
		this._scroll(ref, 0);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	render() {
		console.log("Signup page");
		return (
			<KeyboardAwareView animated={true} >
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<View style={{ flex: 1 }}>
					<ImageBackground source={Images.background} style={{ flex: 1 }}>
						<View style={styles.outerSection}>
							<ScrollView showsVerticalScrollIndicator={false}
								keyboardShouldPersistTaps={'handled'}
								ref="myScrollView" keyboardDismissMode='interactive'>
								<View style={{
									flex: 1,
									alignItems: "center",
									paddingTop: 30,
									paddingBottom: 50
								}}>
									<View style={[styles.innerSection, { marginTop: "20%", minHeight: 620 }]}>
										<View style={styles.logoSection}>
											<Image source={Images.logo} style={styles.logo}></Image>
										</View>
										<Text style={styles.innerlabel}>Sign Up</Text>
										<View style={{
											flex: 1,
											alignItems: 'center',
											width: ((Dimensions.get("window").width) - 50),
										}}>
											<View style={styles.inputSection}>
												<Image source={Images.profile} style={styles.inputLogo} />
												<TextInput style={styles.input}
													value={this.state.full_name}
													placeholder='Full Name'
													placeholderTextColor="grey"
													onChangeText={(text) => { this.setState({ full_name: text }) }}
													returnKeyType="next"
													autoCapitalize="none"
													ref="input"
													onFocus={this.inputFocused.bind(this, 'input')}
												/>
											</View>
											<View style={styles.inputSection}>
												<Image source={Images.email} style={styles.inputLogo} />
												<TextInput style={styles.input}
													value={this.state.email}
													placeholder='Email Id'
													placeholderTextColor="grey"
													onChangeText={(text) => { this.setState({ email: text }) }}
													returnKeyType="next"
													autoCapitalize="none"
													ref="input1"
													onFocus={this.inputFocused.bind(this, 'input1')}
												/>
											</View>
											<View style={styles.inputSection}>
												<Image source={Images.phoneImg} style={styles.inputLogo} />
												<TextInput style={styles.input}
													value={this.state.contact_number}
													placeholder='Mobile Number'
													maxLength={11}
													placeholderTextColor="grey"
													onChangeText={(text) => { this.setState({ contact_number: text }) }}
													returnKeyType="next"
													autoCapitalize="none"
													ref="input2"
													onFocus={this.inputFocused.bind(this, 'input2')}
												/>
											</View>
											<View style={styles.inputSection}>
												<Image source={Images.password} style={styles.inputLogo} />
												<TextInput style={styles.input}
													value={this.state.password}
													placeholder='Password'
													maxLength={15}
													placeholderTextColor="grey"
													secureTextEntry={true}
													onChangeText={(text) => { this.setState({ password: text }) }}
													returnKeyType="next"
													autoCapitalize="none"
													ref="input3"
													onFocus={this.inputFocused.bind(this, 'input3')}
												/>
											</View>
											<View style={styles.inputSection}>
												<Image source={Images.password} style={styles.inputLogo} />
												<TextInput style={styles.input}
													value={this.state.cPassword}
													maxLength={15}
													placeholder='Confirm Password'
													placeholderTextColor="grey"
													secureTextEntry={true}
													onChangeText={(text) => { this.setState({ cPassword: text }) }}
													returnKeyType="next"
													autoCapitalize="none"
													ref="input4"
													onFocus={this.inputFocused.bind(this, 'input4')}
													returnKeyType="done"
													onSubmitEditing={() => this.register()}
												/>
											</View>
											<TouchableOpacity style={styles.loginButton}
												onPress={() => this.register()}>
												<Text style={[styles.loginText, { color: "white" }]}>REGISTER</Text>
											</TouchableOpacity>
											<TouchableOpacity style={styles.forgotLine}
												onPress={() => {
													console.log("Going back to login");
													NavigationService.goBack()
												}}>
												<Text style={{ color: "black", fontFamily: constants.fonts.regular }}>Already have Account?</Text>
												<Text style={{ color: "black", fontFamily: constants.fonts.bold, marginLeft: 5 }}>Log In</Text>
											</TouchableOpacity>
											<View style={styles.signUpBar}>
												<View style={styles.signUpLline}></View>
												<Text style={{ margin: 5, color: "black", fontFamily: constants.fonts.regular }}>Or Signup With</Text>
												<View style={styles.signUpLline}></View>
											</View>
											<View style={styles.socialSection}>
												<TouchableOpacity style={[styles.socialButton, { borderColor: '#475a95', marginRight: 15 }]}
													onPress={() => this.fbLogin()}>
													<Image source={Images.fbLogo} style={{ width: 15, height: 30 }}></Image>
												</TouchableOpacity>
												<TouchableOpacity style={[styles.socialButton, { borderColor: '#c02c2c' }]}
													onPress={() => this.googleSignIn()}>
													<Image source={Images.googleLogo} style={{ width: 25, height: 25 }}></Image>
												</TouchableOpacity>
											</View>
										</View>
									</View>
								</View>
							</ScrollView>
						</View>
					</ImageBackground>
				</View>
			</KeyboardAwareView>
		)
	}
}