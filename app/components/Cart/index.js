import React, { Component } from 'react';
import { findNodeHandle, View, Text, Image, Dimensions, Alert, TextInput, TouchableOpacity, StyleSheet, Animated, TouchableHighlight, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Images from '../../global/images';
import CardView from 'react-native-cardview';
import constants from '../../constants/constants';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import * as ApiManager from '../../apimanager/ApiManager';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import NotificationSetting from 'react-native-open-notification';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class CartScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor() {
		super();
		this.state = {
			selectedIndex: 0,
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			// products: JSON.parse(JSON.stringify(this.props.getCart)),
			products: [],
			loading: false,
			totalAmount: 0,
			grandTotal: 0,
			finalAmount: 0,
			couponApplied: false,
			discount: 0,
			logined: false,
			delivery_charges: 0,
			couponCode: constants.coupon.code ? constants.coupon.code : "",
			taxes: constants.taxes,
			store: constants.cartStore,
			reorder: false
		};
	}

	componentWillMount = async () => {
		let reorder = this.props.navigation.getParam("reorder", false);
		if (reorder) {
			this.setState({ reorder: true });
		}
		let { cart } = this.props;
		this.setState({ products: cart }, () => {
			console.log(this.state.products);
		})
		await AsyncStorage.removeItem("fcmToken");
	}

	updateFcmToken = (token) => {
		this.setState({ loading: true });
		ApiManager.updateFcmToken(token).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(async () => {
				if (res && res.success && res.code == 200) {
					this.proceed();
				} else {
					Toast.showWithGravity(res.singleStringMessage ? res.singleStringMessage : "Undefined error", Toast.LONG, Toast.CENTER);
					this.proceed();
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				console.log(JSON.stringify(err));
				this.proceed();
			}, 500);
		})
	}

	alertNotification = () => {
		Alert.alert(
			'',
			'App requires permission of notification to send you updates of order and status. Do you want to continue?',
			[
				{
					text: 'OK',
					onPress: () => {
						NotificationSetting.open();
					}
				},
				{
					text: 'CANCEL',
					onPress: () => {
						Toast.showWithGravity("You will not receive notifications for any orders", Toast.SHORT, Toast.BOTTOM);
						this.proceed();
					}
				}
			],
			{ cancelable: false },
		);
	}

	checkToken = async () => {
		const fcmToken = await AsyncStorage.getItem("fcmToken");
		if (fcmToken) {
			constants.FCM_TOKEN = fcmToken;
			// this.proceed();
			this.updateFcmToken(fcmToken);
		} else {
			this.checkPermission();
		}
	}

	//1
	checkPermission = () => {
		firebase.messaging().hasPermission().then(enabled => {
			if (enabled) {
				this.getToken();
			} else {
				this.requestPermission();
			}
		});
	}

	//3
	getToken = () => {
		firebase.messaging().getToken().then(async (fcmToken) => {
			if (fcmToken) {
				// user has a device token
				console.log("fcm id is :--" + fcmToken);
				constants.FCM_TOKEN = fcmToken;
				await AsyncStorage.setItem('fcmToken', fcmToken);
				// this.proceed(fcmToken);
				this.updateFcmToken(fcmToken);
				// this.checkLoginCase();
			}
		}).catch(err => {
			console.log("token error")
			throw new Error(err)
		})
	}

	//2
	requestPermission = () => {
		firebase.messaging().requestPermission()
			.then(() => {
				this.getToken();
			})
			.catch(error => {
				// NavigationService.navigate('Login');
				console.log("User has rejected permission");
				this.alertNotification();
			});
	}

	calculateCartItem = () => {
		const charges = constants.delivery_charges;
		let { products, taxes, discount, couponApplied } = this.state;
		this.setState({ loading: false }, () => {
			let totalAmount = 0;
			let grandTotal = 0;
			let delivery_charges = 0;
			let isValid = false;
			for (let i = 0; i < products.length; i++) {
				totalAmount += (products[i].count * products[i].price.sale_price)
			}
			for (let i = 0; i < charges.length; i++) {
				if (totalAmount <= charges[i].order_amount) {
					delivery_charges = charges[i].charges;
					break;
				}
			}
			let totalTax = 0;
			for (let i = 0; i < taxes.length; i++) {
				taxes[i].value = totalAmount * taxes[i].percentage / 100;
				totalTax += taxes[i].value;
			}
			grandTotal = totalAmount + totalTax;
			let finalAmount = (totalAmount - discount) + totalTax;
			this.setState({
				delivery_charges,
				totalAmount, grandTotal,
				finalAmount
			}, () => {
				if (discount >= totalAmount) {
					this.setState({ couponCode: "", couponApplied: false, discount: 0 }, () => {
						console.log("coupon code applied " + couponApplied)
					});
					AsyncStorage.removeItem("coupon");
					constants.coupon = {}
				} else if (discount < totalAmount && couponApplied) {
					this.setState({ couponApplied: true }, () => {
						console.log("coupon code applied " + couponApplied)
					});
				} else {
					this.setState({ couponApplied: false, couponCode: "" }, () => {
						console.log("coupon code applied " + couponApplied)
						AsyncStorage.removeItem("coupon");
						constants.coupon = {}
					});
				}
			})
		});
	}

	addProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				counter++;
				stateProducts[index].count++;
				cart[i].count = stateProducts[index].count;
				break;
			}
		}
		if (counter == 0) {
			stateProducts[index].count = 1;
			cart.push(stateProducts[index]);
		}
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts
			}, () => {
				console.log("update featured products");
				if (this.state.couponApplied) {
					this.calculateDiscount();
				} else this.calculateCartItem();
			})
		});
	}

	removeProduct = (index) => {
		let stateProducts = this.state.products;
		product = stateProducts[index];
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id == product._id) {
				if (stateProducts[index].count && stateProducts[index].count > 0) {
					stateProducts[index].count--;
				}
				cart[i].count = stateProducts[index].count;
				if (cart[i].count == 0) cart.splice(i, 1);
				break;
			}
			if (!cart[i].count || cart[i].count < 1) {
				cart.splice(i, 1);
			}
		}
		stateProducts = cart;
		this.props.setCart(cart);
		AsyncStorage.setItem("cart", JSON.stringify(cart)).then(() => {
			this.setState({
				products: stateProducts
			}, () => {
				console.log("update featured products");
				if (this.state.couponApplied) {
					this.calculateDiscount();
				} else this.calculateCartItem();
			})
		});
	}

	proceed = () => {
		if (this.state.totalAmount == 0) {
			if (constants.previousStateForCart.length) {
				NavigationService.navigate(constants.previousStateForCart);
			} else {
				NavigationService.goBack();
			}
		} else if (this.state.couponApplied && (this.state.totalAmount <= constants.coupon.min_order_amount)) {
			Toast.showWithGravity("Minimum order amount should be " + constants.coupon.min_order_amount + " for this coupon " + constants.coupon.code, Toast.SHORT, Toast.BOTTOM);
		} else {
			if (this.state.logined) {
				constants.previousStateForSelectAddress = this.props.navigation.state.routeName;
				if (this.props.navigation.state.routeName == "Cart") {
					NavigationService.navigate("SelectAddressInHome");
				} else if (this.props.navigation.state.routeName == "CategoryCart") {
					NavigationService.navigate("SelectAddress");
				} else if (this.props.navigation.state.routeName == "SearchCart") {
					NavigationService.navigate("SelectAddressInSearch");
				} else NavigationService.navigate("SelectAddressInStore");
			} else {
				constants.previousStateForLogin = this.props.navigation.state.routeName;
				NavigationService.navigate("OtherLoginStack");
			}
		}
	}

	calculateDiscount = () => {
		const coupon = constants.coupon;
		AsyncStorage.getItem("cart").then((data) => {
			let total = 0;
			let cart = JSON.parse(data);
			let discount = 0;
			let isValid = true;
			for (let i = 0; i < cart.length; i++) {
				total += (cart[i].price.sale_price * cart[i].count);
			}
			if (coupon.value) {
				if (coupon.type == 1) {
					discount = coupon.value
				} else {
					discount = total * coupon.value / 100;
				}
				if (discount >= total) {
					constants.coupon = {};
					Toast.showWithGravity("Invalid Coupon Code Was Applied", Toast.SHORT, Toast.CENTER);
					discount = 0;
					isValid = false;
					this.setState({ couponCode: "" });
					AsyncStorage.removeItem("coupon");
				}
			} else {
				discount = 0;
				isValid = false;
			};

			this.setState({
				couponApplied: isValid,
				discount: discount,
				couponCode: constants.coupon.code
			}, () => {
				this.calculateCartItem();
				console.log("coupon applied " + this.state.couponApplied)
			})
		});
	}

	applyCouponCode = () => {
		this.setState({ loading: true });
		ApiManager.applyCoupon(this.state.couponCode, this.state.totalAmount).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(async () => {
				if (res && res.success && res.code == 200) {
					if (res.data.coupon.status == 1) {
						this.calculateDiscount();
					} else {
						Toast.showWithGravity("Coupon Expired", Toast.SHORT, Toast.CENTER);
					}
				} else {
					Toast.showWithGravity(res.singleStringMessage ? res.singleStringMessage : "Undefined error", Toast.LONG, Toast.CENTER);
					await AsyncStorage.removeItem("coupon");
					this.setState({ couponCode: "", couponApplied: false, discount: 0 }, () => {
						constants.coupon = {};
						this.calculateCartItem();
					})
				}
			}, 500);
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				Alert.alert(JSON.stringify(err));
			}, 500);
		})
	}

	renderDiscount = () => {
		return (
			<View style={{ width: "100%" }}>
				<View style={{
					backgroundColor: constants.lightgreen,
					width: "100%",
					height: 45,
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
				}}>
					<Text style={{
						fontSize: 15,
						color: "black", fontFamily: constants.fonts.semiBold,
						color: constants.green,
						marginLeft: 10
					}}> Coupon Applied:{" " + this.state.couponCode}
					</Text>
					<TouchableOpacity style={{
						width: 25,
						height: 25,
						backgroundColor: "lightgrey",
						borderRadius: 25,
						justifyContent: "center",
						alignItems: "center",
						marginRight: 10
					}} onPress={() => {
						AsyncStorage.removeItem("coupon").then(() => {
							this.setState({
								couponApplied: false,
								discount: 0,
								couponCode: ""
							}, () => {
								constants.coupon = {};
								this.calculateCartItem();
							})
						})
					}}>
						<Image source={Images.closeImg} style={{ width: 12, height: 12 }}></Image>
					</TouchableOpacity>
				</View>
				<View style={{
					width: "100%",
					height: 45,
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
				}}>
					<Text style={{
						fontSize: 15,
						color: "black", fontFamily: constants.fonts.medium,
						color: "black",
						marginLeft: 10
					}}>You Save</Text>
					<Text style={{
						fontSize: 15,
						color: "black", fontFamily: constants.fonts.medium,
						color: "black",
						marginRight: 10
					}}>- {this.state.discount} {constants.currency}</Text>
				</View>
			</View>
		)
	}

	renderCouponInbox = () => {
		return (
			<View style={{
				width: "100%",
				height: 65,
				flexDirection: "row",
				justifyContent: "space-between",
				alignItems: "center",
				borderTopWidth: 2,
				borderTopColor: constants.backgroundPageColor
			}}>
				<View style={{
					width: "35%",
				}}>
					<Text style={{
						fontSize: 13, marginLeft: 10,
						color: "black", fontFamily: constants.fonts.medium, color: "black"
					}}>Coupon Code</Text>
				</View>
				<View style={{
					width: "65%",
					flexDirection: "row",
					justifyContent: "space-between",
				}}>
					<View style={{
						width: "57%",
						height: 40,
						minWidth: 75,
						borderColor: "lightgrey",
						borderWidth: 1,
						borderRadius: 35,
						alignItems: "center",
						justifyContent: "center",
						paddingLeft: 10,
					}}>
						<TextInput style={{ width: "100%", height: "100%" }}
							value={this.state.couponCode}
							onChangeText={(text) => { this.setState({ couponCode: text }) }}
							returnKeyType="done"
							onSubmitEditing={text => {
								if (this.state.couponCode.length > 0) {
									this.applyCouponCode();
								} else {
									Toast.showWithGravity("Please enter the coupon Code", Toast.SHORT, Toast.CENTER);
								}
							}}></TextInput>
					</View>
					<TouchableOpacity style={{
						width: "33%",
						minWidth: 55,
						height: 40,
						backgroundColor: constants.green,
						borderRadius: 25,
						justifyContent: "center",
						alignItems: "center",
						marginRight: 10
					}} onPress={() => {
						if (this.state.couponCode.length > 0) {
							this.applyCouponCode();
						} else {
							Toast.showWithGravity("Please enter the coupon Code", Toast.SHORT, Toast.CENTER);
						}
					}}>
						<Text style={{ fontSize: 14, color: "black", fontFamily: constants.fonts.bold, color: "white" }}>APPLY</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	inputFocused = (ref) => {
		this._scroll(ref, 180);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	renderCartPage = () => {
		let { products, taxes,
			couponApplied, finalAmount,
			delivery_charges, logined } = this.state;
		return (
			<KeyboardAwareView animated={true}>
				<View style={{
					...Platform.select({
						ios: { marginTop: (getStatusBarHeight(true) + 13), },
						android: { marginTop: 48 }
					}),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-start',
					backgroundColor: constants.backgroundPageColor,
					marginLeft: 15,
					marginRight: 15
				}}>
					<ScrollView style={{ width: "100%" }}
						showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}>
						< CardView cardElevation={2}
							cardMaxElevation={2}
							cornerRadius={5}
							style={{
								height: "auto",
								width: "100%",
								backgroundColor: "white",
								borderBottomWidth: 1,
								borderColor: constants.backgroundPageColor,
								marginBottom: 25,
								marginTop: 15,
								justifyContent: "flex-start",
								alignItems: "center"
							}}>
							<View style={{
								width: "100%",
								height: 40,
								backgroundColor: constants.lightgreen,
								flexDirection: "row",
								alignItems: "center",
								justifyContent: "space-between",
								paddingLeft: 5,
								paddingRight: 5,
							}}>
								<View style={{ width: "40%" }}>
									<Text style={{
										color: "black", marginLeft: 5,
										fontSize: 12, color: "black", fontFamily: constants.fonts.medium,
									}}>ITEM</Text>
								</View>
								<View style={{ width: "18%", alignItems: "center" }}>
									<Text style={{
										color: "black",
										fontSize: 12, color: "black", fontFamily: constants.fonts.medium,
										// textAlign: "center"
									}}>SIZE</Text>
								</View>
								<View style={{ width: "27%", alignItems: "center" }}>
									<Text numberOfLines={1} style={{
										color: "black",
										fontSize: 12, color: "black", fontFamily: constants.fonts.medium,
										textAlign: "left"
									}}>QUANTITY</Text>
								</View>
								<View style={{ width: "15%" }}>
									<Text style={{
										color: "black",
										fontSize: 12, color: "black", fontFamily: constants.fonts.medium,
										textAlign: "right"
									}}>PRICE</Text>
								</View>
							</View>
							{
								products.map((product, index) => (
									<View style={{
										width: "100%",
										minHeight: 60,
										flexDirection: "row",
										alignItems: "center",
										justifyContent: "space-between",
										paddingLeft: 5,
										paddingRight: 5,
										borderTopWidth: index != 0 ? 3 : 0,
										borderTopColor: index != 0 ? constants.backgroundPageColor : "transparent"
									}} key={index}>
										<View style={{ width: "40%", flexDirection: "row", maxHeight: 70 }}>
											<View style={{ width: "40%", padding: 5, }}>
												<Image source={{ uri: constants.imageBaseURL + product.pictures[0] }} style={{ height: 60, width: "100%", resizeMode: "contain" }}></Image>
											</View>
											<View style={{
												width: "60%",
												justifyContent: "center",
												paddingLeft: 2,
												paddingRight: 2,
												flexShrink: 1
											}}>
												<Text numberOfLines={1} style={{
													fontSize: 11,
													color: "black", fontFamily: constants.fonts.medium,
												}}>{product.name}</Text>
												<Text numberOfLines={1} style={{
													fontSize: 11,
													color: "black", fontFamily: constants.fonts.medium,
													marginTop: 5
												}}>{product.price.sale_price}/Unit </Text>
											</View>
										</View>
										<View style={{ width: "18%", alignItems: "center" }}>
											<Text style={{
												color: "black",
												fontSize: 11,
												fontFamily: constants.fonts.medium,
												textAlign: "center"
											}}>{product.size}</Text>
										</View>
										<View style={{ width: "27%", alignItems: "center", justifyContent: "center" }}>
											<View style={styles.productCounter}>
												<TouchableOpacity style={styles.counterButton} onPress={() => this.removeProduct(index)}>
													<Text style={{ fontSize: 25, color: "white", marginTop: -7 }}>-</Text>
												</TouchableOpacity>
												<Text numberOfLines={1} style={{
													fontSize: 13,
													color: "black", fontWeight: "400"
												}}>{product.count}</Text>
												<TouchableOpacity style={styles.counterButton}
													onPress={() => {
														if (product.count >= product.order_max) {
															Toast.showWithGravity("Your maxmimum limit has been reached for this product . . .", Toast.SHORT, Toast.CENTER);
														} else if (product.count >= product.stock_quantity) {
															Toast.showWithGravity("Product unavailable now . . .", Toast.SHORT, Toast.CENTER);
														} else this.addProduct(index)
													}}>
													<Text style={{ fontSize: 20, color: "white", marginTop: -4 }}>+</Text>
												</TouchableOpacity>
											</View>
										</View>
										<View style={{ width: "15%" }}>
											<Text numberOfLines={2} style={{
												color: constants.green,
												fontSize: 11, fontFamily: constants.fonts.medium,
												textAlign: "right"
											}}>{product.price.sale_price * product.count} {constants.currency}</Text>
										</View>
									</View>
								))
							}
						</ CardView>
						< CardView cardElevation={2}
							cardMaxElevation={2}
							cornerRadius={5}
							style={{
								height: "auto",
								width: "100%",
								backgroundColor: "white",
								borderBottomWidth: 3,
								borderColor: constants.backgroundPageColor,
								marginBottom: 25,
								justifyContent: "flex-start",
								alignItems: "center"
							}}>
							<View style={{
								width: "100%",
								height: 40,
								backgroundColor: constants.lightgreen,
								justifyContent: "center"
							}}>
								<Text style={{
									color: "black",
									fontSize: 14, color: "black", fontFamily: constants.fonts.medium,
									marginLeft: 10,
								}}>Payment Summary</Text>
							</View>
							<View style={{
								width: "100%",
								height: "auto",
								justifyContent: "space-around",
								alignItems: "center",
								borderTopWidth: 2,
								paddingTop: 5,
								paddingBottom: 5,
								borderTopColor: constants.backgroundPageColor
							}}>
								<View style={{
									width: "100%",
									height: 40,
									flexDirection: "row",
									justifyContent: "space-between",
									alignItems: "center"
								}}>
									<Text style={{
										fontSize: 13, marginLeft: 10,
										fontFamily: constants.fonts.medium, color: "black"
									}}>Total Price (Inc. Tax)</Text>
									<Text style={{
										fontSize: 15,
										fontFamily: constants.fonts.semiBold,
										color: "black",
										marginRight: 10
									}}>{this.state.totalAmount} {constants.currency}</Text>
								</View>
								{
									taxes.map((tax, index) => (
										<View style={{
											width: "100%",
											height: 40,
											flexDirection: "row",
											justifyContent: "space-between",
											alignItems: "center"
										}} key={index}>
											<Text style={{
												fontSize: 13, marginLeft: 10,
												fontFamily: constants.fonts.medium, color: "black"
											}}>{tax.name} ({tax.percentage})</Text>
											<Text style={{
												fontSize: 13,
												fontFamily: constants.fonts.semiBold,
												color: "black",
												marginRight: 10
											}}>{tax.value} {constants.currency}</Text>
										</View>
									))
								}
								<View style={{
									width: "100%",
									height: 35,
									flexDirection: "row",
									justifyContent: "space-between",
									alignItems: "center"
								}} >
									<Text style={{
										fontSize: 13, marginLeft: 10,
										fontFamily: constants.fonts.medium, color: "black"
									}}>Delivery Charge</Text>
									{
										delivery_charges ?
											<Text style={{
												fontSize: 13,
												fontFamily: constants.fonts.semiBold,
												color: "black",
												marginRight: 10
											}}>{this.state.delivery_charges} {constants.currency}</Text> :
											<Text style={{
												fontSize: 13,
												fontFamily: constants.fonts.semiBold,
												color: "black",
												marginRight: 10
											}}>FREE</Text>
									}
								</View>
							</View>
							<View style={{
								width: "100%",
								height: 45,
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								borderTopWidth: 2,
								borderTopColor: constants.backgroundPageColor
							}}>
								<Text style={{
									fontSize: 15,
									fontFamily: constants.fonts.semiBold,
									color: "black",
									marginLeft: 10
								}}>Grand Total</Text>
								<Text style={{
									fontSize: 15,
									fontFamily: constants.fonts.semiBold,
									color: "black",
									marginRight: 10
								}}>{this.state.grandTotal + this.state.delivery_charges} {constants.currency}</Text>
							</View>
							{
								couponApplied ?
									this.renderDiscount() :
									this.renderCouponInbox()
							}
							<View style={{
								width: "100%",
								height: 50,
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								borderTopWidth: 2,
								borderTopColor: constants.backgroundPageColor
							}}>
								<Text style={{
									fontSize: 15,
									color: "black", fontFamily: constants.fonts.semiBold,
									color: "black",
									marginLeft: 10
								}}>Final Bill Amount</Text>
								<Text style={{
									fontSize: 15,
									color: "black", fontFamily: constants.fonts.semiBold,
									color: "black",
									marginRight: 10
								}}>{finalAmount + delivery_charges} {constants.currency}</Text>
							</View>
						</CardView>
						<View style={{
							width: "100%",
							flexDirection: "row",
							justifyContent: "space-between",
							marginBottom: 50
						}}>
							<TouchableOpacity style={{
								borderRadius: 30,
								borderWidth: 1,
								borderColor: constants.green,
								padding: 10,
								paddingLeft: 15,
								paddingRight: 15,
							}} onPress={() => NavigationService.navigate("Home")}>
								<Text style={{
									color: constants.green,
									fontSize: 13,
									fontFamily: constants.fonts.regular
								}}>ADD MORE ITEMS</Text>
							</TouchableOpacity>
							<TouchableOpacity style={{
								borderWidth: 1,
								borderColor: constants.green,
								borderRadius: 30,
								padding: 10,
								paddingLeft: 15,
								paddingRight: 15,
							}} onPress={async () => {
								await AsyncStorage.removeItem("coupon");
								await AsyncStorage.removeItem("cart");
								this.setState({
									products: []
								}, () => {
									this.props.setCart([]);
									this.calculateCartItem();
								})
							}}>
								<Text style={{
									color: constants.green,
									fontSize: 13,
									fontFamily: constants.fonts.regular
								}}>CLEAR CART</Text>
							</TouchableOpacity>
						</View>
					</ScrollView>
					<View style={{
						width: "100%",
						height: 85,
						justifyContent: "center",
						alignItems: "center",
						marginBottom: 5
					}}>
						<TouchableOpacity style={{
							backgroundColor: constants.green,
							height: 50,
							width: 240,
							borderRadius: 30,
							justifyContent: "center",
							alignItems: "center"
						}} onPress={() => {
							if (logined) {
								this.checkToken();
							} else {
								this.proceed()
							}
						}}>
							<Text style={{
								color: "white",
								fontSize: 16,
								fontFamily: constants.fonts.bold
							}}>PROCEED TO CHECKOUT</Text>
						</TouchableOpacity>
					</View>
				</View>
			</KeyboardAwareView>
		)
	}

	render() {
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<NavigationEvents onWillFocus={async () => {
					if (constants.user._id) {
						this.setState({ logined: true });
					}
					if (constants.coupon._id) {
						AsyncStorage.getItem("cart").then((data) => {
							let products = data ? JSON.parse(data) : [];
							let total = 0;
							for (let i = 0; i < products.length; i++) {
								total += (products[i].count * products[i].price.sale_price)
							}
							this.setState({ totalAmount: total, couponCode: constants.coupon.code }, () => this.applyCouponCode())
						})
					}
					this.calculateCartItem();
					//Call whatever logic or dispatch redux actions and update the screen!
				}} />
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.View
					style={{ flex: 1 }}>
					{this.renderCartPage()}
				</Animated.View>
				<Animated.View style={{ top: 0, left: 0, right: 0, position: 'absolute', flexDirection: 'row', ...Platform.select({ ios: { height: (getStatusBarHeight(true) + 40), }, android: { height: (getStatusBarHeight(true) + 40), } }), backgroundColor: '#1faf4b' }}>
					<Animated.View style={{
						height: '100%',
						width: '100%', marginLeft: 10,
						alignItems: 'center', flexDirection: 'row'
					}}>
						<TouchableHighlight style={{
							height: 35,
							width: 35,
							...Platform.select({
								ios: { marginTop: 45 },
								android: { marginTop: 14 }
							})
						}} underlayColor='transparent'
							onPress={() => {
								if (constants.previousStateForCart.length) {
									if (constants.previousStateForCart == "AllProducts") {
										NavigationService.navigate("Home");
									} else {
										NavigationService.navigate(constants.previousStateForCart);
									}
								} else {
									NavigationService.goBack();
								}
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
						<View style={{
							height: '100%',
							marginLeft: 5,
							...Platform.select({
								ios: { marginTop: 30 },
								android: { marginTop: 0 }
							}),
							justifyContent: 'center',
							alignItems: "center"
						}}>
							<Text style={{
								fontSize: 18, fontFamily: constants.fonts.bold,
								color: 'white',
							}}>Cart - {this.state.store.name}</Text>
						</View>
					</Animated.View>
					{/* <Animated.View style={{
						height: '100%',
						marginLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: '60%',
						alignItems: "center"
					}}>
						
					</Animated.View> */}
				</Animated.View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	productCounter: {
		borderColor: constants.green,
		borderWidth: 1,
		height: 20,
		alignItems: "center",
		width: "100%",
		minWidth: 50,
		justifyContent: "space-between",
		borderRadius: 30,
		flexDirection: "row",
		borderLeftWidth: 0,
		borderRightWidth: 0,
	},
	counterButton: {
		height: 20, width: 20,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1,
		backgroundColor: constants.green,
		borderColor: constants.green,
		borderRadius: 30
	}
})

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen)
