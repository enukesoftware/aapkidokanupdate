import React, { Component } from 'react';
import { Platform, View, Dimensions, TouchableOpacity, Image, Text, SafeAreaView, PermissionsAndroid } from 'react-native';
import styles from './styles';
import { TextBold, TextRegular } from '../../global/text';
import Images from '../../global/images';
import Constants from '../../constants/constants';
import Geocoder from 'react-native-geocoding';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import NavigationService from '../../global/NavigationService';
import constants from '../../constants/constants';
import Toast from 'react-native-simple-toast';
import Geolocation from '@react-native-community/geolocation';
// import Geolocation. from 'react-native-geolocation.-service';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
var isFromAutoComplete = false

export default class SearchAddress extends Component {

	constructor() {
		super()
	}


	static navigationOptions = {
		title: "Search Address",
		headerStyle: styles.defaultHeaderStyle,
		headerTitleStyle: styles.defaultHeaderTitleStyle,
		headerTintColor: 'white',
	};

	state = {
		region: {
			latitudeDelta: Platform.OS === 'android' ? 0.5 * (screenWidth / screenHeight) : 0,
			longitudeDelta: Platform.OS === 'android' ? 0.5 * (screenWidth / screenHeight) : 0,
			latitude: constants.defaultRegion.latitude,
			longitude: constants.defaultRegion.longitude
		},
		address: '',
		addressForAutoSearch: "Search Address",
		city: '',
		isShowMarker: false,
	}

	onRegionChange = region => {
		this.closeMarkerInfo()
	}

	onRegionChangeComplete = region => {
		if (!isFromAutoComplete) {
			this.setState({
				region: region
			})
		}
		this.getAddressFromLocation(region)
	}

	componentDidMount = () => {
		Geocoder.init(Constants.GOOGLE_API_KEY);
		let params = this.props.navigation.state.params;
		if (params && params.latitude && params.longitude) {
			let region = {
				latitude: params.latitude,
				longitude: params.longitude,
				latitudeDelta: Platform.OS === "android" ? 0.5 * (screenWidth / screenHeight) : 0,
				longitudeDelta: Platform.OS === "android" ? 0.5 * (screenWidth / screenHeight) : 0,
			}
			this.onSelect({
				address: params.gps_address,
				addressForAutoSearch: params.gps_address,
				region: region
			})
		} else {
			if (Platform.OS === 'android') {
				this.requestPermission();
			} else this.myLocation();
		}
		// this.getAddressFromLocation(params);

		console.log("SEARCH_ADDRESS_STATE:componentDidMount:", JSON.stringify(this.props))
		// this.getAddressFromLocation();
	}

	componentWillUnmount() {
		// navigator.geolocation.clearWatch(this.watchID);
		Geolocation.clearWatch(this.watchID);
	}

	getAddressFromLocation = (region) => {
		if (!region.latitude || !region.longitude) {
			return false
		}
		Geocoder.getFromLatLng(region.latitude, region.longitude, 1).then(json => {
			// console.log("ADDRESS:",JSON.stringify(json))

			var addressComponent = json.results[0].address_components;
			// console.log("\nADD:", JSON.stringify(addressComponent))

			this.getCityFromAddress(addressComponent);

			this.showMarkerInfo()

			if (!isFromAutoComplete) {
				var formattedAddress = json.results[0].formatted_address;
				this.setState({
					address: formattedAddress,
					addressForAutoSearch: formattedAddress
				})
			} else {
				isFromAutoComplete = !isFromAutoComplete
			}
			// console.warn(addressComponent)
			// console.log(addressComponent);
		}).catch(error =>
			console.warn(error),
			// this.setState({ address: '', addressForAutoSearch: strings.search_location,city:'' })
		);
	}

	getCityFromAddress = (addressComponent) => {
		for (let i = 0; i < addressComponent.length - 1; i++) {
			let locality = addressComponent[i]
			let types = locality.types
			for (let j = 0; j < types.length - 1; j++) {
				if (types[j] === 'locality') {
					// console.log("LOCALITY:", locality.long_name)
					this.setState({
						city: locality.long_name
					})
				}
			}
		}
	}

	requestPermission = async () => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
				{
					'title': 'Location Permission',
					'message': 'App needs access to your location '
				}
			)
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				// Toast.showWithGravity("Location Permission Granted.", Toast.SHORT, Toast.BOTTOM);
				console.log("Location Permission Granted.");
				this.myLocation();
			} else {
				// Toast.showWithGravity("Location Permission Not Granted", Toast.SHORT, Toast.BOTTOM);
				console.log("Location Permission Not Granted:--" + granted);
			}
		} catch (err) {
			console.warn(err)
			// Toast.showWithGravity("Location Permission Error", Toast.SHORT, Toast.BOTTOM);
			console.log("Location Permission Error");
		}
	}

	myLocation = () => {
		this.watchID = Geolocation.getCurrentPosition(
			(position) => {
				// Create the object to update this.state.mapRegion through the onRegionChange function
				let region = {
					latitude: position.coords.latitude,
					longitude: position.coords.longitude,
					latitudeDelta: Platform.OS === "android" ? 0.5 * (screenWidth / screenHeight) : 0,
					longitudeDelta: Platform.OS === "android" ? 0.5 * (screenWidth / screenHeight) : 0,
				}
				// this.onRegionChange(region, region.latitude, region.longitude);
				this.setState({
					region
				});
				this.getAddressFromLocation(position.coords.latitude, position.coords.longitude)
				this.updatePinLocationOnMap(position.coords.latitude, position.coords.longitude)
				// alert(JSON.stringify(position.coords.latitude+" " +position.coords.longitude))
				// this.mapView.animateToRegion(region, 100);
			},
			(error) => {
				console.log(error.message);
				alert(error.message);
			},
			// Constants.geoOptions
		);
	}

	//function to be pass to next screen and get result
	onSelect = data => {
		isFromAutoComplete = true;
		this.updatePinLocationOnMap(data.region.latitude, data.region.longitude)

		this.setState(prevState => ({
			region: {
				...prevState.region,
				latitude: data.region.latitude,
				longitude: data.region.longitude
			},
			address: data.address,
			addressForAutoSearch: data.addressForAutoSearch
		}), () => {
			// isFromAutoComplete = false
		});

	};

	searchAddress = () => {
		NavigationService.navigate("AddressAutocomplete", { onSelect: this.onSelect });
	}

	updatePinLocationOnMap = (latitude, longitude) => {
		// console.log("LOCATION-" + latitude + ' - ' + longitude)
		let camera = {
			center: {
				latitude: latitude,
				longitude: longitude,
			},
			zoom: 16,
		}
		this.mapView.animateCamera(camera, 500)
	}

	onAccept = () => {
		if (this.state.region.latitude && this.state.region.longitude) {
			let data = {
				address: this.state.address,
				latitude: this.state.region.latitude,
				longitude: this.state.region.longitude
			}
			console.log(data);
			// this.props.locationSelected()
			NavigationService.goBack();
			this.props.navigation.state.params.addressFetched(data);
		}
	}

	closeMarkerInfo = () => {
		this.setState({
			isShowMarker: false
		})
	}

	showMarkerInfo = () => {
		this.setState({
			isShowMarker: true
		})
	}

	renderMarkerInfo = () => {
		if (this.state.isShowMarker) {
			return (
				<View style={{ alignItems: 'center' }}>
					<View
						style={styles.markerInfoView}>
						<TouchableOpacity style={styles.markerClose} onPress={() => this.closeMarkerInfo()}>
							<Image source={Images.ic_cross}
								style={styles.markerCloseImage}></Image>
						</TouchableOpacity>

						<Text style={styles.markerText}>{this.state.address}</Text>
					</View>

					<Image source={Images.ic_location_arrow}
						style={styles.markerBottomArrow}></Image>
				</View>

			)
		}
	}

	render() {
		const { region, addressForAutoSearch } = this.state
		console.log("regiosns are :-" + JSON.stringify(region))
		return (

			<View style={styles.container}>
				{/* --------------- Map View --------------- */}
				<MapView
					ref={(mapView) => {
						this.mapView = mapView;
					}}
					style={styles.map}
					provider={PROVIDER_GOOGLE}
					initialRegion={region}
					onRegionChangeComplete={this.onRegionChangeComplete}
					onRegionChange={this.onRegionChange}
					zoomEnabled={true}
					zoomControlEnabled={true}
				/>
				{/* --------------- AutoComplete Address View --------------- */}
				<TouchableOpacity style={styles.autoCompleteTouchable}
					onPress={() => NavigationService.navigate('AddressAutocomplete', { onSelect: this.onSelect })}>
					<TextBold title={addressForAutoSearch} numberOfLines={1} ellipsizeMode='tail'
						textStyle={styles.autoCompleteText} />
					<View style={{ width: '10%' }}>
						<Image source={Images.ic_search} style={{ width: 30, height: 30 }}></Image>
					</View>
				</TouchableOpacity>


				{/*--------------- maker view  ---------------*/}
				<View style={styles.markerOuterView}>
					{this.renderMarkerInfo()}
					<Image style={styles.marker} source={Images.ic_marker_pin} />
				</View>

				{/* --------------- MyLocation and Accept Button View --------------- */}
				<SafeAreaView style={styles.bottomSafeStyle}>
					<TouchableOpacity style={styles.myLocation} onPress={() => {
						if (Platform.OS === 'android') {
							this.requestPermission();
						} else this.myLocation()
					}}>
						<Image source={Images.ic_current_location} style={{ width: 28, height: 28 }}></Image>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => this.onAccept()}
						style={{
							width: '100%',
							backgroundColor: Constants.green,
							alignItems: 'center'
						}}>
						<TextRegular title={"Accept"} textStyle={styles.acceptText} />
					</TouchableOpacity>
				</SafeAreaView>
			</View>
		);
	};
}