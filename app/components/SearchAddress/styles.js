import { StyleSheet, Platform, Dimensions } from 'react-native';
import Constants from '../../constants/constants'
import constants from '../../constants/constants';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default StyleSheet.create({
	defaultHeaderStyle: {
		backgroundColor: Constants.green,
		paddingTop: 0,
	},
	defaultHeaderTitleStyle: {
		color: 'white',
		fontFamily: constants.fonts.regular,
		fontSize: Platform.OS === 'android' ? 16 : 20,
		fontWeight: '200',
	},
	safeStyle: {
		flex: 1,
		backgroundColor: 'transparent',
		alignItems: 'center',
		height: screenHeight,
		width: screenWidth,
	},
	container: {
		flex: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		height: screenHeight,
		width: screenWidth,
	},
	map: {
		// ...StyleSheet.absoluteFillObject,
		position: 'absolute',
		top: 0,
		left: 0,
		width: screenWidth,
		height: screenHeight,
	},
	markerFixed: {
		left: '50%',
		marginLeft: -24,
		marginTop: -48,
		position: 'absolute',
		bottom: '50%'
	},
	marker: {
		height: 48,
		width: 48
	},
	footer: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		bottom: 0,
		position: 'absolute',
		width: screenWidth
	},
	region: {
		color: '#fff',
		lineHeight: 20,
		margin: 20
	},
	myLocation: {
		width: 50,
		height: 50,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'flex-end',
		margin: 20,
		borderRadius: 25,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.2,
		shadowRadius: 2,
		elevation: 5,
	},
	autoCompleteTouchable: {
		flexDirection: 'row',
		position: 'absolute',
		alignItems: 'center',
		width: '94%',
		height: 60,
		top: 10,
		borderRadius: 0,
		backgroundColor: 'white',
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.2,
		shadowRadius: 2,
		elevation: 5,
	},
	autoCompleteText: {
		fontSize: 18,
		textAlign: 'left',
		paddingHorizontal: 10,
		width: '88%'
	},
	markerOuterView: {
		width: '60%',
		position: 'absolute',
		alignItems: 'center',
		bottom: '50%'
	},
	markerInfoView: {
		width: screenWidth / 2,
		backgroundColor: '#FFFFFF',
		alignItems: 'center',
		borderRadius: 5,
		opacity: 1
	},
	markerClose: {
		width: 30,
		height: 30,
		alignSelf: 'flex-end',
		justifyContent: 'center'
	},
	markerCloseImage: {
		width: 24,
		height: 24,
		alignSelf: 'center',
	},
	markerText: {
		flexWrap: 'wrap',
		paddingHorizontal: 10,
		paddingBottom: 10,
		textAlign: 'left'
	},
	markerBottomArrow: {
		width: 30,
		height: 30,
		marginTop: -5,
		opacity: 1
	},
	bottomSafeStyle: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		width: screenWidth
	},
	acceptText: {
		padding: 15,
		fontSize: 18,
		color: 'white'
	}

})