
import React, { Component } from 'react';
import {
	// KeyboardAwareScrollView,
	Image, View, Text, TextInput, TouchableOpacity, ImageBackground, ScrollView, Dimensions, StatusBar, findNodeHandle
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Images from '../../global/images';
import styles from '../../../app/global/styles';
import constants from '../../../app/constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import * as SocialHelper from '../../apimanager/social';
import Toast from 'react-native-simple-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import firebase from 'react-native-firebase';
import {
	LoginButton,
	AccessToken,
	GraphRequest,
	GraphRequestManager,
	LoginManager
} from 'react-native-fbsdk';

class LoginScreen extends Component {
	static navigationOptions = () => ({
		header: null
	});

	state = {
		userInfo: {},
		username: "",
		password: "",
		screenWidth: Dimensions.get("window").width,
		screenHeight: Dimensions.get("window").height,
		loading: false,
		fcmToken: constants.FCM_TOKEN,
		loginCase: 1
	}

	componentDidMount = async () => {
		await AsyncStorage.removeItem("fcmToken");
		await SocialHelper._configureGoogleSignIn();
		// LoginManager.setLoginBehavior("browser");
	}

	checkToken = async () => {
		const fcmToken = await AsyncStorage.getItem("fcmToken");
		if (fcmToken) {
			constants.FCM_TOKEN = fcmToken;
			this.setState({ fcmToken }, () => {
				console.log("fcmToken checked;--" + this.state.fcmToken)
				this.checkLoginCase();
			});
		} else {
			this.checkPermission();
		}
	}

	//1
	checkPermission = () => {
		firebase.messaging().hasPermission().then(enabled => {
			if (enabled) {
				this.getToken();
			} else {
				this.requestPermission();
			}
		});
	}

	//3
	getToken = () => {
		firebase.messaging().getToken().then(async (fcmToken) => {
			if (fcmToken) {
				// user has a device token
				console.log("fcm id is :--" + fcmToken);
				constants.FCM_TOKEN = fcmToken;
				await AsyncStorage.setItem('fcmToken', fcmToken);
				this.checkLoginCase();
			}
		}).catch(err => {
			throw new Error(err)
		})
	}

	//2
	requestPermission = () => {
		firebase.messaging().requestPermission()
			.then(() => {
				this.getToken();
			})
			.catch(() => {
				// NavigationService.navigate('Login');
				// Toast.showWithGravity("You have rejected permission for notification", Toast.SHORT, Toast.BOTTOM);
				console.log("User has rejected permission");
				this.checkLoginCase();
			});
	}

	checkLoginCase = () => {
		switch (this.state.loginCase) {
			case 1:
				this.signIn();
				break;
			case 2:
				this.fbLogin();
				break;
			case 3:
				this.googleSignIn();
				break;
		}
	}

	checkValidation = () => {
		if (!this.state.username || !this.state.username.length) {
			Toast.showWithGravity("Please enter your email or Mobile no.", Toast.SHORT, Toast.BOTTOM);
		} else {
			let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (isNaN(this.state.username) && !emailcontext.test(this.state.username)) {
				Toast.showWithGravity("Please enter valid email ID", Toast.SHORT, Toast.BOTTOM);
			} else if (!isNaN(this.state.username) && ((this.state.username.startsWith('0') && (this.state.username.length != 11)) || (!this.state.username.startsWith('0') && (this.state.username.length != 10)))) {
				Toast.showWithGravity("Please enter valid Mobile no.", Toast.SHORT, Toast.BOTTOM);
			} else if (!this.state.password || !this.state.password.length) {
				Toast.showWithGravity("Please enter your password", Toast.SHORT, Toast.BOTTOM);
			} else if (!this.state.fcmToken || !this.state.fcmToken.length) {
				this.setState({ loginCase: 1 }, () => this.checkToken());
			} else this.signIn();
		}
	}

	signIn = () => {
		this.setState({ loading: true });
		let body = { username: this.state.username, password: this.state.password };
		ApiManager.login(body).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => {
					console.log("login paused");
				})
			}, 100);
			if (res && res.success && res.code == 200) {
				setTimeout(() => {
					this.setState({ loading: false }, () => {
						console.log("login paused1:-" + this.state.loading);
						console.log("login succes");
						constants.user = res.data.user;
						NavigationService.navigate(constants.previousStateForLogin, { logined: true });
					})
				}, 500);
			} else {
				setTimeout(() => {
					if (res.error && res.error.contact_number && res.error.contact_number.length) {
						if (this.props.navigation.state.routeName == "OtherLogin") {
							NavigationService.navigate("OtherOtp", { data: res.error });
						} else {
							NavigationService.navigate("Otp", { data: res.error })
						}
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
					}
				}, 500);
			}
			// });
		}).catch((err) => {
			this.setState({ loading: false });
			console.log("login error" + JSON.stringify(err));
		})
	}

	googleSignIn = async () => {
		try {
			this.setState({ loading: true });
			const result = await SocialHelper.googleLogin();
			ApiManager.googleLogin({
				id_token: result.idToken
			}).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false });
				}, 100);
				setTimeout(() => {
					if (res && res.success && res.code == 200) {
						console.log("login succes" + JSON.stringify(res));
						constants.user = res.data.user;
						NavigationService.navigate(constants.previousStateForLogin, { logined: true });
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
					}
				}, 500)
			}).catch((err) => {
				this.setState({ loading: false }, () => {
					console.log(JSON.stringify(err));
				})
			})
		} catch (error) {
			this.setState({ loading: false }, () => {
				console.log('error in google sign in' + JSON.stringify(error));
			})
		}
	};

	fbLogin = async () => {
		try {
			const data = await SocialHelper.fbLogin();
			if (data) {
				this.setState({ loading: true });
				ApiManager.fbLogin({
					user_id: data.userID,
					access_token: data.accessToken
				}).then((res) => {
					setTimeout(() => {
						this.setState({ loading: false });
					}, 100);
					setTimeout(() => {
						if (res && res.success && res.code == 200) {
							console.log("login succes" + JSON.stringify(res));
							constants.user = res.data.user;
							NavigationService.navigate(constants.previousStateForLogin, { logined: true });
						} else {
							Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
						}
					}, 500)
				}).catch((err) => {
					this.setState({ loading: false }, () => {
						console.log(JSON.stringify(err));
					})
				})
			}
		} catch (error) {
			this.setState({ loading: false }, () => {
				console.log('error in fb sign in' + JSON.stringify(error));
			});
		}
	}

	inputFocused = (ref) => {
		this._scroll(ref, ref == "myInput" ? 300 : 180);
	}

	inputBlurred = (ref) => {
		this._scroll(ref, 0);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	render() {
		console.log("Login page");
		return (
			<KeyboardAwareView animated={true} >
				<NavigationEvents onWillFocus={() => {
					this.setState({
						username: "",
						password: ""
					}, () => {
						console.log("Id and password refreshed..")
					})
					//Call whatever logic or dispatch redux actions and update the screen!
				}} />
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<ImageBackground source={Images.background} style={{ flex: 1, minHeight: this.state.screenHeight }}>
					< ScrollView style={{ width: "100%" }}
						showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}
						ref="myScrollView" keyboardDismissMode='interactive'
						contentContainerStyle={{
							width: this.state.screenWidth,
							height: this.state.screenHeight,
							height: this.state.screenHeight,
							alignItems: "center",
							justifyContent: "center"
						}}>
						<View style={styles.outerSection}>
							<View style={styles.innerSection}>
								<View style={styles.logoSection}>
									<Image source={Images.logo} style={styles.logo}></Image>
								</View>
								<Text style={styles.innerlabel}>Log In</Text>
								<View style={{
									alignItems: 'center',
									marginTop: 10,
									width: (this.state.screenWidth - 50),
								}}>
									<View style={styles.inputSection}>
										<Image source={Images.profile} style={styles.inputLogo} />
										<TextInput style={styles.input}
											value={this.state.username}
											placeholder='Email Id Or Mobile Number'
											placeholderTextColor={constants.placeHolderColor}
											onChangeText={(text) => { this.setState({ username: text }) }}
											returnKeyType="next"
											autoCapitalize="none"
											ref="myInput"
											onFocus={this.inputFocused.bind(this, 'myInput')}
										/>
									</View>
									<View style={styles.inputSection}>
										<Image source={Images.password} style={styles.inputLogo} />
										<TextInput style={styles.input}
											value={this.state.password}
											onChangeText={(text) => { this.setState({ password: text }) }}
											placeholder='Password'
											placeholderTextColor={constants.placeHolderColor}
											secureTextEntry={true}
											returnKeyType="next"
											autoCapitalize="none"
											ref="myInput1"
											onFocus={this.inputFocused.bind(this, 'myInput1')}
											returnKeyType="done"
											onSubmitEditing={() => this.checkValidation()}
										/>
									</View>
									<TouchableOpacity style={styles.loginButton}
										onPress={() => this.checkValidation()}>
										<Text style={[styles.loginText, { color: "white" }]}>LOGIN</Text>
									</TouchableOpacity>
									<TouchableOpacity style={styles.forgotLine}
										onPress={() => {
											if (this.props.navigation.state.routeName == "OtherLogin") {
												NavigationService.navigate("OtherForgotPassword");
											} else {
												NavigationService.navigate("ForgotPassword");
											}
										}}>
										<Text style={{ color: "black", fontFamily: constants.fonts.regular }}>Forgot your password?</Text>
										<Text style={{ color: "black", fontFamily: constants.fonts.bold, marginLeft: 5 }}>Reset it</Text>
									</TouchableOpacity>
									<TouchableOpacity style={[styles.loginButton, { borderColor: 'lightgrey', backgroundColor: 'white', }]}
										onPress={() => {
											if (this.props.navigation.state.routeName == "OtherLogin") {
												NavigationService.navigate("OtherSignUp");
											} else {
												NavigationService.navigate("SignUp");
											}
										}}>
										<Text style={styles.loginText}>SIGNUP</Text>
									</TouchableOpacity>
									<View style={styles.signUpBar}>
										<View style={styles.signUpLline}></View>
										<Text style={{ margin: 5, color: "black", fontFamily: constants.fonts.regular }}>Or Signup With</Text>
										<View style={styles.signUpLline}></View>
									</View>
									<View style={styles.socialSection}>
										<TouchableOpacity style={[styles.socialButton, { borderColor: '#475a95', marginRight: 15 }]}
											onPress={() => {
												this.setState({ loginCase: 2 }, () => this.checkToken())
											}}>
											<Image source={Images.fbLogo} style={{ width: 15, height: 30 }}></Image>
										</TouchableOpacity>
										<TouchableOpacity style={[styles.socialButton, { borderColor: '#c02c2c' }]}
											onPress={() => this.setState({ loginCase: 3 }, () => this.checkToken())}>
											<Image source={Images.googleLogo} style={{ width: 25, height: 27 }}></Image>
										</TouchableOpacity>
									</View>
								</View>
							</View>
						</View>
					</ ScrollView>
				</ImageBackground>
				<StatusBar backgroundColor="transparent" hidden={true} barStyle="light-content" />
			</KeyboardAwareView>
		)
	}
}

export default LoginScreen;