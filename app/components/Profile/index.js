import React, { Component } from 'react';
import { View, Text, TextInput, Image, Dimensions, TouchableOpacity, Animated, StatusBar, TouchableHighlight, Platform, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import * as SocialHelper from '../../apimanager/social';
import { GoogleSignin } from 'react-native-google-signin';
import ImagePicker from "react-native-image-picker";
import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../../../app/global/styles';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';

export default class ProfileScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			user: constants.user,
			mobile: '',
			loading: false,
			editable: false,
			contact_number: constants.user.contact_number,
			full_name: constants.user.full_name,
			imageUpdated: false
		};
	}

	componentDidMount = () => {
		SocialHelper._configureGoogleSignIn();
		let mobile = "";
		if (constants.user.facebook_id || constants.user.gmail_id) {
			mobile = "NA";
		} else {
			mobile = constants.user.contact_number.slice(0, 2) + "******" + constants.user.contact_number.slice(8, 10);
		}
		if (!constants.user.email) constants.user.email = "NA";
		this.setState({ user: constants.user, mobile: mobile }, () => {
			console.log(this.state.user);
			console.log(constants.user);
		});
	}

	logOut = async () => {
		// alert("Sorry");
		try {
			const isGoogleSignedIn = await GoogleSignin.isSignedIn();
			if (isGoogleSignedIn) {
				await GoogleSignin.revokeAccess();
				await GoogleSignin.signOut();
			}
			await AsyncStorage.removeItem("user");
			await AsyncStorage.removeItem("cart");
			await AsyncStorage.removeItem("selectAddress");
			await AsyncStorage.removeItem("selectedSlot");
			this.setState({ user: {} }, () => {
				constants.user = {};
				constants.selectAddress = {};
				constants.selectedSlot = {};
			})
		} catch (error) {
			console.error(error);
		}
	}

	showImagePicker() {
		// this.setState({ loading: true });
		const options = {
			rotation: 360,
			allowsEditing: true,
			noData: true,
			mediaType: "photo",
			maxWidth: 300,
			maxHeight: 300,
			storageOptions: {
				skipBackup: true
			}
		}
		ImagePicker.showImagePicker(options, response => {
			console.log("Response = ", response);
			// this.setState({ loading: false });
			if (response.didCancel) {
				//   console.log(JSON.stringify(source));
				console.warn('User cancelled image picker');
			} else if (response.error) {
				//  console.log(JSON.stringify(source));
				console.warn('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				//  console.log('User tapped custom button: ', response.customButton);
			} else {
				delete response.data;
				let user = this.state.user;
				user.picture = response.uri;
				const source = { uri: response.uri };
				this.setState({ user: user, imageUpdated: true }, () => {
					console.log("Image captured");
				});
				console.warn(source);
			}
		});
	}

	update = async () => {
		if (!this.state.full_name || !this.state.full_name.length) {
			Toast.showWithGravity("Please enter your full name", Toast.SHORT, Toast.BOTTOM);
			return false
		}
		if (!this.state.contact_number) {
			Toast.showWithGravity('Please enter contact number', Toast.LONG, Toast.CENTER);
			return
		}
		if (isNaN(this.state.contact_number) || (this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 11)) || (!this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 10))) {
			Toast.showWithGravity('Contact number is not valid', Toast.LONG, Toast.CENTER);
			return
		}
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity("Internet is not available", Toast.SHORT, Toast.BOTTOM);
			return
		}
		this.setState({ loading: true });
		let data = new FormData();
		if (this.state.imageUpdated) {
			data.append('picture', {
				uri: constants.user.picture,
				type: "image/jpeg", // or photo.type
				name: "profile_picture"
			});
		}
		data.append('contact_number', this.state.contact_number);
		data.append('full_name', this.state.full_name);
		let url = constants.url + 'user/profile/' + constants.user._id;
		let token = await AsyncStorage.getItem("token");
		const header = {
			"Content-Type": "multipart/form-data",
			"Authorization": "Bearer " + token
		};
		fetch(url, {
			method: 'PUT',
			headers: header,
			body: data
		}).then((res) => { return res.json() })
			.then(async (res) => {
				setTimeout(() => {
					this.setState({ loading: false });
				}, 100);
				if (res && res.success && res.code == 200) {
					if (res.data.customer.picture && !res.data.customer.picture.startsWith("http")) {
						res.data.customer.picture = constants.imageBaseURL + res.data.customer.picture;
					}
					await AsyncStorage.setItem("user", JSON.stringify(res.data.customer));
					constants.user = res.data.customer;
					let mobile = res.data.customer.contact_number.slice(0, 2) + "******" + res.data.customer.contact_number.slice(8, 10);
					this.setState({
						user: res.data.customer,
						contact_number: res.data.customer.contact_number,
						full_name: res.data.customer.full_name,
						mobile: mobile,
						imageUpdated: false,
						editable: false
					}, () => {
						console.log("Updated");
						console.log(JSON.stringify(this.state));
					});
				} else {
					setTimeout(() => {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					}, 500)
				}
			});
	}

	renderEditPage = () => {
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: constants.backgroundPageColor,
				marginTop: 40,
			}}>
				<View style={styles.inputSection}>
					<Image source={Images.profile} style={styles.inputLogo} />
					<TextInput
						style={styles.input}
						value={this.state.full_name}
						placeholder='Enter your full name'
						placeholderTextColor={constants.placeHolderColor}
						onChangeText={(text) => { this.setState({ full_name: text }) }}
					/>
				</View>
				<View style={styles.inputSection}>
					<Image source={Images.phoneImg} style={styles.inputLogo} />
					<TextInput
						style={styles.input}
						value={this.state.contact_number}
						placeholder='Enter Mobile Number'
						keyboardType="phone-pad"
						maxLength={11}
						placeholderTextColor={constants.placeHolderColor}
						onChangeText={(text) => { this.setState({ contact_number: text }) }}
						onSubmitEditing={() => this.update()}
					/>
				</View>
				<TouchableOpacity style={{
					backgroundColor: constants.green,
					padding: 10,
					paddingLeft: 17,
					paddingRight: 17,
					borderRadius: 25,
					marginTop: 50
				}} onPress={() => this.update()}>
					<Text style={{ color: "white", fontSize: 16, fontFamily: constants.fonts.bold }}>UPDATE</Text>
				</TouchableOpacity>
			</View>
		)
	}

	renderProfilePage = () => {
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: constants.backgroundPageColor
			}}>
				<View style={{
					width: "100%",
					minHeight: 90,
					flexDirection: "row"
				}}>
					<View style={{
						width: "30%",
						alignItems: "center",
						justifyContent: "space-around"
					}}>
						<Image source={Images.phoneImg} style={{ width: 15, height: 25 }}></Image>
						<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 14 }}>{this.state.mobile}</Text>
					</View>
					<View style={{
						width: "40%",
						alignItems: "center",
						justifyContent: "space-around",
						borderLeftWidth: 1,
						borderRightWidth: 1,
						borderColor: "darkgray",
						paddingLeft: 5,
						paddingRight: 5,
					}}>
						<Image source={Images.email} style={{ width: 25, height: 18 }}></Image>
						<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 14, textAlign: "center" }}>{this.state.user.email}</Text>
					</View>
					<View style={{
						width: "30%",
						alignItems: "center",
						justifyContent: "space-around"
					}}>
						<Image source={Images.password} style={{ width: 18, height: 22 }}></Image>
						<Text style={{ color: "black", fontFamily: constants.fonts.medium, fontSize: 14 }}>******</Text>
					</View>
				</View>
				<TouchableOpacity style={{
					backgroundColor: constants.green,
					padding: 10,
					paddingLeft: 17,
					paddingRight: 17,
					borderRadius: 25,
					marginTop: 50
				}} onPress={() => NavigationService.navigate("ChangePassword")}>
					<Text style={{ color: "white", fontSize: 16, fontFamily: constants.fonts.bold }}>CHANGE PASSWORD</Text>
				</TouchableOpacity>
			</View>
		)
	}

	render() {
		const Screen = {
			heignt: Dimensions.get('window').height,
			width: Dimensions.get('window').width,
			scale: Dimensions.get('window').scale,
			fontScale: Dimensions.get('window').fontScale,
		};
		let animatBackgroundColor = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: ['transparent', '#1faf4b'],
			extrapolate: 'clamp'
		});
		let translateY = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: [0, -((Screen.heignt / 3) - (getStatusBarHeight(false) + 44))],
			extrapolate: 'clamp',
		});

		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					contentContainerStyle={{ paddingTop: Screen.heignt / 3 }}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{
						this.state.editable ?
							this.renderEditPage() :
							this.renderProfilePage()
					}
				</Animated.ScrollView>

				<Animated.Image style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: Screen.heignt / 3.5,
					width: '100%',
					backgroundColor: 'white',
					alignItems: 'center',
					justifyContent: 'flex-end',
					transform: [{ translateY }],
					backgroundColor: '#f5f5f5',
					zIndex: -1
				}}
					resizeMode='stretch'
					source={Images.backgroundHeader}
				/>
				<Animated.View style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: Screen.heignt / 2.2,
					backgroundColor: 'transparent',
					zIndex: 0,
					alignItems: 'center',
					justifyContent: 'center',
					transform: [{ translateY }],
				}}>
					<View style={{ alignItems: 'center', justifyContent: 'center' }}>
						<TouchableOpacity style={{
							width: 120,
							height: 120,
						}} onPress={() => {
							if (this.state.editable) {
								this.showImagePicker();
							}
						}}>
							<View style={{
								width: "100%",
								height: "100%",
								resizeMode: 'cover',
								marginTop: -20,
								borderRadius: 60,
								position: "absolute",
								zIndex: -1,
								justifyContent: "center",
								alignItems: "center"
							}} >
								<ActivityIndicator size="large" color={constants.lightgreen} />
							</View>
							{
								this.state.user.picture ?
									<Image style={{
										width: "100%",
										height: "100%",
										resizeMode: 'cover',
										marginTop: -20,
										borderRadius: 60,
										position: "absolute",
										zIndex: 1
									}} source={{ uri: this.state.user.picture }} /> :
									<Image style={{
										width: 120,
										height: 120,
										resizeMode: 'cover',
										marginTop: -20,
										borderRadius: 60,
										position: "absolute",
										zIndex: 1
									}} source={Images.default_user} />
							}
						</TouchableOpacity>
						<View style={{
							width: Screen.width,
							height: 45,
							alignItems: "center"
						}}>
							<Text style={{
								color: "black", fontFamily: constants.fonts.medium,
								fontSize: 18, marginTop: 10
							}}>{this.state.user.full_name}</Text>
						</View>
					</View>
				</Animated.View>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					backgroundColor: animatBackgroundColor,
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: {
							height: (getStatusBarHeight(true) + 40),
						}
					})
				}}>
					<Animated.View style={{
						height: '100%',
						width: 60,
						justifyContent: 'center',
						alignItems: 'center'
					}}>
						<TouchableHighlight style={{
							height: 35,
							width: 35,
							...Platform.select({
								ios: { marginTop: 45 }, android: { marginTop: 14 }
							})
						}} underlayColor='transparent'
							onPress={() => {
								NavigationService.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.back} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						width: (Screen.width - 110),
						justifyContent: 'center'
					}}>
						<Text style={{
							fontSize: 18, color: 'white',
							fontFamily: constants.fonts.bold,
						}}>{this.state.editable ? "Edit" : "My"} Profile</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						width: 60,
						justifyContent: 'center',
						alignItems: 'center',
					}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							...Platform.select({
								ios: { marginTop: 40 },
								android: { marginTop: 10 }
							})
						}} underlayColor='transparent' onPress={() => {
							this.setState({ editable: !this.state.editable })
						}}>
							{
								this.state.editable ?
									<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.closeImg} /> :
									<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.editwhite} />
							}
						</TouchableHighlight>
					</Animated.View>
				</Animated.View>
				<StatusBar backgroundColor="#1faf4b" barStyle="light-content" />
			</View>
		)
	}
}