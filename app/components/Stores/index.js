import React, { Component } from 'react';
import {
	View, DeviceEventEmitter, ScrollView, FlatList, Text, Image, StyleSheet,
	Dimensions, TouchableOpacity, Alert, Animated, TouchableHighlight, Platform
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../constants/constants';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import Images from '../../global/images';
import { connect } from 'react-redux'
import * as Actions from '../../redux/Action/Index';

class StoreScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			stores: [],
			categories: [],
			loading: false,
			headerHeight: 0,
			totalCartItem: 0,
			area: {},
			city: {},
			storeChanged: false,
			categoryId: 1,
			categoryStores: []
		};
	}

	componentDidMount = () => {
		this.getStores();
		this.calculateCartItem();
	}

	calculateCartItem = () => {
		let cart = JSON.parse(JSON.stringify(this.props.cart));
		let counter = 0;
		for (let i = 0; i < cart.length; i++) {
			counter = counter + cart[i].count;
		}
		this.setState({ totalCartItem: counter }, () => {
			console.log("total items" + this.state.totalCartItem);
		});
	}

	changedStoreCheck = () => {
		if (this.state.storeChanged) {
			Alert.alert(
				'Alert',
				'Your basket has products from another store. Clear basket to shop from this store?',
				[
					{
						text: 'CLEAR',
						onPress: () => {
							AsyncStorage.removeItem("cart").then(() => {
								// this.addProduct(index, type)
								this.props.setCart([]);
								this.calculateCartItem();
							})
						},
						style: 'cancel',
					},
					{
						text: 'CANCEL',
						onPress: () => console.log('Cancel Pressed')
					}
				],
				{ cancelable: false },
			);
		} else this.goToCart();
	}

	goToCart = () => {
		constants.previousStateForCart = this.props.navigation.state.routeName;
		if (this.props.navigation.state.routeName == "SetStoreInHome") {
			NavigationService.navigate("Cart");
		} else NavigationService.navigate("StoreCart");
	}

	getStores = () => {
		this.setState({ loading: true });
		ApiManager.getStoresForSetting().then((res) => {
			setTimeout(() => {
				this.setState({ loading: false, }, () => console.log("Loading paused"));
			}, 100);
			setTimeout(() => {
				if (res && res.success) {
					console.log(res)
					this.setState({
						categories: res.data.categories ? res.data.categories : [],
						stores: res.data.stores ? res.data.stores : [],
						area: res.data.area,
						city: res.data.city
					}, () => {
						this.calculateCartItem();
					});
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false });
			Alert.alert(err.singleStringMessage);
		})
	}

	setStoreAndNavigateToHome = (store) => {
		if (constants.store._id != store._id) {
			constants.store = store;
			AsyncStorage.multiSet([['store', JSON.stringify(store)]], () => {
				NavigationService.navigate("Home");
				DeviceEventEmitter.emit("storeUpdate", store._id);
			});
		} else NavigationService.navigate("Home");
	}

	storeListRender = () => {
		return (
			<View style={{
				flex: 1,
				marginTop: (this.state.headerHeight - 22),
				alignItems: 'center',
				justifyContent: 'flex-start',
			}}>
				{
					this.state.stores.map((store, index) =>
						(
							<TouchableOpacity style={{
								height: 100,
								width: "100%",
								flexDirection: "row",
								justifyContent: "flex-start",
								backgroundColor: "white",
								borderBottomWidth: 8,
								paddingLeft: 5,
								paddingRightt: 5,
								borderColor: constants.backgroundPageColor,
							}} key={index} onPress={() => this.setStoreAndNavigateToHome(store)}>
								<View style={{ width: 90, height: 100, justifyContent: "center", alignItems: "center" }}>
									<View style={{
										width: 60, height: 60, borderRadius: 30,
										borderWidth: 1,
										borderColor: constants.backgroundPageColor
									}}>
										<Image source={store.picture ? store.imgUrl : defaultImg}
											style={{
												width: 60, height: 60,
												resizeMode: "contain",
												borderRadius: 30,
											}}></Image>
									</View>
								</View>
								<View style={{
									width: (this.state.screenWidth - 90),
									flexDirection: "column",
									justifyContent: "space-evenly",
									alignItems: "center",
									height: "95%"
								}}>
									<View style={{
										width: "100%",
										flexDirection: "row",
										height: 50,
										justifyContent: "space-between",
										alignItems: "flex-end",
										paddingRight: 15
									}}>
										<Text style={{
											fontSize: 15,
											color: "black", fontFamily: constants.fonts.semiBold
										}}>{store.name}</Text>
										<View style={{
											flexDirection: "row",
											width: "auto",
											justifyContent: "center",
											alignItems: "center"
										}}>
											<Image source={Images.clock_circle} style={{ width: 15, height: 15, marginRight: 5 }}></Image>
											<Text style={{
												fontSize: 12,
												color: "black", fontFamily: constants.fonts.medium
											}}>{store.has_express_delivery ? "Instant" : "Scheduled"} Delivery</Text>
										</View>
									</View>
									<View style={{
										width: "100%",
										height: 50,
										maxHeight: "50%", justifyContent: "center"
									}}>
										<Text style={{
											fontSize: 12,
											color: "black", fontFamily: constants.fonts.medium
										}}>Shop no. {store.address.shop_no}, {this.state.area.name}</Text>
									</View>
								</View>
							</TouchableOpacity>
						))
				}
			</View >)
	}

	renderStore = store => {
		let { screenWidth, area } = this.state;
		return (
			<TouchableOpacity style={{
				height: 100,
				width: "100%",
				flexDirection: "row",
				justifyContent: "flex-start",
				backgroundColor: "white",
				paddingLeft: 5,
				paddingRightt: 5,
				borderBottomWidth: 2,
				borderBottomColor: constants.backgroundPageColor
			}} onPress={() => this.setStoreAndNavigateToHome(store)}>
				<View style={{ width: 90, height: 100, justifyContent: "center", alignItems: "center" }}>
					<View style={{
						width: 60, height: 60, borderRadius: 30,
						borderWidth: 1,
						borderColor: constants.backgroundPageColor
					}}>
						<Image source={store.picture ? store.imgUrl : Images.gift}
							style={{
								width: 60, height: 60,
								resizeMode: "contain",
								borderRadius: 30,
							}}></Image>
					</View>
				</View>
				<View style={{
					width: (screenWidth - 90),
					flexDirection: "column",
					justifyContent: "space-evenly",
					alignItems: "center",
					height: "95%"
				}}>
					<View style={{
						width: "100%", height: 50,
						justifyContent: "flex-end", paddingRight: 15
					}}>
						<Text style={{
							fontSize: 15, color: "black",
							fontFamily: constants.fonts.bold
						}} numberOfLines={2}>{store.name}</Text>
					</View>
					<View style={{
						flexDirection: "row",
						width: "100%", height: 30,
						justifyContent: "flex-start",
						alignItems: "flex-end",
						// backgroundColor: "green",
					}}>
						<Image source={Images.clock_circle} style={{ width: 15, height: 15, marginRight: 5 }}></Image>
						<Text style={{
							fontSize: 12,
							color: "black", fontFamily: constants.fonts.medium
						}}>{store.has_express_delivery ? "Instant" : "Scheduled"} Delivery</Text>
					</View>
					<View style={{
						width: "100%", height: 50,
						maxHeight: "50%", justifyContent: "center"
					}}>
						<Text style={{
							fontSize: 12,
							color: "black", fontFamily: constants.fonts.medium
						}}>Shop no. {store.address.shop_no}, {area.name}</Text>
					</View>
				</View>
			</TouchableOpacity>
		)
	}

	renderStores = () => {
		return (
			<View style={{ flex: 1, backgroundColor: "white" }}>
				<FlatList data={this.state.categoryStores}
					renderItem={({ item }) => this.renderStore(item)}
					keyExtractor={this.keyExtractor} />
			</View>
		)
	}

	keyExtractor = (item) => item._id;

	renderAllStores = () => {
		let { categories, screenWidth } = this.state;
		return (
			<ScrollView contentContainerStyle={{ flex: 1, backgroundColor: "white" }} style={{ flex: 1 }}>
				{
					categories.map((category, index) => (
						<View key={index} style={{
							width: screenWidth,
							justifyContent: "center",
							borderBottomWidth: 1,
							borderBottomColor: "lightgrey",
						}}>
							<View style={{
								backgroundColor: constants.backgroundPageColor,
								height: 35, paddingStart: 10,
								justifyContent: "center"
							}}>
								<Text style={{
									color: constants.green,
									fontFamily: constants.fonts.semiBold,
									fontSize: 14, textTransform: "capitalize"
								}}>{category.categoryDetails.name}</Text>
							</View>
							<FlatList data={category.stores}
								renderItem={({ item }) => this.renderStore(item)}
								keyExtractor={this.keyExtractor} />
						</View>
					))
				}
			</ScrollView>
		)
	}

	renderCategories = () => {
		let { categories, screenWidth, categoryId } = this.state;
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-start',
				...Platform.select({
					ios: { paddingTop: (getStatusBarHeight(true) + 10), },
					android: { paddingTop: (getStatusBarHeight(true) + 10), }
				})
			}}>
				<View style={{
					height: 40,
					width: screenWidth,
					backgroundColor: "white"
				}}>
					<ScrollView horizontal={true}
						keyboardShouldPersistTaps={'handled'}
						showsHorizontalScrollIndicator={false}>
						<TouchableOpacity style={[styles.categoryBtn, { borderBottomWidth: categoryId === 1 ? 1.5 : 0 }]} å
							onPress={() => {
								this.setState({ categoryId: 1 });
							}}>
							<Text style={{
								fontSize: 14,
								color: categoryId === 1 ? constants.green : constants.black,
								fontFamily: constants.fonts.semiBold,
							}}>All </Text>
						</TouchableOpacity>
						{
							categories.map((category, index) =>
								(
									<TouchableOpacity style={[styles.categoryBtn, { borderBottomWidth: categoryId === category._id ? 1.5 : 0 }]} key={index}
										onPress={() => {
											this.setState({ categoryId: category._id, categoryStores: category.stores });
										}}>
										<Text style={{
											fontSize: 14,
											color: categoryId === category._id ? constants.green : constants.black,
											fontFamily: constants.fonts.semiBold,
										}}>{category.categoryDetails.name} </Text>
									</TouchableOpacity>
								))
						}
					</ScrollView>
				</View>
				{
					categoryId === 1 ?
						this.renderAllStores() :
						this.renderStores()
				}
			</View>
		)
	}

	render() {
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<NavigationEvents onWillFocus={() => {
					this.getStores();
					const store = constants.store;
					let cart = JSON.parse(JSON.stringify(this.props.cart));
					let changedStore = false;
					for (let i = 0; i < cart.length; i++) {
						if (store._id != cart[i].store_id) {
							changedStore = true;
							break;
						}
					}
					this.setState({ storeChanged: changedStore }, () => {
						console.log("store changed:--" + this.state.storeChanged);
					});
					//Call whatever logic or dispatch redux actions and update the screen!
				}}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderCategories()}
					{/* {this.storeListRender()} */}
				</Animated.ScrollView>
				<Animated.View style={{ top: 0, left: 0, right: 0, position: 'absolute', flexDirection: 'row', ...Platform.select({ ios: { height: (getStatusBarHeight(true) + 40), }, android: { height: (getStatusBarHeight(true) + 40), } }), backgroundColor: '#1faf4b' }}>
					<Animated.View style={{
						height: '100%',
						...Platform.select({
							ios: { marginTop: 15 }, android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: (this.state.screenWidth - 100),
						// alignItems: "center",
						// backgroundColor: "red"
					}}>
						<Text style={{
							fontSize: 20, color: 'white',
							fontFamily: constants.fonts.bold,
							marginLeft: 20
						}}>All Stores</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						width: 100,
						justifyContent: "space-around",
						alignItems: 'center', flexDirection: 'row',
						...Platform.select({
							ios: { marginTop: 15 }, android: { marginTop: 0 }
						}),
					}}
						onLayout={(event) => {
							this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
								console.log("Header height is:--" + this.state.headerHeight);
							})
							console.log(event.nativeEvent.layout);
						}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							justifyContent: "center",
							alignItems: "center"
						}} underlayColor='transparent' onPress={() => {
							NavigationService.navigate("Search")
							// Alert.alert("Search feature is coming soon..")
						}}>
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.search} />
						</TouchableHighlight>
						<TouchableOpacity style={{
							height: 35, width: 35,
							justifyContent: "center",
							alignItems: "center"
						}} underlayColor='transparent'
							onPress={() => {
								{
									this.state.totalCartItem ?
										this.changedStoreCheck() :
										Alert.alert("Please add few items in cart");
								}
							}}>
							{
								this.state.totalCartItem ?
									<View style={{
										backgroundColor: "#e9001f",
										width: "auto",
										height: "auto",
										paddingTop: this.state.storeChanged ? 5 : 2,
										paddingBottom: this.state.storeChanged ? 4 : 3,
										paddingLeft: 7,
										paddingRight: 8,
										position: "absolute",
										zIndex: 999,
										top: -8,
										left: -4,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: 15
									}}>
										{
											this.state.storeChanged ?
												<Image source={Images.closeImg} style={{ height: 10, width: 10 }} /> :
												<Text style={{
													color: "white",
													fontFamily: constants.fonts.bold, fontSize: 10
												}}>{this.state.totalCartItem}</Text>
										}
									</View> : null
							}
							<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.cart} />
						</TouchableOpacity>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.getCart,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setCart: (data) => dispatch(Actions.setCart(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreScreen)

const styles = StyleSheet.create({
	categoryBtn: {
		width: "auto",
		height: 40,
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
		paddingLeft: 15,
		paddingRight: 15,
		borderBottomColor: constants.green
	}
})