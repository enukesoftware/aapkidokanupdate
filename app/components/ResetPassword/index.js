
import React, { Component } from 'react';
import { Image, findNodeHandle, Dimensions, View, Text, TextInput, TouchableOpacity, ImageBackground, ScrollView } from 'react-native'
import styles from '../../../app/global/styles';
import constants from '../../../app/constants/constants';
import * as ApiManager from '../../apimanager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
import Images from '../../global/images';

export default class ResetPasswordScreen extends Component {
	static navigationOptions = () => ({
		header: null
	});

	state = {
		password: "",
		re_password: "",
		loading: false
	}

	inputFocused = (ref) => {
		this._scroll(ref, 150);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	resetPassword = () => {
		if (!this.state.password || !this.state.password.length) {
			Toast.showWithGravity(
				"Please enter your New Password",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if ((!this.state.re_password || !this.state.re_password.length)) {
			Toast.showWithGravity(
				"Please confirm your New Password",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (this.state.password != this.state.re_password) {
			Toast.showWithGravity(
				"Password Mismatch",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else {
			this.setState({ loading: true });
			ApiManager.resetPassword({ password: this.state.password }).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(() => {
					if (res && res.success && res.code == 200) {
						Toast.showWithGravity(
							res.data.message,
							Toast.LONG,
							Toast.BOTTOM //TOP,BOTTOM
						);
						if (this.props.navigation.state.routeName == "ResetPassword") {
							NavigationService.navigate("Login");
						} else {
							NavigationService.navigate("OtherLogin");
						}
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.LONG, Toast.CENTER);
					}
				}, 500);
			}).catch((err) => {
				this.setState({ loading: false });
				console.log(JSON.stringify(err));
			})
		}
	}

	render() {
		console.log(this.props.navigation.state.routeName);
		return (
			<KeyboardAwareView animated={true}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: constants.fonts.bold }}
				/>
				<ImageBackground source={Images.background} style={{ flex: 1 }}>
					<ScrollView showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}
						ref="myScrollView" keyboardDismissMode='interactive'>
						<View style={{ height: screenHeight, width: screenWidth, justifyContent: "center", alignItems: "center" }}>
							<View style={[styles.innerSection, { minHeight: 250, paddingBottom: 30 }]}>
								<View style={styles.logoSection}>
									<Image source={Images.logo} style={styles.logo}></Image>
								</View>
								<Text style={[styles.innerlabel, { fontSize: 15, marginBottom: 15, marginTop: 20 }]}>Reset Your Password</Text>
								<View style={styles.inputSection}>
									<Image source={Images.password} style={styles.inputLogo} />
									<TextInput
										style={styles.input}
										value={this.state.password}
										placeholder='Enter New Password'
										placeholderTextColor={constants.placeHolderColor}
										autoCapitalize="none"
										secureTextEntry={true}
										onChangeText={(text) => { this.setState({ password: text }) }}
										returnKeyType="next"
										autoCapitalize="none"
										ref="input"
										onFocus={this.inputFocused.bind(this, 'input')}
									/>
								</View>
								<View style={[styles.inputSection, { marginTop: 15 }]}>
									<Image source={Images.password} style={styles.inputLogo} />
									<TextInput
										style={styles.input}
										value={this.state.re_password}
										placeholder='Confirm New Password'
										placeholderTextColor={constants.placeHolderColor}
										autoCapitalize="none"
										secureTextEntry={true}
										onChangeText={(text) => { this.setState({ re_password: text }) }}
										returnKeyType="done"
										autoCapitalize="none"
										ref="input1"
										onFocus={this.inputFocused.bind(this, 'input1')}
										onSubmitEditing={() => this.resetPassword()}
									/>
								</View>
								<TouchableOpacity style={[styles.loginButton, { marginTop: 15 }]}
									onPress={() => this.resetPassword()}>
									<Text style={[styles.loginText, { color: "white" }]}>RESET PASSWORD</Text>
								</TouchableOpacity>
							</View>
						</View>
					</ScrollView>
				</ImageBackground>
			</KeyboardAwareView>
		)
	}
}