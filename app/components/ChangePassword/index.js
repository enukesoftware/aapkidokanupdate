import React, { Component } from 'react';
import { View, findNodeHandle, ScrollView, Text, Image, Dimensions, TouchableOpacity, Animated, Alert, Platform, TextInput } from 'react-native';
import constants from '../../constants/constants';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Images from '../../global/images';
import styles from '../../../app/global/styles';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ApiManager from '../../apimanager/ApiManager';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../global/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'

export default class ChangePasswordScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			user: constants.user,
			headerHeight: 0,
			new_password: "",
			old_password: "",
			re_password: "",
			loading: false,
		};
	}

	changePassword = () => {
		if (!this.state.old_password.length) {
			Toast.showWithGravity(
				"Please enter your old password ...",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.new_password.length) {
			Toast.showWithGravity(
				"Please enter your new password ...",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.re_password.length) {
			Toast.showWithGravity(
				"Please confirm your new password ...",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (this.state.new_password != this.state.re_password) {
			Toast.showWithGravity(
				"Password mismatch ...",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else {
			this.setState({ loading: true })
			ApiManager.changePassword({
				password: this.state.old_password,
				new_password: this.state.new_password
			}).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => {
						console.log("loading paused");
					})
				}, 100);
				setTimeout(() => {
					Toast.showWithGravity(
						(res && res.success) ? res.data.message : res.singleStringMessage,
						Toast.LONG,
						Toast.BOTTOM //TOP,BOTTOM
					);
					if (res && res.success) {
						NavigationService.goBack();
					}
				}, 500);
			}).catch((err) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => {
						console.log("loading paused");
					})
				}, 100);
				setTimeout(() => {
					Alert.alert(JSON.stringify(err));
				}, 1000);
			})
		}
	}

	inputFocused = (ref) => {
		this._scroll(ref, ref == "myInput2" ? 300 : 180);
	}

	inputBlurred = (ref) => {
		this._scroll(ref, 0);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	renderPage = () => {
		return (
			<KeyboardAwareView animated={true}>
				< ScrollView style={{ width: "100%" }}
					showsVerticalScrollIndicator={false}
					keyboardShouldPersistTaps={'handled'}
					ref="myScrollView" keyboardDismissMode='interactive'
					contentContainerStyle={{ width: this.state.screenWidth, alignItems: "center" }}>
					<View style={{
						width: this.state.screenWidth - 30,
						alignItems: "center",
						marginTop: this.state.screenHeight / 5,
						height: 300,
					}}>
						<View style={[styles.inputSection, { width: "90%" }]}>
							<Image source={Images.password} style={styles.inputLogo} />
							<TextInput style={styles.input}
								value={this.state.old_password}
								onChangeText={(text) => { this.setState({ old_password: text }) }}
								placeholder='Old Password'
								placeholderTextColor={constants.placeHolderColor}
								secureTextEntry={true}
								returnKeyType="next"
								autoCapitalize="none"
								ref="myInput"
								onFocus={this.inputFocused.bind(this, 'myInput')}
							/>
						</View>
						<View style={[styles.inputSection, { width: "90%" }]}>
							<Image source={Images.password} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.new_password}
								onChangeText={(text) => { this.setState({ new_password: text }) }}
								placeholder='New Password'
								placeholderTextColor={constants.placeHolderColor}
								secureTextEntry={true}
								returnKeyType="next"
								autoCapitalize="none"
								ref="myInput1"
								onFocus={this.inputFocused.bind(this, 'myInput1')}
							/>
						</View>
						<View style={[styles.inputSection, { width: "90%" }]}>
							<Image source={Images.password} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.re_password}
								onChangeText={(text) => { this.setState({ re_password: text }) }}
								placeholder='Confirm Password'
								placeholderTextColor={constants.placeHolderColor}
								secureTextEntry={true}
								returnKeyType="next"
								autoCapitalize="none"
								ref="myInput2"
								onFocus={this.inputFocused.bind(this, 'myInput2')}
							/>
						</View>
						<TouchableOpacity style={{
							backgroundColor: constants.green,
							padding: 10,
							paddingLeft: 17,
							paddingRight: 17,
							borderRadius: 25,
							marginTop: 50
						}} onPress={() => this.changePassword()}>
							<Text style={{ color: "white", fontSize: 16, fontFamily: constants.fonts.bold }}>CHANGE PASSWORD</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</KeyboardAwareView >
		)
	}

	render() {
		console.log("More page");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner size="large"
					visible={this.state.loading}
					color={constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white" }}
				/>
				<Animated.View
					style={{ flex: 1 }}>
					{this.renderPage()}
				</Animated.View>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: { height: (getStatusBarHeight(true) + 40), }
					}),
					backgroundColor: '#1faf4b',
					paddingBottom: 10
				}}>
					<Animated.View style={{
						height: '100%', width: '10%',
						minWidth: 60,
						minHeight: 60,
						justifyContent: 'center',
						alignItems: 'center',
						position: "absolute",
						left: 0,
						top: 0,
						zIndex: 2,
						paddingLeft: 10
					}}>
						<TouchableOpacity style={{
							height: 45, width: 45,
							...Platform.select({
								ios: { marginTop: 50 },
								android: { marginTop: 19 }
							}),
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								NavigationService.goBack()
							}}>
							<Image style={{ height: 20, width: 30, resizeMod: 'contain' }} source={Images.back} />
						</TouchableOpacity>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 60,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: this.state.screenWidth - 60,
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<Text style={{
							fontSize: 18, color: 'white',
							fontFamily: constants.fonts.bold,
							// backgroundColor: "red"
						}}>Change Password</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}