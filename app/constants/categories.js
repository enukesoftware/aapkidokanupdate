export default Categories = [
  {
    "_id": "5e4fb5e3beee3369a6b32a0d",
    "stores": [
      {
        "_id": "5d7607c59b5f0f76ee4f68b0",
        "picture": "12.png",
        "has_express_delivery": true,
        "drivers": [
          "5d79f10ed22f1e78b7671d74",
          "5dea892ed85c19035ebf1238"
        ],
        "status": 1,
        "timings": {
          "open_time": "12:00",
          "close_time": "21:00"
        },
        "owner": {
          "email": "ham.hassansiddiqui@gmail.com",
          "password": "$2b$10$WMWphftjpv98yO.X0gUFVu6.FFK/oH0CmucMFdFb.vpaL9eFw81Wu",
          "full_name": "Hammad Hassan",
          "contact_number": "03333743790"
        },
        "self_delivery": false,
        "address": [
          {
            "_id": "5d7607c59b5f0f76ee4f68b1",
            "coordinates": {
              "latitude": 24.9210435,
              "longitude": 67.10559360000002
            },
            "city_id": "5d7603909b5f0f76ee4f68ad",
            "area_id": "5e262d10d21fe166d8c93049",
            "shop_no": "NA-Class 190/219",
            "locality": "Karachi",
            "unique_link": "TKhr3"
          }
        ],
        "name": "METRO",
        "commission": 3,
        "created_at": "2019-09-09T08:05:25.721Z",
        "updated_at": "2020-02-21T12:34:19.142Z",
        "__v": 0,
        "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNzYwN2M1OWI1ZjBmNzZlZTRmNjhiMCIsInR5cGUiOjIsImlhdCI6MTU4MjI4ODQ1OX0.PNI84Hyt2gq3dRhtIH-emKoc9Og0FqnsS02VHhgQJKk",
        "sku_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNzYwN2M1OWI1ZjBmNzZlZTRmNjhiMCIsImlhdCI6MTU3ODkxMTA4Mn0.k-Ek52UwRftyj8hywTqFUneWb1kW8_pfocE0ayMaFZI",
        "storeCategory": "5e4fb5e3beee3369a6b32a0d"
      }
    ],
    "categoryDetails": {
      "_id": "5e4fb5e3beee3369a6b32a0d",
      "status": 2,
      "name": "category 34",
      "created_at": "2020-02-21T10:50:11.584Z",
      "updated_at": "2020-02-21T12:24:10.138Z",
      "__v": 0
    }
  },
  {
    "_id": "5e4ff077d463092ecbc36e6e",
    "stores": [
      {
        "_id": "5df637e34648a37afa44d109",
        "picture": "cowwww.jpg",
        "has_express_delivery": false,
        "drivers": [],
        "status": 1,
        "timings": {
          "open_time": "10:00",
          "close_time": "20:00"
        },
        "owner": {
          "email": "tanzeels@gmail.com",
          "password": "$2b$10$ENYq6vaX5CnvzbqLTVp/WuMVOlh0hsgK37X2KNvsKHSC/oGesQAJK",
          "full_name": "Tanzeel",
          "contact_number": "03018492425"
        },
        "self_delivery": true,
        "address": [
          {
            "_id": "5df637e34648a37afa44d10a",
            "coordinates": {
              "latitude": 24.909252523573674,
              "longitude": 67.13001967493653
            },
            "city_id": "5d7603909b5f0f76ee4f68ad",
            "area_id": "5e262d10d21fe166d8c93049",
            "shop_no": "37",
            "locality": "Johar",
            "gps_address": "32, Block 18 Gulistan-e-Johar, Karachi, Karachi City, Sindh, Pakistan",
            "unique_link": "Z2jPHpd"
          }
        ],
        "name": "Fresh Picked",
        "commission": 3,
        "created_at": "2019-12-15T13:40:51.978Z",
        "updated_at": "2020-02-21T15:01:17.476Z",
        "__v": 0,
        "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkZjYzN2UzNDY0OGEzN2FmYTQ0ZDEwOSIsInR5cGUiOjIsImlhdCI6MTU3NjgzNzA0Nn0.J5xEfM3cvWfv4P5onmFaOgjyKR2QO_-GYXU40SOvotM",
        "storeCategory": "5e4ff077d463092ecbc36e6e"
      }
    ],
    "categoryDetails": {
      "_id": "5e4ff077d463092ecbc36e6e",
      "status": 1,
      "name": "Category 25",
      "created_at": "2020-02-21T15:00:07.822Z",
      "updated_at": "2020-02-21T15:00:26.006Z",
      "__v": 0
    }
  }
]