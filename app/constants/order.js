export default Order = {
  "_id": "5e5e46ae2daf030e5c835fec",
  "address": {
    "pickup": {
      "coordinates": {
        "latitude": 24.9237623,
        "longitude": 67.1405999
      },
      "city_id": "5d7603909b5f0f76ee4f68ad",
      "area_id": "5e262d10d21fe166d8c93049",
      "shop_no": "Shop #101  Street 5, Block 10 Gulistan-e-Johar, Karachi, Karachi City, Sindh Pakistan",
      "locality": "Kamran Chorangi"
    },
    "delivery": {
      "coordinates": {
        "latitude": 28.477400026343858,
        "longitude": 77.03496001660824
      },
      "full_name": "Pince Kumar",
      "email": "prince.kumar@enukesoftware.com",
      "contact_number": "08285724681",
      "house_no": "339/2, Pavilion Building, Anamika Enclave, ",
      "locality": "Mahrauli road, Sector-13, Gurugram",
      "city_id": "5d7603909b5f0f76ee4f68ad",
      "alias": "work",
      "gps_address": "1777/3, Mata Rd, Rajiv Nagar, Sector 13, Gurugram, Haryana 122001, India",
      "landmark": "Misaki Express Building",
      "what_3_words": "",
      "city": {
        "_id": "5d7603909b5f0f76ee4f68ad",
        "areas": [
          "5d7605099b5f0f76ee4f68ae",
          "5d7605219b5f0f76ee4f68af",
          "5dea5254d85c19035ebf1217",
          "5dea52b5d85c19035ebf1218",
          "5dea52bfd85c19035ebf1219",
          "5dea52d0d85c19035ebf121a",
          "5dea52e0d85c19035ebf121b",
          "5dea52eed85c19035ebf121c",
          "5df635134648a37afa44d108",
          "5e05acca954bc84f45786dd2",
          "5e05acf1954bc84f45786dd3",
          "5e05acfb954bc84f45786dd4",
          "5e05ad5e954bc84f45786dd5",
          "5e262d10d21fe166d8c93049",
          "5e262d43d21fe166d8c9304a",
          "5e2ac47dd21fe166d8c930b3",
          "5e469966d21fe166d8c9356f"
        ],
        "status": 1,
        "name": "Karachi",
        "created_at": "2019-09-09T07:47:28.591Z",
        "updated_at": "2020-02-14T12:58:14.336Z",
        "__v": 0
      }
    }
  },
  "status": 1,
  "is_express_delivery": true,
  "payment_type": 1,
  "discount": 0,
  "delivery_charges": 50,
  "driver_assigned": false,
  "is_delivered_by_store": false,
  "store_paid": false,
  "deliver_start_time": "2020-03-03T11:59:42.977Z",
  "deliver_end_time": "2020-03-03T13:59:42.977Z",
  "customer_id": "5e425cb6d21fe166d8c934d0",
  "store_id": "5e05d5ff954bc84f45786dd6",
  "products": [
    {
      "pictures": [
        "003000184.jpg"
      ],
      "_id": "5e5e46ae2daf030e5c835fee",
      "product_id": "5e09e5cc954bc84f45786e74",
      "size": "10 Kg",
      "price": 600,
      "count": 1,
      "name": "Al-Khubz Chakki Atta"
    },
    {
      "pictures": [
        "00300011.png"
      ],
      "_id": "5e5e46ae2daf030e5c835fed",
      "product_id": "5e09e61d954bc84f45786e75",
      "size": "5 Kg",
      "price": 300,
      "count": 1,
      "name": "Al-Khubz Chakki Atta"
    }
  ],
  "total_amount": 900,
  "taxes": [],
  "total_amount_after_tax": 900,
  "commission_percentage": 2,
  "admin_commission_amount": 18,
  "store_payout_amount": 882,
  "order_id": "60f3f87",
  "created_at": "2020-03-03T11:59:43.002Z",
  "updated_at": "2020-03-03T11:59:43.002Z",
  "__v": 0,
  "store": {
    "_id": "5e05d5ff954bc84f45786dd6",
    "picture": "KNN STORE.jpg",
    "has_express_delivery": true,
    "drivers": [
      "5d9dda22d22f1e78b76728c3",
      "5dea5445d85c19035ebf121e",
      "5dea892ed85c19035ebf1238",
      "5dea53edd85c19035ebf121d",
      "5e4e2a39d21fe166d8c94153",
      "5e537a88d21fe166d8c94568",
      "5e58ec2a2daf030e5c835e34"
    ],
    "status": 1,
    "timings": {
      "open_time": "11:00",
      "close_time": "23:00"
    },
    "owner": {
      "email": "najam.sahto@gmail.com",
      "password": "$2b$10$oJMcgQDP2Ml8SO0lxoCHmu.gblcisq8Ue18wiKtAX0ZL6KUTbqjsm",
      "full_name": "Najam",
      "contact_number": "03012999901"
    },
    "self_delivery": false,
    "address": [
      {
        "_id": "5e05d5ff954bc84f45786dd7",
        "coordinates": {
          "latitude": 24.9237623,
          "longitude": 67.1405999
        },
        "city_id": "5d7603909b5f0f76ee4f68ad",
        "area_id": "5e262d10d21fe166d8c93049",
        "shop_no": "Shop #101  Street 5, Block 10 Gulistan-e-Johar, Karachi, Karachi City, Sindh Pakistan",
        "locality": "Kamran Chorangi",
        "gps_address": "Street 5, Block 10 Gulistan-e-Johar, Karachi, Karachi City, Sindh, Pakistan",
        "unique_link": "1K9RNH"
      },
      {
        "_id": "5e26d1f5d21fe166d8c9305b",
        "coordinates": {
          "latitude": 24.901932227546816,
          "longitude": 67.08738320527952
        },
        "city_id": "5d7603909b5f0f76ee4f68ad",
        "area_id": "5e262d43d21fe166d8c9304a",
        "shop_no": "Shop B 170, Block 18 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan",
        "locality": "Itehad Park",
        "gps_address": "Plot B 170, Block 18 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan",
        "unique_link": "2nq4ow"
      }
    ],
    "name": "KNN General Store",
    "commission": 2,
    "created_at": "2019-12-27T09:59:27.130Z",
    "updated_at": "2020-02-28T10:39:02.269Z",
    "__v": 0,
    "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlMDVkNWZmOTU0YmM4NGY0NTc4NmRkNiIsInR5cGUiOjIsImlhdCI6MTU4MjcwNjkzNX0.0m6WT23VeDZD__Cir4Ax9DCnGhklxizP04Sfc2X0jtM"
  },
  "customer": {
    "_id": "5e425cb6d21fe166d8c934d0",
    "picture": "https://lh3.googleusercontent.com/a-/AOh14Gg8Zox_5Q9tbq30_JFEzhyRb3etnKmDnb5u_Zcs=s96-c",
    "gmail_id": "112833573533799174620",
    "facebook_id": null,
    "is_logout": false,
    "status": 1,
    "email": "prince.kumar735@gmail.com",
    "full_name": "Prince Kumar",
    "fcm_token": "f_7CVA7DiBY:APA91bFOubB7TlFAxMvM-wluSbCLn3pmUnjXGMbCHIiQazH54Nfeken8QKBn6GLJ0q_OYCt6huwbQ46Elyjr-OqvOo-STaEkSTgpHduBG7SKjE31CvWM3OZ0CNBpD3raULei0k9304-R",
    "address": [
      {
        "_id": "5e53aea6d21fe166d8c94582",
        "full_name": "Pince Kumar",
        "email": "prince.kumar@enukesoftware.com",
        "contact_number": "08285724681",
        "house_no": "339/2, Pavilion Building, Anamika Enclave, ",
        "locality": "Mahrauli road, Sector-13, Gurugram",
        "city_id": "5d7603909b5f0f76ee4f68ad",
        "alias": "work",
        "coordinates": {
          "latitude": 28.477400026343858,
          "longitude": 77.03496001660824
        },
        "gps_address": "1777/3, Mata Rd, Rajiv Nagar, Sector 13, Gurugram, Haryana 122001, India",
        "landmark": "Misaki Express Building",
        "what_3_words": ""
      }
    ],
    "created_at": "2020-02-11T07:50:14.664Z",
    "updated_at": "2020-03-03T12:06:26.396Z",
    "__v": 0
  },
  "order_date": "3 March, 2020",
  "orderStatus": "Placed"
}