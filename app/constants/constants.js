const productionUrl = "https://aapkidokan.com/";
const testingUrl = "https://test.aapkidokan.com/";
const yiiproUrl = "http://aapkidokan.yiipro.com/";
const baseUrl = productionUrl;

export default Constants = {
	red: "#ef4b4b",
	green: "#1faf4b",
	placeHolderColor: "#3e3f3f",
	black: "#4d4d4d",
	backgroundPageColor: "#f5f5f5",
	lightgreen: '#D0EFDC',
	currency: "PKR",
	baseUrl,
	url: baseUrl + "api/customer/",
	imageBaseURL: baseUrl + "uploads/",
	store: {},
	area: {},
	city: {},
	user: {},
	previousStateForLogin: "SelectLocationAfterLogin",
	previousStateForCart: "",
	previousStateForSubCategory: "Home",
	previousStateForSelectAddress: "Cart",
	backStateForOrders: "Home",
	category: {},
	selectAddress: {},
	order: {},
	products: [],
	selectedSlot: {},
	version: "1.14",
	coupon: {},
	delivery_charges: [],
	taxes: [],
	fonts: {
		bold: "Montserrat-Bold",
		extraBold: "Montserrat-ExtraBold",
		semiBold: "Montserrat-SemiBold",
		light: "Montserrat-Light",
		regular: "Montserrat-Regular",
		medium: "Montserrat-Medium",
		thin: "Montserrat-Thin",
	},
	cartStore: {},
	cartCity: {},
	cartArea: {},
	WEB_CLIENT_ID: "348600376683-bj54dpfkvj46ojsigmm835spp8k0b8gp.apps.googleusercontent.com",
	GOOGLE_API_KEY: "AIzaSyALTt5V3_2zClONWGccm8UrLK626-gi4Vs",
	FCM_TOKEN: "",
	defaultRegion: {
		latitude: 24.860735,
		longitude: 67.001137
	},
	geoOptions: {
		enableHighAccuracy: true,
		// timeOut: 20000,
		maximumAge: 60 * 60 * 24
	},
	routesData: ["store", "area", "city",
		"selectAddress", "user", "coupon",
		"taxes", "delivery_charges", "fcmToken",
		"cartStore", "cartArea", "cartCity", "cart"],
	EVENTS: {
		liveLocation: "LIVE_LOCATION"
	},
	projectId: 'aapkidokan-4c311',
	googleMapDirectionApiUrl: 'https://maps.googleapis.com/maps/api/directions/json?',
	PLATFORM: {
		android: 'android',
		ios: 'ios'
	},
	TOOLBAR_HEIGHT: {
		android: 56,
		ios: 44,
	},
	STORAGE_KEY: {
		selectedLocation: 'SELECTED_LOCATION',
		isLogin: 'IS_LOGIN',
		userData: 'USER_DATA',
		adminData: 'ADMIN_DATA',
		basketData: 'BASKET_DATA',
		deviceToken: 'DEVICE_TOKEN',
	},
};