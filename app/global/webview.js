import { View, StyleSheet, Dimensions, Modal, Alert, SafeAreaView, ActivityIndicator, TouchableOpacity, Image, Text } from "react-native";
import { WebView } from "react-native-webview";
import React, { Component } from "react";
import constants from "../constants/constants";
import images from "./images";
// import { SafeAreaView } from "react-navigation";

class WebViewExample extends React.Component {
  renderToolbar() {
    return (
      <View style={styles.toolbar}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          paddingHorizontal: 10
        }}>
          <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center', height: "100%" }}>
            <TouchableOpacity activeOpacity={0.8}
              style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%', height: "100%" }}
              onPress={() => {
                console.log("closing web view");
                this.props.closeWebView();
              }}>
              <Image source={images.ic_back_ios} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
              <Text style={styles.backBtnText}> Back</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '70%', alignItems: 'center' }}>
            <Text style={styles.headerStnName} >{this.props.title}</Text>
          </View>
          <View style={{ width: '15%' }}>
            {this.props.rightImg ?
              <TouchableOpacity style={styles.headerButton} onPress={() => this.props.fireEvent()}>
                <Image source={this.props.rightImg ? this.props.rightImg : null} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
              </TouchableOpacity> : null
            }
          </View>
        </View>
      </View >
    )
  }

  ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <View style={styles.ActivityIndicatorStyle}>
        <ActivityIndicator
          color={constants.green}
          size="large"
        />
      </View>
    );
  }

  render() {
    let { webViewUrl, visible } = this.props;
    return (
      <Modal visible={visible}
        animationType="slide"
        transparent={true}
        onRequestClose={() => {
          this.props.closeWebView();
          // Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.container}>
          <SafeAreaView style={{ backgroundColor: constants.green }}></SafeAreaView>
          <View style={{ flex: 1, backgroundColor: "white" }}>
            {this.renderToolbar()}
            <WebView
              source={{ uri: webViewUrl }}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              startInLoadingState={true}
              renderLoading={this.ActivityIndicatorLoadingView}
            />
            {/* <Text>Loading . . . </Text> */}
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width,
    backgroundColor: constants.green
  },
  toolbar: {
    height: 50,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    backgroundColor: constants.green,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 3,
    shadowRadius: 0,
  },
  headerStnName: {
    color: "white",
    fontSize: 16,
    fontFamily: constants.fonts.semiBold
  },
  backBtnText: {
    color: "white",
    fontFamily: constants.fonts.semiBold
  },
  ActivityIndicatorStyle: {
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width,
    justifyContent: 'center',
    alignItems: "center",
    // backgroundColor: "red"
  },
})

export default WebViewExample;