import React from 'react';
import { View, Text } from 'react-native';
import constants from '../constants/constants';

export default OffScreen = (props) => {
	if (props.offValue) {
		return (
			<View style={{
				backgroundColor: constants.green,
				position: "absolute",
				top: 5,
				left: 5,
				zIndex: 2,
				padding: 5
			}}>
				<Text style={{
					fontSize: 12,
					fontFamily: constants.fonts.bold,
					color: "white"
				}}>{props.offValue}% OFF</Text>
			</View>
		)
	} else {
		return null
	}
}