import { StyleSheet, Dimensions } from 'react-native';
import constants from '../constants/constants';

export default StyleSheet.create({
	bgImage: {
		width: "100%",
		height: "100%",
		position: "absolute"
	},
	outerSection: {
		flex: 1,
		// height: (Dimensions.get("window").height),
		alignItems: 'center',
		justifyContent: 'center',
		position: "relative"
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: ((Dimensions.get("window").width) - 50),
		minHeight: 480,
		position: "relative",
		marginTop: 45,
		paddingTop: 50,
		marginBottom: 0
	},
	innerImage: {
		width: 100, height: 100,
		borderRadius: 50,
		zIndex: 1, marginTop: -510
	},
	innerlabel: {
		marginTop: 10,
		fontSize: 18,
		color: constants.black,
		color: "black", fontFamily: constants.fonts.bold
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		top: 0,
		backgroundColor: constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15
	},
	forgotLine: {
		width: "auto", height: 30,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121',
		margin: 5
	},
	loginText: {
		fontSize: 22,
		color: constants.black,
		color: "black", fontFamily: constants.fonts.bold,
	},
	input: {
		marginLeft: 10,
		width: "80%",
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 10,
		marginBottom: 5
	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5,
		borderRadius: 25,
		backgroundColor: constants.green,
		borderColor: constants.green
	},
	socialSection: {
		width: "auto",
		height: 70,
		alignContent: "center",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	socialButton: {
		height: 55,
		width: 55,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 2,
		borderRadius: 50,
		marginRight: 5
	},
	signUpBar: {
		width: "80%",
		height: 40,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	signUpLline: {
		height: 1,
		backgroundColor: "lightgrey",
		flex: 1
	},
	loader: { color: "white", fontFamily: constants.fonts.bold }
});