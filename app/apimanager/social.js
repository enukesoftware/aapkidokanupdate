import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../constants/constants';
import firebase from 'react-native-firebase'
const FBSDK = require('react-native-fbsdk');
const { LoginManager, AccessToken } = FBSDK;
export const fbLogin = async () => {
	console.log("log in to fb");
	// const behaviour = await LoginManager.getLoginBehavior();
	// LoginManager.logOut();
	return LoginManager.logInWithPermissions(["public_profile", "email"])
		.then((result) => {
			if (result.isCancelled) {
				console.log("Login cancelled");
			} else {
				return AccessToken.getCurrentAccessToken().then(
					(data) => {
						AsyncStorage.setItem('token', data.accessToken);
						console.log(
							"Login success with permissions: " +
							result.grantedPermissions.toString()
						);
						return data;
					});
			}
		}, (error) => {
			console.log("Login fail with error: " + error);
		});
}

export const _configureGoogleSignIn = async () => {
	try {
		const configured = await GoogleSignin.configure({
			// webClientId: "699255423286-tokel11tvpgrb4q0hn6h753j5ge28r97.apps.googleusercontent.com",
			webClientId: Constants.WEB_CLIENT_ID,
			offlineAccess: true,
		});
		return configured;
	} catch (error) {
		console.log("google not configured:-- ", error);
	}
}

export const googleLogin = async () => {
	try {
		console.log("Login in google");
		// add any configuration settings here:
		await GoogleSignin.configure({
			// webClientId: "699255423286-tokel11tvpgrb4q0hn6h753j5ge28r97.apps.googleusercontent.com",
			webClientId: Constants.WEB_CLIENT_ID,
			offlineAccess: true,
		});
		console.log("configured");
		const isGoogleSignedIn = await GoogleSignin.isSignedIn();
		console.log("signed in google checked");
		if (isGoogleSignedIn) {
			try {
			  console.log("signed in google going to log out");
			  await GoogleSignin.revokeAccess();
			  await GoogleSignin.signOut();
			  console.log("google log out");
			} catch (error) {
			  console.log("ERROR:" + JSON.stringify(error));
			}
		  }

		await GoogleSignin.hasPlayServices();

		console.log("playservices checked");

		const data = await GoogleSignin.signIn();

		console.log("google signed in");
		AsyncStorage.setItem('token', data.idToken);

		return data;
		// create a new firebase credential with the token
		// const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
		// console.log("google got the credential");



		// login with credential  from firebase
		// const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);

		// console.warn(JSON.stringify(firebaseUserCredential.user.toJSON()));
	} catch (error) {
		console.log("error in google" + JSON.stringify(error));
		if (error.code === statusCodes.SIGN_IN_CANCELLED) {
			// sign in was cancelled
			alert('cancelled');
		} else if (error.code === statusCodes.IN_PROGRESS) {
			// operation in progress already
			alert('in progress');
		} else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
			alert('play services not available or outdated');
		} else {
			alert('Something went wrong', error.toString());
		}
	}
}

export const fbLoginWithFirebase = async () => {
	try {
		const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);

		if (result.isCancelled) {
			// handle this however suites the flow of your app
			throw new Error('User cancelled request');
		}

		console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);

		// get the access token
		const data = await AccessToken.getCurrentAccessToken();

		if (!data) {
			// handle this however suites the flow of your app
			throw new Error('Something went wrong obtaining the users access token');
		}
		console.log(JSON.stringify(data));
		// create a new firebase credential with the token
		const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

		// login with credential
		const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);

		console.warn(JSON.stringify(firebaseUserCredential.user.toJSON()))
	} catch (e) {
		console.error(e);
	}
}