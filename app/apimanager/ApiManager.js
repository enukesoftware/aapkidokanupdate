import AsyncStorage from '@react-native-community/async-storage';
import constants from '../constants/constants';
import NetInfo from "@react-native-community/netinfo";
import moment from 'moment';
import NavigationService from '../global/NavigationService';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';

callApi = async (url, methodType, body, customHeader) => {
	const netInfo = await NetInfo.fetch();
	if (!netInfo.isConnected) {
		return { singleStringMessage: "Internet is not available" }
	}
	let token = await AsyncStorage.getItem("token");
	const header = customHeader ? customHeader : {
		Accept: "application/json",
		"Content-Type": "application/json",
		"Authorization": token ? "Bearer " + token : null,
		// agency_Id: Constants.agencyId
	};
	console.log(url);
	console.log(token);
	console.log(JSON.stringify(header));
	console.log(JSON.stringify(body));
	return fetch(url, {
		method: methodType,
		headers: header,
		body: (methodType == "POST" || methodType == "PUT") ? JSON.stringify(body) : null
	}
	).then((response) => {
		return response.json();
	}).then((response) => {
		// if (response.error && response.error == 401) {
		// 	constants.previousStateForLogin = "Home";
		// 	NavigationService.navigate("OtherLoginStack");
		// 	return { singleStringMessage: "You have been loged out" };
		// } else 
		if (response.status || (response.code && response.code == 200) || response.singleStringMessage) {
			return response
		} else {
			return new Error(" status code " + (response.code || response.error))
		}
	}).catch((error) => {
		console.error(error);
		return error
		// throw error
	});
}

export const fbLogin = async (body) => {
	body.fcm_token = await AsyncStorage.getItem("fcmToken");
	console.log("FB login data is:-" + JSON.stringify(body));
	return callApi(constants.url + 'auth/facebook-login', "POST", body)
		.then(async (res) => {
			if (res && res.data && res.data.user) {
				if (res.data.user.picture && !res.data.user.picture.startsWith("http")) {
					res.data.user.picture = constants.imageBaseURL + res.data.user.picture;
				}
				await AsyncStorage.setItem("token", res.data.token);
				await AsyncStorage.setItem("user", JSON.stringify(res.data.user));
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const googleLogin = async (body) => {
	body.fcm_token = await AsyncStorage.getItem("fcmToken");
	console.log("Google login data is:-" + JSON.stringify(body));
	return callApi(constants.url + 'auth/google-login', "POST", body)
		.then((res) => {
			if (res && res.data && res.data.user) {
				if (res.data.user.picture && !res.data.user.picture.startsWith("http")) {
					res.data.user.picture = constants.imageBaseURL + res.data.user.picture;
				}
				res.data.user.contact_number = "";
				AsyncStorage.setItem("user", JSON.stringify(res.data.user));
				AsyncStorage.setItem("token", res.data.token);
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const login = async (body) => {
	let fcm_token = await AsyncStorage.getItem("fcmToken");
	if (fcm_token) {
		body.fcm_token = fcm_token;
	}
	console.log("Login data is:-" + JSON.stringify(body));
	return callApi(constants.url + 'auth/login', "POST", body)
		.then(async (res) => {
			if (res && res.success && res.code == 200) {
				if (res.data.user.picture && !res.data.user.picture.startsWith("http")) {
					res.data.user.picture = constants.imageBaseURL + res.data.user.picture;
				}
				await AsyncStorage.setItem("user", JSON.stringify(res.data.user));
				await AsyncStorage.setItem("token", res.data.token);
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const register = async (body) => {
	return callApi(constants.url + 'auth/register', "POST", body)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const updateFcmToken = (token) => {
	return callApi(constants.url + 'user/fcm-token', "POST", { fcm_token: token })
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getProfile = () => {
	console.log("checking profile");
	return callApi(constants.url + 'user/profile', "GET")
		.then((res) => {
			if (!res.success && res.code && res.code.code && res.code.code == 422 && res.code.name && res.code.name == "UNPROCESSABLE_ENTITY") {
				console.log("profile not found, going to log out");
				logOut().then((res) => {
					console.log("You are logged out now");
				});
			}
		}).catch((err) => {
			console.log(JSON.stringify(err));
		});
}

export const logOut = async () => {
	const netInfo = await NetInfo.fetch();
	if (!netInfo.isConnected) {
		Toast.showWithGravity("Internet is not available", Toast.SHORT, Toast.BOTTOM);
		return
	}
	try {
		let fbToken = await AccessToken.getCurrentAccessToken();
		console.log(fbToken);
		if (fbToken) {
			await LoginManager.logOut();
		}
		await GoogleSignin.configure({
			// webClientId: "699255423286-tokel11tvpgrb4q0hn6h753j5ge28r97.apps.googleusercontent.com",
			webClientId: constants.WEB_CLIENT_ID,
			offlineAccess: true,
		});
		const isGoogleSignedIn = await GoogleSignin.isSignedIn();
		
		if (isGoogleSignedIn) {
			try {
			  console.log("signed in google going to log out");
			  await GoogleSignin.revokeAccess();
			  await GoogleSignin.signOut();
			  console.log("google log out");
			} catch (error) {
			  console.log("ERROR:" + JSON.stringify(error));
			}
		  }
		return AsyncStorage.multiSet(
			[
				['user', '{}'],
				['cart', '[]'],
				['selectAddress', '{}'],
				['selectedSlot', '{}'],
				['coupon', '{}'],
				['fcmToken', '']
			], () => {
				return true;
			});
	} catch (error) {
		console.error(error);
		return false;
	}
}

export const verifyOtp = async (body) => {
	return callApi(constants.url + 'auth/verify-otp', "POST", body)
		.then(async (res) => {
			if (res.success && res.code == 200) {
				if (res.data.user.picture && !res.data.user.picture.startsWith("http")) {
					res.data.user.picture = constants.imageBaseURL + res.data.user.picture;
				}
				await AsyncStorage.setItem("user", JSON.stringify(res.data.user));
				await AsyncStorage.setItem("token", res.data.token);
				constants.user = res.data.user;
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getCities = async () => {
	return callApi(constants.url + 'cities', "GET")
		.then((res) => {
			if (res && res.data && res.data.cities) {
				for (let index = 0; index < res.data.cities.length; index++) {
					res.data.cities[index].value = res.data.cities[index].name;
				}
				console.log("got cities");
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getAreas = async (city) => {
	return callApi(constants.url + 'areas?city_id=' + city._id, "GET")
		.then((res) => {
			if (res && res.data && res.data.city) {
				for (let index = 0; index < res.data.city.areas.length; index++) {
					res.data.city.areas[index].value = res.data.city.areas[index].name;
				}
				console.log("got areas");
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getStoresForSetting = async () => {
	let url = constants.url + 'store?area_id=' + constants.area._id;
	console.log("url is:-" + url);
	return callApi(url, "GET")
		.then((res) => {
			if (res && res.data) {
				if (res.data.stores) {
					for (let index = 0; index < res.data.stores.length; index++) {
						if (res.data.stores[index].picture) {
							res.data.stores[index].imgUrl = { uri: constants.imageBaseURL + res.data.stores[index].picture }
						} else {
							res.data.stores[index].imgUrl = { uri: "http://storage.x-e.com.cy/cache/1/zbS2uMlupbIj49ylDIdtjZ7K8f9Q7gH3.jpg?s=5796994b519916a04d4538ba86076236" }
						}
					}
					console.log("recieved stores");
				} else if (res.data.categories) {
					for (let i = 0; i < res.data.categories.length; i++) {
						for (let j = 0; j < res.data.categories[i].stores.length; j++) {
							if (res.data.categories[i].stores[j].picture) {
								res.data.categories[i].stores[j].imgUrl = { uri: constants.imageBaseURL + res.data.categories[i].stores[j].picture }
							} else {
								res.data.categories[i].stores[j].imgUrl = { uri: "http://storage.x-e.com.cy/cache/1/zbS2uMlupbIj49ylDIdtjZ7K8f9Q7gH3.jpg?s=5796994b519916a04d4538ba86076236" }
							}
						}
					}
				}
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getStoresForHome = async () => {
	console.log(constants.store)
	let url = constants.url + 'store/' + constants.store._id;
	console.log("url is:---" + url);
	return callApi(url, "GET")
		.then((res) => {
			if (res.success && res.code == 200) {
				for (let index = 0; index < res.data.categories.length; index++) {
					res.data.categories[index].imgUrl = { uri: "http://storage.x-e.com.cy/cache/1/zbS2uMlupbIj49ylDIdtjZ7K8f9Q7gH3.jpg?s=5796994b519916a04d4538ba86076236" }
				}
				return AsyncStorage.getItem("cart").then((data) => {
					let cart = data ? JSON.parse(data) : [];
					let counter = 0;
					for (let j = 0; j < res.data.featured_products.length; j++) {
						res.data.featured_products[j].count = 0;
						res.data.featured_products[j].off = 0;
						if (res.data.featured_products[j].price.cost_price && res.data.featured_products[j].price.cost_price != res.data.featured_products[j].price.sale_price) {
							res.data.featured_products[j].off = ~~(100 - ((res.data.featured_products[j].price.sale_price * 100) / res.data.featured_products[j].price.cost_price));
						}
						if (!res.data.featured_products[j].order_max) {
							res.data.featured_products[j].order_max = 0
						}
						if (!res.data.featured_products[j].stock_quantity) {
							res.data.featured_products[j].stock_quantity = 0
						}
						for (let i = 0; i < cart.length; i++) {
							if (cart[i]._id == res.data.featured_products[j]._id) {
								res.data.featured_products[j].count = cart[i].count;
							}
						}
					}
					for (let j = 0; j < res.data.best_selling_products.length; j++) {
						res.data.best_selling_products[j].count = 0;
						res.data.best_selling_products[j].off = 0;
						if (res.data.best_selling_products[j].price.cost_price && res.data.best_selling_products[j].price.cost_price != res.data.best_selling_products[j].price.sale_price) {
							res.data.best_selling_products[j].off = ~~(100 - ((res.data.best_selling_products[j].price.sale_price * 100) / res.data.best_selling_products[j].price.cost_price));
						}
						if (!res.data.best_selling_products[j].order_max) {
							res.data.best_selling_products[j].order_max = 0
						}
						if (!res.data.best_selling_products[j].stock_quantity) {
							res.data.best_selling_products[j].stock_quantity = 0
						}
						for (let i = 0; i < cart.length; i++) {
							if (cart[i]._id == res.data.best_selling_products[j]._id) {
								res.data.best_selling_products[j].count = cart[i].count;
							}
						}
					}
					console.log("recieved stores for home");
					constants.taxes = res.data.taxes;
					AsyncStorage.setItem("taxes", JSON.stringify(res.data.taxes));
					return res;
				});
			} else return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getCategories = () => {
	let url = constants.url + 'category/?store_id=' + constants.store._id;
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getSubcategories = (id) => {
	let url = constants.url + 'category/' + id;
	console.log(url);
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getProducts = (subcategory_id, search, pageNo) => {
	let url = constants.url + 'product?store_id=' + constants.store._id;
	if (subcategory_id) {
		url += "&subcategory_id=" + subcategory_id;
	}
	if (search && search.length > 0) {
		url += "&search=" + search;
	}
	if (pageNo) {
		url += "&pageNo=" + pageNo;
	}
	if (pageNo) {
		url += "&pageNo=" + pageNo + "&perPage=30";
	}
	console.log("url calling for :- " + url);
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			if (res.success && res.code == 200) {
				return AsyncStorage.getItem("cart").then((data) => {
					let cart = data ? JSON.parse(data) : [];
					let counter = 0;
					for (let j = 0; j < res.data.products.length; j++) {
						res.data.products[j].count = 0;
						res.data.products[j].off = 0;
						if (res.data.products[j].price.cost_price && res.data.products[j].price.cost_price != res.data.products[j].price.sale_price) {
							res.data.products[j].off = ~~(100 - ((res.data.products[j].price.sale_price * 100) / res.data.products[j].price.cost_price));
						}
						if (!res.data.products[j].stock_quantity) {
							res.data.products[j].stock_quantity = 0
						}
						if (!res.data.products[j].order_max) {
							res.data.products[j].order_max = 0
						}
						for (let i = 0; i < cart.length; i++) {
							if (cart[i]._id == res.data.products[j]._id) {
								res.data.products[j].count = cart[i].count;
							}
						}
					}
					console.log("recieved stores for home");
					return res;
				});
			} else return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getAddressList = () => {
	let url = constants.url + 'user/address';
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const saveAddress = (data) => {
	let url = constants.url + 'user/address';
	return callApi(url, "POST", data)
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const updateAddress = (data) => {
	let url = constants.url + 'user/address/' + data._id;
	return callApi(url, "PUT", data)
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const deleteAddress = (id) => {
	let url = constants.url + 'user/address/' + id;
	return callApi(url, "DELETE")
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getSlots = (store_id) => {
	let url = constants.url + 'store/' + store_id + "/slots";
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const placeOrder = (data, slot_id, store_id) => {
	let url = constants.url + 'order/checkout';
	console.log("place order url is:--" + url);
	console.log("data is ;---" + JSON.stringify(data));
	return callApi(url, "POST", data)
		.then((res) => {
			console.log(res)
			if (res.success && res.code == 200) {
				AsyncStorage.removeItem("coupon");
				constants.coupon = {};
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getOrders = (pageNo) => {
	let url = constants.url + "order?name=created_at&sortType=-1&pageNo=" + pageNo;
	return callApi(url, "GET")
		.then((res) => {
			if (res.success && res.code == 200) {
				for (let i = 0; i < res.data.orders.length; i++) {
					res.data.orders[i].order_date = moment(res.data.orders[i].created_at).format("D MMMM, YYYY");
					// res.data.orders[i].customer = { picture: "https://zdnet3.cbsistatic.com/hub/i/r/2018/08/14/e12bd7e8-6fd7-46a1-b9b3-dc015f001216/resize/770xauto/9c22e91bc78ace64efc8c9c62db65637/sainsburys-smartshop-scan-pay-go-trial-customers-scanning-their-shopping-with-their-smartphone-fo.jpg" };
					if (res.data.orders[i].customer.picture && !res.data.orders[i].customer.picture.startsWith("http")) {
						res.data.orders[i].customer.picture = constants.imageBaseURL + res.data.orders[i].customer.picture;
					}
					switch (res.data.orders[i].status) {
						case 1:
							res.data.orders[i].orderStatus = "Placed";
							break;
						case 2:
							res.data.orders[i].orderStatus = "Picked Up";
							break;
						case 3:
							res.data.orders[i].orderStatus = "Delivered";
							break;
						case 4:
							res.data.orders[i].orderStatus = "UnDelivered";
							break;
						case 5:
							res.data.orders[i].orderStatus = "Cancelled";
							break;
						case 6:
							res.data.orders[i].orderStatus = "In Delivery";
							break;
					}
				}
			}
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const resendOtp = (body) => {
	return callApi(constants.url + 'auth/forget-password', "POST", body)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const changePassword = (data) => {
	let url = constants.url + 'auth/change-password';
	return callApi(url, "POST", data)
		.then(async (res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const forgotPassword = (body) => {
	return callApi(constants.url + 'auth/forget-password', "POST", body)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const resetPassword = (data) => {
	let url = constants.url + 'auth/reset-password';
	return callApi(url, "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const applyCoupon = (code, amount) => {
	let url = constants.url + "coupon/check?coupon_code=" + code + "&store_id=" + constants.store._id + "&cart_value=" + amount;
	console.log("url is:--" + url);
	return callApi(url, "GET")
		.then((res) => {
			// debugger
			if (res && res.success && res.code == 200 && res.data.coupon.status == 1) {
				constants.coupon = res.data.coupon;
				AsyncStorage.setItem("coupon", JSON.stringify(res.data.coupon))
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const cancelOrder = (id) => {
	let url = constants.url + 'order/cancel';
	return callApi(url, "PUT", { order_id: id })
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getInvoice = (id) => {
	let url = constants.url + 'order/invoice/' + id;
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			return res;
		}).catch((error) => {
			console.log(error);

			// throw new Error(error);
		});
}

export const getOrderDetails = (id) => {
	let url = constants.url + 'order/' + id;
	return callApi(url, "GET")
		.then((res) => {
			console.log(res)
			if (res.success && res.code == 200) {
				switch (res.data.order.status) {
					case 1:
						res.data.order.orderStatus = "Placed";
						break;
					case 2:
						res.data.order.orderStatus = "Picked Up";
						break;
					case 3:
						res.data.order.orderStatus = "Delivered";
						break;
					case 4:
						res.data.order.orderStatus = "UnDelivered";
						break;
					case 5:
						res.data.order.orderStatus = "Cancelled";
						break;
					case 6:
						res.data.order.orderStatus = "In Delivery";
						break;
				}
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getDirectionsJson = (url) => {
	return callApi(url, "GET");
}

export const checkProductsAvailability = (data) => {
	let url = constants.url + 'product/availability';
	return callApi(url, "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}