import { createStore, combineReducers, compose } from "redux";
import liveLocationReducer from "../Reducers/liveLocationReducer";
import cartReducer from "../Reducers/cartReducer";

const rootReducer = combineReducers({
	getLiveLocation: liveLocationReducer,
	getCart: cartReducer
})

export const configureStore = () => {
	return createStore(rootReducer,/* preloadedState, */
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
}