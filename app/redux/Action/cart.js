import ActionTypes from '../types'

export const setCart = (data) => {
	return {
		type: ActionTypes.CART,
		data: data
	};
}
