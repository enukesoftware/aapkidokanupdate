import ActionTypes from '../types'

export const setLiveLocation = (liveLocationData) => {
	return {
		type: ActionTypes.LIVE_LOCATION,
		liveLocationData: liveLocationData
	};
}
