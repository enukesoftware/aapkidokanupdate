import ActionTypes from '../types'

export default (state = [], action) => {
	switch (action.type) {
		case ActionTypes.CART:
			return action.data
		default:
			return state
	}
}